<?php



namespace App;



use Illuminate\Notifications\Notifiable;

use Illuminate\Foundation\Auth\User as Authenticatable;

use Illuminate\Support\Facades\Hash;



class User extends Authenticatable

{

    use Notifiable;

    const Administrator = '1';
    const Finance = '2';
    const OPS = '3';
    const Business   = '4';
    const Client = '5';

    //status
    const ACTIVE = 'Active';
    const INACTIVE = 'Inactive';

    /**

     * The attributes that are mass assignable.

     *

     * @var array

     */

    protected $fillable = [

        'name', 'email', 'password'

    ];



    /**

     * The attributes that should be hidden for arrays.

     *

     * @var array

     */

    protected $hidden = [

        'password', 'remember_token',

    ];



    public function updateUser($data)

{

        $ticket = $this->find($data['id']);

        

        $ticket->name = $data['name'];

        $ticket->email = $data['email'];

        $ticket->password = Hash::make($data['password']);

        $ticket->role_id = $data['role_id'];

        $ticket->status = $data['status'];

        $ticket->save();

        return 1;

}

    public function AddUser($data,$password)
{

   
    $user = new user();
        $user->name = $data['first_name'];
        $user->email = $data['email'];
        $user->password = Hash::make($password);
        $user->role_id = user::Client;
        $user->status = user::ACTIVE;
        $user->remember_token = request('_token');
        $user->save();
        return $user->id;
}

    /* get menu */

    public static  function showMenu() {
  
        $role_id = Auth::user()->role_id;
       // $id = Auth::user()->id;
        $roles = DB::select("SELECT * FROM `tbl_role_permission` as role_permission inner join tbl_permission as permission on role_permission.permission_id=permission.id where role_id='$role_id'");
       
      if(count($roles) == 0){
            return '0';
      }
      else{
        foreach ($roles as $key => $value) {
          $arry[]= $value->action_name; 
        }
         return $arry;
       
      }
    } 

    /*
    *get Role
    */ 
    public function getUserRole($role_id)
    {
        return $roles = DB::select("SELECT name FROM `tbl_roles` where id=".$role_id."");
    }

}

