<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Process;
use App\Notification;
use App\Invoice;
use App\Ticket;
use App\Message;
use App\Client;
use App\User;
use App\Company;
use App\ProcessStepHistory;
class cronCreateTicket extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:name';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
     
       $status = process::TicketForApprovedFromClient;
       $today_date=date('Y-m-d');
       $check_status = Invoice::select('invoice_id','client_id','invoice_date','first_contact_spoc','status','id','company_id')->where(['status' => process::ApproveAtFinance])->get();

      foreach($check_status as $invoice){
          $new_invoice_date= date('Y-m-d', strtotime($invoice->invoice_date. ' + '.'5 days'));
       
          $today_date=date('Y-m-d');
  
          $client_info = Client::where(['id' => $invoice->client_id])->first();
          $get_client_role_id = User::where(['id' => $client_info->user_id])->first();
          $client_spoc = User::where(['id' => $invoice->first_contact_spoc])->first();
          $company_mail = Company::where(['id' => $invoice->company_id])->first();

        if($new_invoice_date <= $today_date){
          
          //create ticket
        $content= 'Invoice no. '.$invoice->invoice_id.'there is no updates form client side past 5 days';
        $ticket = new Ticket();
        $data=$ticket->create_ticket_for_dues($content, $client_info->user_id,$get_client_role_id->role_id,$status);

        $ticket->create_ticket_for_dues($content, $client_spoc->id,$client_spoc->role_id,$status);
       
        $notification = new Notification();
        $notification->console_send_notification_finance($invoice->invoice_id,$status,$company_mail->id);
        $notification->console_send_notification_other($invoice->invoice_id,$client_spoc->id,$status,$company_mail->id);
        $notification->console_send_notification_other($invoice->invoice_id,$client_info->user_id,$status,$company_mail->id);   

        //process_step
        $process_histroy = new ProcessStepHistory();
        $process_histroy = ProcessStepHistory::whereNull('process_time')->where('invoice_id' ,$invoice->invoice_id)->first();

        $process_histroy->console_updateprocess_time($process_histroy->id);
        $process_histroy->console_save_process_history($status,$invoice->invoice_id,$company_mail->id);
        //Send mail
            //mail

            $message = new Message();
            $content ='Invoice Approval delay';
            $new_status ='Invoice no. '.$invoice->invoice_id.'there is no updates form client side past 5 days';
            $message->console_save_mail_invoice_status_spoc($invoice->invoice_id,$client_spoc->id,$status,$content,$company_mail->id,$company_mail->email);
            
            $message->console_Send_mail_approve_invoice_spoc($invoice->invoice_id,$client_spoc->id,$client_info->user_id,$new_status,$content,$company_mail->email); 
//get first spoc
            $message->console_save_mail_invoice_status_client($invoice->invoice_id,$client_info->user_id,$status,$content,$company_mail->id,$company_mail->email);
            $message->console_Send_mail_approve_invoice_client($invoice->invoice_id,$client_info->user_id,$new_status,$content,$company_mail->email);

       // $invoice->updateInvoiceStatus($invoice->id,$status);
        }

      }
        if (empty($ticket->errors)) {
            return '1';
        } else {
            return '0';
        }


    }
}
