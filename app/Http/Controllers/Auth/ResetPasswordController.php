<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Foundation\Auth\PasswordReset;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
    
    protected function reset(Request $request){
        /*
        $whereconditoin = ['email'=>$request->email]; 
        $user = User::select('id','password')->where($whereconditoin)->first();  
        $token = DB::table('password_resets')
                    ->select('email')
                    ->where('email', '=', $request->email)
                    ->get();
        if(count($token) != 0)
        {
            if ( !empty($user)) {
                if(Hash::check($request->password, $user->password)){
                    return redirect()->back()->withErrors('You can not use your current password.');
                }
                else{
                    $user->password = Hash::make($request->password);
                    $user->save();
                    $delete = DB::table('password_resets')->where('email', '=', $request->email)->delete();
                    return redirect()->route('login')->with('message','Password changed Successfully!');
                }
            }
        }
        else
        {
            return redirect()->back()->withErrors('Link you are using is expired.');
        }
        */
        
        
        $request->validate($this->rules(), $this->validationErrorMessages());
        
        $token = DB::table('password_resets')->select('token')->where('email', $request->email)->first();
        if($token){
            if(!Hash::check($request->token, $token->token)){
                return redirect()->back()->withErrors('Invalid token please request reset password again.');
                exit;
            }
        }
        else{
            return redirect()->back()->withErrors('Invalid token please request reset password again.');
            exit;
        }
        

        $whereconditoin = ['email'=>$request->email]; 
        $u = User::select('id','password')->where($whereconditoin)->first();
        if(Hash::check($request->password, $u->password)){
            return redirect()->back()->withErrors('You can not use your current password.');
            exit;
        }
        

        // Here we will attempt to reset the user's password. If it is successful we
        // will update the password on an actual user model and persist it to the
        // database. Otherwise we will parse the error and return the response.
        $response = $this->broker()->reset(
            $this->credentials($request), function ($user, $password) {
                $this->resetPassword($user, $password);
            }
        );
        
        

        // If the password was successfully reset, we will redirect the user back to
        // the application's home authenticated view. If there is an error we can
        // redirect them back to where they came from with their error message.
        return $response == Password::PASSWORD_RESET
                    ? $this->sendResetResponse($request, $response)
                    : $this->sendResetFailedResponse($request, $response);
        
    }
    
}
