<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Validator;
use App\Roles;

class RolesController extends Controller
{


    public function index(){
    	$roles = DB::select('select * from tbl_roles');
       // $user = User::all();
        return view('roles.index',compact('roles'));
    }
 
    public function create(){
        return view('roles.create');
    }
 
    public function store(Request $request){
 	
        $roles = new Roles();
            $validatedData = $request->validate([
            'name' => 'required|max:255',
            'status'  => 'required',

        ]);
        $roles->name = request('name');
        $roles->status = request('status');
        $roles->save();
        return redirect('/roles')->with('message', 'Role Create Successfully!');;
 
    }

        public function edit($id)
    {

        $roles = Roles::where(['id'=>$id])->first();
     //   return view('member.edit',compact('user'));
        return view('roles.edit', compact('roles', 'id'));
    }

        public function destroy($id)
    {
        $ticket = Roles::find($id);
        $ticket->delete();

        return redirect('/roles')->with('message', 'Roles has been deleted!!');
    }


        public function update(Request $request, $id)
    {
        $user = new Roles();
        $data = $this->validate($request, [
            'name' => 'required|max:255',
            'status'  => 'required',
        ]);
        $data['id'] = $id;
        $user->updateRoles($data);
        return redirect('/roles')->with('message', 'Role has been updated!!');
    }


}
