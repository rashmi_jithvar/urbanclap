<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Validator;
use App\User;
use App\Notification;
use App\Invoice;
use App\Roles;
use App\Client;
use App\Process;
use App\Ticket;
use App\Message;
use App\Company;
use App\ProcessStepHistory;
use Auth;
use DB;



class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index()
    {    
      return view('home1');
    }

    public function view_profile()
    {
        $role_id = Auth::user()->role_id;
        $role = DB::table('tbl_roles')->find($role_id);
        $role=$role->name;
        return view('profile.view_profile',compact('role'));
    }
    
    public function changepassword()
    {
        return view('profile.changepassword');
    }

    public function updatepassword(Request $request)
    {
        $whereconditoin = ['id'=>Auth::user()->id];
        $request->validate([
            'password' => ['required', 'string',  'confirmed'],
        ]);
        $user = User::select('id','password')->where($whereconditoin)->first();
        if ( !empty($user)) {

            if(Hash::check($request->password, $user->password)){
                return redirect()->back()->withErrors('You can not use your current password.');
                exit;
            }
            else{
                $user->password = Hash::make($request->password);
                $user->save();
                return redirect()->route('home')->with('message','Password  has been Changed Successfully !');
            }
        }else{
            return redirect()->back()->with('message','Confirm Password does not match with the New Password');
        }
    }

        public function changedetails()
    {
        $id = Auth::user()->id;
        $user = User::where(['id'=>$id])->first();
        return view('profile.changedetails',compact('user', 'id'));
    }

        public function updatedetails(Request $request)
    {
        $id = Auth::user()->id;
        $user = new User();
        $data = $this->validate($request, [
            'id' => 'required',
            'name' => 'required|max:255',
            'email' => 'required|max:255',
        ]);
        
        $user =  User::find($id);
        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->save();
        return redirect()->route('home')->with('message','Password  has been Changed Successfully !');
    }

        public function show_notification()
    {
       $role_id = Auth::user()->role_id;
      $role=Roles::where(['id'=>$role_id])->first();
     
    if($role->name =='Finance'){
        
        $notification =  Notification::where(['has_read'=>'0','notification_to' => Auth::user()->id])->orderBy('id', 'desc')->get();
       
    $output = "";
    $output .="<a href='javascript:;'' class='dropdown-toggle' data-toggle='dropdown' data-hover='dropdown' data-close-others='true'>
                                <i class='fa fa-bell-o'></i>
                                <span class='badge headerBadgeColor1'> ".count($notification)." </span>
                            </a>
                            <ul class='dropdown-menu animated swing'>
                                <li class='external'>
                                    <h3><span class='bold'>Notifications</span></h3>
                                    <span class='notification-label purple-bgcolor'>New ".count($notification)."</span>
                                </li>
                                <li> <ul class='dropdown-menu-list small-slimscroll-style' data-handle-color='#637283' id='notification'>";
       foreach ($notification as $key) {
           # code... 
       $status = DB::table('tbl_process')->where(['id' => $key->status])->first();
       $output .= "
            <li>
                <a href=".action('HomeController@update_notification_finance',['id' => $key->id,'invoice_id' => $key->invoice_id]).">
                    <span class='time'>".$key->title."</span>
                    <span class='details'>
                    <span class='notification-icon circle deepPink-bgcolor'><i class='fa fa-check'></i></span>Invoice No.".$key->invoice_id."</span><h6><b>Current Status:-</b>".$status->name."</h6>
                </a>
            </li>
                    ";
      }
      $output .=" </ul>
                       <div class='dropdown-menu-footer'>
                                        <a href='javascript:void(0)'> All notifications </a>
                                    </div>
                                </li>
                            </ul>";
      echo $output;
  }
  else if($role->name =='Client'){
        $id = Auth::user()->id;
        $notification =  Notification::where(['has_read'=>'0','notification_to' => $id])->orderBy('id', 'desc')->get();
          
    $output = "";
    $output .="<a href='javascript:;'' class='dropdown-toggle' data-toggle='dropdown' data-hover='dropdown' data-close-others='true'>
                                <i class='fa fa-bell-o'></i>
                                <span class='badge headerBadgeColor1'> ".count($notification)." </span>
                            </a>
                            <ul class='dropdown-menu animated swing'>
                                <li class='external'>
                                    <h3><span class='bold'>Notifications</span></h3>
                                    <span class='notification-label purple-bgcolor'>New ".count($notification)."</span>
                                </li>
                                <li> <ul class='dropdown-menu-list small-slimscroll-style' data-handle-color='#637283' id='notification'>";
       foreach ($notification as $key) {
           # code... 
        $status = DB::table('tbl_process')->where(['id' => $key->status])->first();
       $output .= "
            <li>
                <a href=".action('HomeController@update_notification_client',['id' => $key->id,'invoice_id' => $key->invoice_id]).">
                    <span class='time'>".$key->title."</span>
                    <span class='details'>
                    <span class='notification-icon circle deepPink-bgcolor'><i class='fa fa-check'></i></span>Invoice No.".$key->invoice_id."</span><h6><b>Current Status:-</b>".$status->name."</h6>
                </a>
            </li>
                    ";
      }
      $output .=" </ul>
                       <div class='dropdown-menu-footer'>
                                        <a href='javascript:void(0)'> All notifications </a>
                                    </div>
                                </li>
                            </ul>";
      echo $output;
  }
      else{
        $output = "";
        $output .="<a href='javascript:;'' class='dropdown-toggle' data-toggle='dropdown' data-hover='dropdown' data-close-others='true'>
                                <i class='fa fa-bell-o'></i>
                                
                            </a>
                            <ul class='dropdown-menu animated swing'>
                                <li class='external'>
                                    <h3><span class='bold'>Notifications</span></h3>
                                    <span class='notification-label purple-bgcolor'></span>
                                </li>
                                <li> <ul class='dropdown-menu-list small-slimscroll-style' data-handle-color='#637283' id='notification'>";
        $output .= "<li>
                <a href=''>
                   
                    <span class='details'>
                     <span class=''></span>No Notification</span>
                </a>
            </li>";
        $output .=" </ul>
                       <div class='dropdown-menu-footer'>
                                        <a href='javascript:void(0)'> All notifications </a>
                                    </div>
                                </li>
                            </ul>";
        echo $output;
      }

    }

        public function update_notification_finance($id,$invoice_id)
    {
       $notification = new Notification();
       $invoice = new Invoice();
       $notification->updateNotificationHasReadFinance($id);
      // $invoice->updateInvoiceStatusFinance($invoice_id);
      $invoice_id = Invoice::where(['invoice_id'=>$invoice_id])->first();
      $id= $invoice_id->id;
      return redirect('/invoice/view/'. $id)->with('message', 'Invoice');
    }

        public function update_notification_client($id,$invoice_id)
    {
       $notification = new Notification();
       $invoice = new Invoice();
       $notification->updateNotificationHasReadClient($id);
      $invoice_id = Invoice::where(['invoice_id'=>$invoice_id])->first();
      $id= $invoice_id->id;
       return redirect('/invoice/view/'. $id)->with('message', 'Invoice');
    }

    public function genarate_ticket_approve_invoice(){
       $status = process::TicketForApprovedFromClient;
       $today_date=date('Y-m-d');
       $check_status = Invoice::select('invoice_id','client_id','invoice_date','first_contact_spoc','status','id','company_id')->where(['status' => process::ApproveAtFinance])->get();

      foreach($check_status as $invoice){
          $new_invoice_date= date('Y-m-d', strtotime($invoice->invoice_date. ' + '.'5 days'));
       
          $today_date=date('Y-m-d');
  
          $client_info = Client::where(['id' => $invoice->client_id])->first();
          $get_client_role_id = User::where(['id' => $client_info->user_id])->first();
          $client_spoc = User::where(['id' => $invoice->first_contact_spoc])->first();
          $company_mail = Company::where(['id' => $invoice->company_id])->first();

        if($new_invoice_date <= $today_date){
          
          //create ticket
        $content= 'Invoice no. '.$invoice->invoice_id.'there is no updates form client side past 5 days';
        $ticket = new Ticket();
        $data=$ticket->create_ticket_for_dues($content, $client_info->user_id,$get_client_role_id->role_id,$status);

        $ticket->create_ticket_for_dues($content, $client_spoc->id,$client_spoc->role_id,$status);
       
        $notification = new Notification();
        $notification->console_send_notification_finance($invoice->invoice_id,$status,$company_mail->id);
        $notification->console_send_notification_other($invoice->invoice_id,$client_spoc->id,$status,$company_mail->id);
        $notification->console_send_notification_other($invoice->invoice_id,$client_info->user_id,$status,$company_mail->id);   

        //process_step
        $process_histroy = new ProcessStepHistory();
        $process_histroy = ProcessStepHistory::whereNull('process_time')->where('invoice_id' ,$invoice->invoice_id)->first();

        $process_histroy->console_updateprocess_time($process_histroy->id);
        $process_histroy->console_save_process_history($status,$invoice->invoice_id,$company_mail->id);
        //Send mail
            //mail

            $message = new Message();
            $content ='Invoice Approval delay';
            $new_status ='Invoice no. '.$invoice->invoice_id.'there is no updates form client side past 5 days';
            $message->console_save_mail_invoice_status_spoc($invoice->invoice_id,$client_spoc->id,$status,$content,$company_mail->id,$company_mail->email);
            
            $message->console_Send_mail_approve_invoice_spoc($invoice->invoice_id,$client_spoc->id,$client_info->user_id,$new_status,$content,$company_mail->email); 
//get first spoc
            $message->console_save_mail_invoice_status_client($invoice->invoice_id,$client_info->user_id,$status,$content,$company_mail->id,$company_mail->email);
            $message->console_Send_mail_approve_invoice_client($invoice->invoice_id,$client_info->user_id,$new_status,$content,$company_mail->email);

       // $invoice->updateInvoiceStatus($invoice->id,$status);
        }

      }
        if (empty($ticket->errors)) {
            return '1';
        } else {
            return '0';
        }


    }

      public function genarate_ticket_dues_invoice(){
       $status = process::TicketForApprovedFromClient;
       $today_date=date('Y-m-d');
       $check_status = Invoice::select('invoice_id','client_id','invoice_date','first_contact_spoc','status','id')->where(['status' => process::ApproveAtClient])->get();

      foreach($check_status as $invoice){

          $client_info = Client::where(['id' => $invoice->client_id])->first();
          $get_client_role_id = User::where(['id' => $client_info->user_id])->first();
          $client_spoc = User::where(['id' => $invoice->first_contact_spoc])->first();

        if($invoice->due_date <= $today_date){
          //create ticket
        $content= 'Invoice no. '.$invoice->invoice_id.'Has Dues Please Clear you all Dues ASAP' ;
        $ticket = new Ticket();
        $ticket->create_ticket_for_dues($content, $client_info->user_id,$get_client_role_id->role_id,$status);
        $ticket->create_ticket_for_dues($content, $client_spoc->id,$client_spoc->role_id,$status);
        ////notification
        $notification = new Notification();
        //$notification->send_notification_finance($invoice->invoice_id,$status);
        $notification->send_notification_other($invoice->invoice_id,$client_spoc->id,$status);
        $notification->send_notification_other($invoice->invoice_id,$client_info->user_id,$status);   
        //process_step
        $process_histroy = new ProcessStepHistory();
        $process_histroy = ProcessStepHistory::whereNull('process_time')->where('invoice_id' ,$invoice->invoice_id)->first();

        $process_histroy->updateprocess_time($process_histroy->id);
        $process_histroy->save_process_history($status,$invoice->invoice_id);
            //mail
            $message = new Message();
            $content ='Invoice Payment delay';
            $new_status ='Invoice no. '.$invoice->invoice_id.'Has Dues Please Clear you all Dues ASAP';
            $message->save_mail_invoice_status_spoc($invoice->invoice_id,$client_spoc->id,$status,$content);
            $message->Send_mail_approve_invoice_spoc($invoice->invoice_id,$client_spoc->id,$client_info->user_id,$new_status,$content); 
//get first spoc

            $message->save_mail_invoice_status_client($invoice->invoice_id,$client_info->user_id,$status,$content);
            $message->Send_mail_approve_invoice_client($invoice->invoice_id,$client_info->user_id,$new_status,$content);

        }

      }
        if (empty($ticket->errors)) {
            return '1';
        } else {
            return '0';
        }
    }

    
}
