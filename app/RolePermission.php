<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RolePermission extends Model
{

	  protected $table = 'tbl_role_permission';
	  
        public function updateRolePermission($data)
{
        $roles = $this->find($data['id']);
        $roles->permission_id = $data['permission_id'];
        $roles->role_id = $data['role_id'];
        $roles->status = $data['status'];
        $roles->updated_by = '1';
        $roles->save();
        return 1;
}
}
