-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 27, 2019 at 02:35 PM
-- Server version: 10.1.40-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ptindiao_finance`
--

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_client`
--

CREATE TABLE `tbl_client` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `company_name` varchar(255) NOT NULL,
  `website` varchar(255) DEFAULT NULL,
  `mobile` varchar(12) NOT NULL,
  `email` varchar(255) NOT NULL,
  `address_line_1` text,
  `address_line_2` text,
  `city` varchar(150) NOT NULL,
  `state` varchar(200) NOT NULL,
  `pincode` varchar(6) NOT NULL,
  `gst_number` varchar(50) DEFAULT NULL,
  `credit_period` varchar(20) DEFAULT NULL,
  `credit_period_defination` int(11) NOT NULL,
  `spoc_business` varchar(200) DEFAULT NULL,
  `spoc_ops` varchar(200) DEFAULT NULL,
  `spoc_finance` varchar(200) DEFAULT NULL,
  `first_contact_spoc` int(11) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_client`
--

INSERT INTO `tbl_client` (`id`, `user_id`, `first_name`, `last_name`, `company_name`, `website`, `mobile`, `email`, `address_line_1`, `address_line_2`, `city`, `state`, `pincode`, `gst_number`, `credit_period`, `credit_period_defination`, `spoc_business`, `spoc_ops`, `spoc_finance`, `first_contact_spoc`, `warehouse_id`, `image`, `status`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(1, 16, 'Rashmi', 'Pathak', 'Company ABC', NULL, '9826456874', 'rashmi_pathak@gmail.com', 'Bhopal', NULL, 'Bhopal', 'Madhya Pradesh', '235641', NULL, '30', 2, NULL, NULL, NULL, 5, 0, NULL, 1, 1, '2019-07-17 07:03:45', NULL, '2019-07-16 01:33:37'),
(2, 17, 'Sachin', 'Jaiswal', 'JCS Pvt. Ltd.', NULL, '7905590238', 'sachin@ptindia.org', 'C-219, Sec C', 'Near Bansal Hospital', 'Bhopal', 'Madhya Pradesh', '462039', NULL, '60', 2, 'Sachin J', 'Vivek Kumar', 'Rahul Singh', 5, 0, NULL, 1, 1, '2019-07-16 07:20:43', NULL, '2019-07-16 01:50:43'),
(12, 27, 'Rashmi', 'pathak', 'Rashmi', NULL, '9426555555', 'rashmimalviya1994@gmail.com', 'bhopal', NULL, 'bhopal', 'm.p', '454454', NULL, NULL, 1, '9', '8', '13', 14, 0, NULL, 1, 1, '2019-07-16 10:56:17', NULL, '2019-07-16 05:26:17'),
(11, 26, 'Rashmi', 'Pathak', 'Rashmi', NULL, '8845445648', 'rashmipathak0202@gmail.com', 'bhopal', NULL, 'bhopal', 'm.p', '265465', NULL, NULL, 3, NULL, NULL, NULL, 14, 0, NULL, 1, 1, '2019-07-16 10:42:45', NULL, '2019-07-16 05:12:45');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_client_warehouse`
--

CREATE TABLE `tbl_client_warehouse` (
  `id` int(11) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_client_warehouse`
--

INSERT INTO `tbl_client_warehouse` (`id`, `warehouse_id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 8, 23, '2019-07-16 00:37:14', '2019-07-16 00:37:14'),
(2, 9, 23, '2019-07-16 00:37:14', '2019-07-16 00:37:14'),
(3, 8, 24, '2019-07-16 00:39:59', '2019-07-16 00:39:59'),
(4, 9, 24, '2019-07-16 00:39:59', '2019-07-16 00:39:59'),
(5, 8, 1, '2019-07-16 01:33:37', '2019-07-16 01:33:37'),
(6, 10, 1, '2019-07-16 01:33:37', '2019-07-16 01:33:37'),
(7, 6, 2, '2019-07-16 01:50:43', '2019-07-16 01:50:43'),
(8, 8, 3, '2019-07-16 05:00:00', '2019-07-16 05:00:00'),
(9, 9, 3, '2019-07-16 05:00:00', '2019-07-16 05:00:00'),
(10, 8, 4, '2019-07-16 05:00:40', '2019-07-16 05:00:40'),
(11, 9, 4, '2019-07-16 05:00:40', '2019-07-16 05:00:40'),
(12, 8, 5, '2019-07-16 05:00:50', '2019-07-16 05:00:50'),
(13, 9, 5, '2019-07-16 05:00:50', '2019-07-16 05:00:50'),
(14, 8, 6, '2019-07-16 05:04:42', '2019-07-16 05:04:42'),
(15, 9, 6, '2019-07-16 05:04:42', '2019-07-16 05:04:42'),
(16, 8, 7, '2019-07-16 05:04:57', '2019-07-16 05:04:57'),
(17, 9, 7, '2019-07-16 05:04:57', '2019-07-16 05:04:57'),
(18, 8, 8, '2019-07-16 05:05:03', '2019-07-16 05:05:03'),
(19, 9, 8, '2019-07-16 05:05:03', '2019-07-16 05:05:03'),
(20, 8, 9, '2019-07-16 05:05:28', '2019-07-16 05:05:28'),
(21, 9, 9, '2019-07-16 05:05:28', '2019-07-16 05:05:28'),
(22, 8, 10, '2019-07-16 05:09:37', '2019-07-16 05:09:37'),
(23, 9, 10, '2019-07-16 05:09:37', '2019-07-16 05:09:37'),
(24, 9, 11, '2019-07-16 05:12:45', '2019-07-16 05:12:45'),
(25, 10, 11, '2019-07-16 05:12:45', '2019-07-16 05:12:45'),
(26, 8, 12, '2019-07-16 05:26:16', '2019-07-16 05:26:16'),
(27, 9, 12, '2019-07-16 05:26:16', '2019-07-16 05:26:16');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_company_wings`
--

CREATE TABLE `tbl_company_wings` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `company_name` varchar(255) NOT NULL,
  `website` varchar(255) DEFAULT NULL,
  `mobile` varchar(12) NOT NULL,
  `email` varchar(255) NOT NULL,
  `address_line_1` text,
  `address_line_2` text,
  `city` varchar(150) NOT NULL,
  `state` varchar(200) NOT NULL,
  `pincode` varchar(6) NOT NULL,
  `gst_number` varchar(50) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_company_wings`
--

INSERT INTO `tbl_company_wings` (`id`, `user_id`, `first_name`, `last_name`, `company_name`, `website`, `mobile`, `email`, `address_line_1`, `address_line_2`, `city`, `state`, `pincode`, `gst_number`, `image`, `status`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(2, NULL, 'XYZ', NULL, 'XYZ', NULL, '9856745855', 'xyz@gmail.com', 'Bhopal', NULL, 'Bhopal', 'm.p', '565885', NULL, NULL, 1, 1, '2019-07-12 06:52:39', NULL, '2019-07-12 06:52:39'),
(4, NULL, 'Holisol', 'Logistics', 'Holisol Logistics Private Limited', NULL, '7042118266', 'info@holisollogistics.com', 'A1 Cariappa Marg Saket', 'Saket', 'New Delhi', 'Delhi', '110062', '07AACCH2364P1Z9', NULL, 1, 1, '2019-07-18 03:48:51', NULL, '2019-07-18 03:48:51');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_follow_up`
--

CREATE TABLE `tbl_follow_up` (
  `id` int(11) NOT NULL,
  `follow_up_date` date NOT NULL,
  `invoice_id` varchar(50) NOT NULL,
  `is_new_payment` tinyint(4) DEFAULT NULL,
  `new_payment_date` date DEFAULT NULL,
  `remark` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_follow_up`
--

INSERT INTO `tbl_follow_up` (`id`, `follow_up_date`, `invoice_id`, `is_new_payment`, `new_payment_date`, `remark`, `created_at`, `created_by`, `updated_at`) VALUES
(3, '2019-06-21', '123', 0, NULL, 'hello', '2019-06-21 07:22:02', 1, '2019-06-21 07:22:02'),
(4, '2019-06-29', '12', 0, NULL, 'Hello', '2019-06-29 04:21:30', 1, '2019-06-29 04:21:30'),
(5, '2019-07-03', '0001', 1, '2019-07-04', 'Hello', '2019-07-03 05:47:11', 5, '2019-07-03 05:47:11'),
(6, '2019-07-09', '0022', 1, '2019-07-09', 'test', '2019-07-09 06:37:10', 13, '2019-07-09 06:37:10'),
(7, '2019-07-16', '0008', 0, '1970-01-01', 'hii', '2019-07-16 12:38:13', 1, '2019-07-16 12:38:13'),
(8, '2019-07-16', '0008', 0, '1970-01-01', 'hii', '2019-07-16 12:39:08', 1, '2019-07-16 12:39:08'),
(9, '2019-07-16', '0008', 0, '1970-01-01', 'hii', '2019-07-16 12:43:15', 1, '2019-07-16 12:43:15'),
(10, '2019-07-16', '0008', 0, NULL, 'hello', '2019-07-16 12:49:12', 1, '2019-07-16 12:49:12'),
(11, '2019-07-16', '0008', 1, '2019-07-17', 'hello rashmi', '2019-07-16 12:50:41', 1, '2019-07-16 12:50:41'),
(12, '2019-07-16', '0008', 0, NULL, 'Hii admin', '2019-07-16 12:51:05', 1, '2019-07-16 12:51:05');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_invoice`
--

CREATE TABLE `tbl_invoice` (
  `id` int(11) NOT NULL,
  `invoice_id` varchar(150) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `due_date` date DEFAULT NULL,
  `invoice_date` date NOT NULL,
  `warehouse_name` int(11) NOT NULL,
  `finance_refference_no` varchar(50) DEFAULT NULL,
  `finance_invoice_date` date DEFAULT NULL,
  `cost_center` varchar(200) NOT NULL,
  `total` decimal(20,2) NOT NULL,
  `status` varchar(20) NOT NULL,
  `file` varchar(255) DEFAULT NULL,
  `paid_amount` decimal(20,2) DEFAULT NULL,
  `due_amount` decimal(20,2) DEFAULT NULL,
  `first_contact_spoc` int(11) DEFAULT NULL,
  `company_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_delete` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_invoice`
--

INSERT INTO `tbl_invoice` (`id`, `invoice_id`, `client_id`, `due_date`, `invoice_date`, `warehouse_name`, `finance_refference_no`, `finance_invoice_date`, `cost_center`, `total`, `status`, `file`, `paid_amount`, `due_amount`, `first_contact_spoc`, `company_id`, `created_by`, `created_at`, `updated_by`, `updated_at`, `is_delete`) VALUES
(1, '0001', 1, NULL, '2019-07-18', 10, NULL, '1970-01-16', '1000', '7000.00', '11', NULL, NULL, '7000.00', 5, 3, 1, '2019-07-16 11:20:24', NULL, '2019-07-16 05:50:24', 0),
(2, '0002', 2, NULL, '2019-07-16', 6, NULL, '1970-01-01', '1500', '165000.00', '11', '1563275308.jpg', NULL, '165000.00', 5, 2, 1, '2019-07-16 11:19:25', NULL, '2019-07-16 05:49:25', 0),
(3, '0003', 11, '1970-01-01', '2019-07-18', 10, NULL, '2019-07-16', '1000', '53000.00', '11', NULL, NULL, '53000.00', 14, 3, 1, '2019-07-16 12:27:56', NULL, '2019-07-16 06:57:56', 0),
(6, '0006', 11, NULL, '2019-07-20', 10, NULL, '2019-07-17', '1111', '1000.00', '1', NULL, NULL, '1000.00', 14, 3, 1, '2019-07-16 13:16:50', NULL, '2019-07-16 07:46:50', 0),
(5, '0005', 12, '1970-01-01', '2019-07-16', 9, NULL, '2019-07-16', '1000', '20000.00', '1', NULL, NULL, '20000.00', 14, 2, 1, '2019-07-16 13:10:53', NULL, '2019-07-16 07:40:53', 0),
(7, '0007', 1, '2019-08-17', '2019-07-17', 10, NULL, '2019-07-31', 'Bhopal', '232000.00', '12', NULL, NULL, '200000.00', 5, 3, 1, '2019-07-18 05:52:41', NULL, '2019-07-18 00:22:41', 0),
(8, '0008', 1, '2019-08-16', '2019-07-06', 10, NULL, '2019-07-13', 'bhopal', '200000.00', '12', NULL, NULL, '200000.00', 5, 2, 1, '2019-07-17 07:05:34', NULL, '2019-07-17 01:35:34', 0),
(9, '0009', 1, '2019-08-17', '2019-07-17', 8, NULL, '2019-07-17', '100000', '22300.00', '12', NULL, NULL, '22300.00', 5, 2, 1, '2019-07-18 06:55:34', NULL, '2019-07-18 01:25:34', 0),
(10, '0010', 1, '2019-08-17', '2019-07-18', 8, NULL, '2019-07-18', '12000', '50000.00', '8', NULL, '49550.25', '449.75', 5, 2, 1, '2019-07-18 06:12:32', NULL, '2019-07-18 00:42:32', 0),
(11, '0011', 2, NULL, '2019-06-01', 6, NULL, '2019-07-18', 'MFC Mundka', '130000.00', '4', NULL, NULL, '130000.00', 5, 4, 1, '2019-07-18 09:29:53', NULL, '2019-07-18 03:59:53', 0),
(12, '0012', 1, NULL, '2019-07-23', 10, NULL, '2019-07-23', '2000', '2000000.00', '1', NULL, NULL, '2000000.00', 5, 4, 1, '2019-07-22 06:16:38', NULL, '2019-07-22 00:46:38', 0),
(13, '0013', 1, NULL, '2019-07-22', 10, NULL, '2019-07-24', '10000', '15000000.00', '1', NULL, NULL, '15000000.00', 5, 4, 1, '2019-07-22 06:47:05', NULL, '2019-07-22 01:17:05', 0),
(14, '0014', 12, '1970-01-01', '2019-07-22', 8, NULL, '2019-07-24', '10000', '15000000.00', '1', NULL, NULL, '15000000.00', 14, 4, 1, '2019-07-22 06:48:10', NULL, '2019-07-22 01:18:10', 0),
(15, '0015', 12, '1970-01-01', '2019-07-25', 9, NULL, '2019-07-25', '20000', '280000000.00', '1', NULL, NULL, '280000000.00', 14, 4, 1, '2019-07-22 06:49:03', NULL, '2019-07-22 01:19:03', 0),
(16, '0016', 2, NULL, '2019-07-18', 6, NULL, '2019-07-11', '10000', '200000.00', '1', '1564217823.xlsx', NULL, '200000.00', 5, 4, 1, '2019-07-27 08:57:03', NULL, '2019-07-27 03:27:03', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_invoice_status`
--

CREATE TABLE `tbl_invoice_status` (
  `Id` int(11) NOT NULL,
  `invoice_id` varchar(50) NOT NULL,
  `status` varchar(20) NOT NULL,
  `remark` text,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_invoice_status`
--

INSERT INTO `tbl_invoice_status` (`Id`, `invoice_id`, `status`, `remark`, `created_by`, `created_at`, `updated_at`) VALUES
(1, '0008', '9', 'modify', 1, '2019-07-16 12:52:57', '2019-07-16 12:52:57'),
(2, '0008', '9', 'change city', 1, '2019-07-16 12:53:44', '2019-07-16 12:53:44'),
(3, '0008', '5', 'cancel', 1, '2019-07-16 12:56:29', '2019-07-16 12:56:29'),
(4, '0008', '1', NULL, 1, '2019-07-16 12:56:37', '2019-07-16 12:56:37'),
(5, '0008', '5', 'cancel', 1, '2019-07-16 12:56:46', '2019-07-16 12:56:46'),
(6, '0008', '1', NULL, 1, '2019-07-16 12:58:47', '2019-07-16 12:58:47'),
(7, '0008', '5', 'cancel', 1, '2019-07-17 01:27:56', '2019-07-17 01:27:56'),
(8, '0008', '1', NULL, 1, '2019-07-17 01:28:00', '2019-07-17 01:28:00'),
(9, '0008', '9', 'change name', 1, '2019-07-17 01:28:46', '2019-07-17 01:28:46'),
(10, '0008', '5', 'cancel admin', 1, '2019-07-17 01:29:24', '2019-07-17 01:29:24'),
(11, '0008', '1', NULL, 1, '2019-07-17 01:29:27', '2019-07-17 01:29:27'),
(12, '0008', '9', 'change', 1, '2019-07-17 01:31:29', '2019-07-17 01:31:29'),
(13, '0008', '5', 'cancel test', 1, '2019-07-17 01:32:01', '2019-07-17 01:32:01'),
(14, '0008', '1', NULL, 1, '2019-07-17 01:33:06', '2019-07-17 01:33:06'),
(15, '0007', '9', 'change city', 1, '2019-07-17 01:46:46', '2019-07-17 01:46:46'),
(16, '0009', '9', 'change city', 16, '2019-07-17 03:52:43', '2019-07-17 03:52:43'),
(17, '0007', '6', 'reject', 1, '2019-07-18 00:18:10', '2019-07-18 00:18:10'),
(18, '0010', '9', 'Add item', 1, '2019-07-18 00:34:30', '2019-07-18 00:34:30'),
(19, '0010', '9', 'Add item', 16, '2019-07-18 00:36:18', '2019-07-18 00:36:18'),
(20, '0010', '9', 'change city', 1, '2019-07-18 00:39:43', '2019-07-18 00:39:43'),
(21, '0010', '8', NULL, 1, '2019-07-18 00:41:23', '2019-07-18 00:41:23');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_message`
--

CREATE TABLE `tbl_message` (
  `id` int(11) NOT NULL,
  `sent_from` varchar(255) NOT NULL,
  `sent_to` varchar(255) NOT NULL,
  `sent_cc` text,
  `sent_bcc` varchar(255) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `message_type` varchar(20) NOT NULL,
  `message_content` text NOT NULL,
  `message_attachment` varchar(255) DEFAULT NULL,
  `sent_flag` smallint(6) NOT NULL DEFAULT '0',
  `sent_attempts` smallint(3) NOT NULL DEFAULT '0',
  `sent_on` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_message`
--

INSERT INTO `tbl_message` (`id`, `sent_from`, `sent_to`, `sent_cc`, `sent_bcc`, `subject`, `message_type`, `message_content`, `message_attachment`, `sent_flag`, `sent_attempts`, `sent_on`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'admin@gmail.com', 'rashmi@gmail.com', 'admin@gmail.com', NULL, '$content', 'text', '', NULL, 0, 1, NULL, 1, '2019-07-16 05:38:28', '2019-07-16 05:38:28'),
(2, 'admin@gmail.com', 'saroj.kumar@holisollogistics.com', 'admin@gmail.com', NULL, '$content', 'text', '', NULL, 0, 1, NULL, 1, '2019-07-16 05:38:28', '2019-07-16 05:38:28'),
(3, 'admin@gmail.com', 'finance@gmail.com', 'admin@gmail.com', NULL, '$content', 'text', '', NULL, 0, 1, NULL, 1, '2019-07-16 05:38:28', '2019-07-16 05:38:28'),
(4, 'admin@gmail.com', 'rashmi@gmail.com', 'admin@gmail.com', NULL, '$content', 'text', '', '/tmp/phppkoOjG', 0, 1, NULL, 1, '2019-07-16 05:38:28', '2019-07-16 05:38:28'),
(5, 'admin@gmail.com', 'saroj.kumar@holisollogistics.com', 'admin@gmail.com', NULL, '$content', 'text', '', '/tmp/phppkoOjG', 0, 1, NULL, 1, '2019-07-16 05:38:28', '2019-07-16 05:38:28'),
(6, 'admin@gmail.com', 'finance@gmail.com', 'admin@gmail.com', NULL, '$content', 'text', '', '/tmp/phppkoOjG', 0, 1, NULL, 1, '2019-07-16 05:38:28', '2019-07-16 05:38:28'),
(7, 'admin@gmail.com', 'finance@gmail.com', 'admin@gmail.com', NULL, 'Invoice Created', 'text', '', NULL, 0, 1, NULL, 1, '2019-07-16 05:38:30', '2019-07-16 05:38:30'),
(8, 'admin@gmail.com', 'finance@gmail.com', 'admin@gmail.com', NULL, 'Invoice Created', 'text', '', '/tmp/phppkoOjG', 0, 1, NULL, 1, '2019-07-16 05:38:30', '2019-07-16 05:38:30'),
(9, 'admin@gmail.com', 'rashmi@gmail.com', 'admin@gmail.com', NULL, '$content', 'text', '', NULL, 0, 1, NULL, 1, '2019-07-16 05:41:59', '2019-07-16 05:41:59'),
(10, 'admin@gmail.com', 'saroj.kumar@holisollogistics.com', 'admin@gmail.com', NULL, '$content', 'text', '', NULL, 0, 1, NULL, 1, '2019-07-16 05:41:59', '2019-07-16 05:41:59'),
(11, 'admin@gmail.com', 'finance@gmail.com', 'admin@gmail.com', NULL, '$content', 'text', '', NULL, 0, 1, NULL, 1, '2019-07-16 05:41:59', '2019-07-16 05:41:59'),
(12, 'admin@gmail.com', 'finance@gmail.com', 'admin@gmail.com', NULL, 'Invoice Updated', 'text', '', NULL, 0, 1, NULL, 1, '2019-07-16 05:42:00', '2019-07-16 05:42:00'),
(13, 'admin@gmail.com', 'rashmi@gmail.com', 'admin@gmail.com', NULL, '$content', 'text', '', NULL, 0, 1, NULL, 1, '2019-07-16 05:48:54', '2019-07-16 05:48:54'),
(14, 'admin@gmail.com', 'saroj.kumar@holisollogistics.com', 'admin@gmail.com', NULL, '$content', 'text', '', NULL, 0, 1, NULL, 1, '2019-07-16 05:48:54', '2019-07-16 05:48:54'),
(15, 'admin@gmail.com', 'finance@gmail.com', 'admin@gmail.com', NULL, '$content', 'text', '', NULL, 0, 1, NULL, 1, '2019-07-16 05:48:54', '2019-07-16 05:48:54'),
(16, 'admin@gmail.com', 'rashmi@gmail.com', 'admin@gmail.com', NULL, 'Invoice Created', 'text', '', NULL, 0, 1, NULL, 1, '2019-07-16 05:48:55', '2019-07-16 05:48:55'),
(17, 'admin@gmail.com', 'rashmi@gmail.com', 'admin@gmail.com', NULL, '$content', 'text', '', NULL, 0, 1, NULL, 1, '2019-07-16 07:40:34', '2019-07-16 07:40:34'),
(18, 'admin@gmail.com', 'saroj.kumar@holisollogistics.com', 'admin@gmail.com', NULL, '$content', 'text', '', NULL, 0, 1, NULL, 1, '2019-07-16 07:40:34', '2019-07-16 07:40:34'),
(19, 'admin@gmail.com', 'finance@gmail.com', 'admin@gmail.com', NULL, '$content', 'text', '', NULL, 0, 1, NULL, 1, '2019-07-16 07:40:34', '2019-07-16 07:40:34'),
(20, 'admin@gmail.com', 'rashmi@gmail.com', 'admin@gmail.com', NULL, '$content', 'text', '', NULL, 0, 1, NULL, 1, '2019-07-16 07:40:53', '2019-07-16 07:40:53'),
(21, 'admin@gmail.com', 'saroj.kumar@holisollogistics.com', 'admin@gmail.com', NULL, '$content', 'text', '', NULL, 0, 1, NULL, 1, '2019-07-16 07:40:53', '2019-07-16 07:40:53'),
(22, 'admin@gmail.com', 'finance@gmail.com', 'admin@gmail.com', NULL, '$content', 'text', '', NULL, 0, 1, NULL, 1, '2019-07-16 07:40:53', '2019-07-16 07:40:53'),
(23, 'admin@gmail.com', 'rashmi@gmail.com', 'admin@gmail.com', NULL, 'Invoice Created', 'text', '', NULL, 0, 1, NULL, 1, '2019-07-16 07:40:55', '2019-07-16 07:40:55'),
(24, 'admin@gmail.com', 'rashmi@gmail.com', 'admin@gmail.com', NULL, '$content', 'text', '', NULL, 0, 1, NULL, 1, '2019-07-16 07:46:50', '2019-07-16 07:46:50'),
(25, 'admin@gmail.com', 'saroj.kumar@holisollogistics.com', 'admin@gmail.com', NULL, '$content', 'text', '', NULL, 0, 1, NULL, 1, '2019-07-16 07:46:50', '2019-07-16 07:46:50'),
(26, 'admin@gmail.com', 'finance@gmail.com', 'admin@gmail.com', NULL, '$content', 'text', '', NULL, 0, 1, NULL, 1, '2019-07-16 07:46:50', '2019-07-16 07:46:50'),
(27, 'admin@gmail.com', 'rashmi@gmail.com', 'admin@gmail.com', NULL, 'Invoice Created', 'text', '', NULL, 0, 1, NULL, 1, '2019-07-16 07:46:51', '2019-07-16 07:46:51');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_notification`
--

CREATE TABLE `tbl_notification` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `invoice_id` varchar(50) NOT NULL,
  `notification_from` varchar(255) NOT NULL,
  `notification_to` varchar(255) DEFAULT NULL,
  `has_read` int(11) NOT NULL,
  `sent_flag` int(11) NOT NULL,
  `status` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_notification`
--

INSERT INTO `tbl_notification` (`id`, `title`, `invoice_id`, `notification_from`, `notification_to`, `has_read`, `sent_flag`, `status`, `created_at`, `created_by`, `updated_at`) VALUES
(1, 'Invoice', '0001', '1', '14', 0, 0, '1', '2019-07-16 01:42:44', NULL, '2019-07-16 01:42:44'),
(2, 'Invoice', '0001', '1', '13', 0, 0, '1', '2019-07-16 01:42:44', NULL, '2019-07-16 01:42:44'),
(3, 'Invoice', '0001', '1', '5', 0, 0, '1', '2019-07-16 01:42:44', NULL, '2019-07-16 01:42:44'),
(4, 'Invoice', '0001', '1', '5', 0, 0, '1', '2019-07-16 01:42:44', NULL, '2019-07-16 01:42:44'),
(5, 'Invoice', '0002', '1', '14', 0, 0, '1', '2019-07-16 01:46:13', NULL, '2019-07-16 01:46:13'),
(6, 'Invoice', '0002', '1', '13', 0, 0, '1', '2019-07-16 01:46:13', NULL, '2019-07-16 01:46:13'),
(7, 'Invoice', '0002', '1', '5', 0, 0, '1', '2019-07-16 01:46:13', NULL, '2019-07-16 01:46:13'),
(8, 'Invoice', '0002', '1', '5', 0, 0, '1', '2019-07-16 01:46:13', NULL, '2019-07-16 01:46:13'),
(9, 'Invoice', '0003', '1', '14', 0, 0, '1', '2019-07-16 02:00:52', NULL, '2019-07-16 02:00:52'),
(10, 'Invoice', '0003', '1', '13', 0, 0, '1', '2019-07-16 02:00:52', NULL, '2019-07-16 02:00:52'),
(11, 'Invoice', '0003', '1', '5', 0, 0, '1', '2019-07-16 02:00:52', NULL, '2019-07-16 02:00:52'),
(12, 'Invoice', '0003', '1', '5', 0, 0, '1', '2019-07-16 02:00:52', NULL, '2019-07-16 02:00:52'),
(13, 'Invoice', '0003', '5', '14', 0, 0, '2', '2019-07-16 02:06:55', NULL, '2019-07-16 02:06:55'),
(14, 'Invoice', '0003', '5', '13', 0, 0, '2', '2019-07-16 02:06:55', NULL, '2019-07-16 02:06:55'),
(15, 'Invoice', '0003', '5', '5', 0, 0, '2', '2019-07-16 02:06:55', NULL, '2019-07-16 02:06:55'),
(16, 'Invoice', '0003', '5', '5', 0, 0, '2', '2019-07-16 02:06:55', NULL, '2019-07-16 02:06:55'),
(17, 'Invoice', '0003', '5', '14', 0, 0, '2', '2019-07-16 02:07:32', NULL, '2019-07-16 02:07:32'),
(18, 'Invoice', '0003', '5', '13', 0, 0, '2', '2019-07-16 02:07:32', NULL, '2019-07-16 02:07:32'),
(19, 'Invoice', '0003', '5', '5', 0, 0, '2', '2019-07-16 02:07:32', NULL, '2019-07-16 02:07:32'),
(20, 'Invoice', '0003', '5', '5', 0, 0, '2', '2019-07-16 02:07:32', NULL, '2019-07-16 02:07:32'),
(21, 'Invoice', '0003', '5', '14', 0, 0, '2', '2019-07-16 02:17:25', NULL, '2019-07-16 02:17:25'),
(22, 'Invoice', '0003', '5', '13', 0, 0, '2', '2019-07-16 02:17:25', NULL, '2019-07-16 02:17:25'),
(23, 'Invoice', '0003', '5', '5', 0, 0, '2', '2019-07-16 02:17:25', NULL, '2019-07-16 02:17:25'),
(24, 'Invoice', '0003', '5', '5', 0, 0, '2', '2019-07-16 02:17:25', NULL, '2019-07-16 02:17:25'),
(25, 'Invoice', '0003', '5', '14', 0, 0, '2', '2019-07-16 02:17:38', NULL, '2019-07-16 02:17:38'),
(26, 'Invoice', '0003', '5', '13', 0, 0, '2', '2019-07-16 02:17:38', NULL, '2019-07-16 02:17:38'),
(27, 'Invoice', '0003', '5', '5', 0, 0, '2', '2019-07-16 02:17:38', NULL, '2019-07-16 02:17:38'),
(28, 'Invoice', '0003', '5', '5', 0, 0, '2', '2019-07-16 02:17:38', NULL, '2019-07-16 02:17:38'),
(29, 'Invoice', '0003', '5', '14', 0, 0, '1', '2019-07-16 02:17:48', NULL, '2019-07-16 02:17:48'),
(30, 'Invoice', '0003', '5', '13', 0, 0, '1', '2019-07-16 02:17:48', NULL, '2019-07-16 02:17:48'),
(31, 'Invoice', '0003', '5', '5', 0, 0, '1', '2019-07-16 02:17:48', NULL, '2019-07-16 02:17:48'),
(32, 'Invoice', '0003', '5', '5', 0, 0, '1', '2019-07-16 02:17:48', NULL, '2019-07-16 02:17:48'),
(33, 'Invoice', '0003', '5', '14', 0, 0, '2', '2019-07-16 02:18:02', NULL, '2019-07-16 02:18:02'),
(34, 'Invoice', '0003', '5', '13', 0, 0, '2', '2019-07-16 02:18:02', NULL, '2019-07-16 02:18:02'),
(35, 'Invoice', '0003', '5', '5', 0, 0, '2', '2019-07-16 02:18:02', NULL, '2019-07-16 02:18:02'),
(36, 'Invoice', '0003', '5', '5', 0, 0, '2', '2019-07-16 02:18:02', NULL, '2019-07-16 02:18:02'),
(37, 'Invoice', '0003', '5', '14', 0, 0, '2', '2019-07-16 02:23:10', NULL, '2019-07-16 02:23:10'),
(38, 'Invoice', '0003', '5', '13', 0, 0, '2', '2019-07-16 02:23:10', NULL, '2019-07-16 02:23:10'),
(39, 'Invoice', '0003', '5', '5', 0, 0, '2', '2019-07-16 02:23:10', NULL, '2019-07-16 02:23:10'),
(40, 'Invoice', '0003', '5', '5', 0, 0, '2', '2019-07-16 02:23:10', NULL, '2019-07-16 02:23:10'),
(41, 'Invoice', '0003', '5', '14', 0, 0, '2', '2019-07-16 02:23:29', NULL, '2019-07-16 02:23:29'),
(42, 'Invoice', '0003', '5', '13', 0, 0, '2', '2019-07-16 02:23:29', NULL, '2019-07-16 02:23:29'),
(43, 'Invoice', '0003', '5', '5', 0, 0, '2', '2019-07-16 02:23:29', NULL, '2019-07-16 02:23:29'),
(44, 'Invoice', '0003', '5', '5', 0, 0, '2', '2019-07-16 02:23:29', NULL, '2019-07-16 02:23:29'),
(45, 'Invoice', '0003', '5', '14', 0, 0, '2', '2019-07-16 02:23:50', NULL, '2019-07-16 02:23:50'),
(46, 'Invoice', '0003', '5', '13', 0, 0, '2', '2019-07-16 02:23:50', NULL, '2019-07-16 02:23:50'),
(47, 'Invoice', '0003', '5', '5', 0, 0, '2', '2019-07-16 02:23:50', NULL, '2019-07-16 02:23:50'),
(48, 'Invoice', '0003', '5', '5', 0, 0, '2', '2019-07-16 02:23:50', NULL, '2019-07-16 02:23:50'),
(49, 'Invoice', '0003', '5', '14', 0, 0, '2', '2019-07-16 02:24:17', NULL, '2019-07-16 02:24:17'),
(50, 'Invoice', '0003', '5', '13', 0, 0, '2', '2019-07-16 02:24:17', NULL, '2019-07-16 02:24:17'),
(51, 'Invoice', '0003', '5', '5', 0, 0, '2', '2019-07-16 02:24:17', NULL, '2019-07-16 02:24:17'),
(52, 'Invoice', '0003', '5', '5', 0, 0, '2', '2019-07-16 02:24:17', NULL, '2019-07-16 02:24:17'),
(53, 'Invoice', '0004', '1', '14', 0, 0, '1', '2019-07-16 02:49:39', NULL, '2019-07-16 02:49:39'),
(54, 'Invoice', '0004', '1', '13', 0, 0, '1', '2019-07-16 02:49:39', NULL, '2019-07-16 02:49:39'),
(55, 'Invoice', '0004', '1', '5', 0, 0, '1', '2019-07-16 02:49:39', NULL, '2019-07-16 02:49:39'),
(56, 'Invoice', '0004', '1', '5', 0, 0, '1', '2019-07-16 02:49:39', NULL, '2019-07-16 02:49:39'),
(57, 'Invoice', '0004', '1', '14', 0, 0, '1', '2019-07-16 03:08:50', NULL, '2019-07-16 03:08:50'),
(58, 'Invoice', '0004', '1', '13', 0, 0, '1', '2019-07-16 03:08:50', NULL, '2019-07-16 03:08:50'),
(59, 'Invoice', '0004', '1', '5', 0, 0, '1', '2019-07-16 03:08:50', NULL, '2019-07-16 03:08:50'),
(60, 'Invoice', '0004', '1', '5', 0, 0, '1', '2019-07-16 03:08:50', NULL, '2019-07-16 03:08:50'),
(61, 'Invoice', '0004', '1', '14', 0, 0, '1', '2019-07-16 03:10:02', NULL, '2019-07-16 03:10:02'),
(62, 'Invoice', '0004', '1', '13', 0, 0, '1', '2019-07-16 03:10:02', NULL, '2019-07-16 03:10:02'),
(63, 'Invoice', '0004', '1', '5', 0, 0, '1', '2019-07-16 03:10:02', NULL, '2019-07-16 03:10:02'),
(64, 'Invoice', '0004', '1', '5', 0, 0, '1', '2019-07-16 03:10:02', NULL, '2019-07-16 03:10:02'),
(65, 'Invoice', '0004', '1', '14', 0, 0, '1', '2019-07-16 03:11:03', NULL, '2019-07-16 03:11:03'),
(66, 'Invoice', '0004', '1', '13', 0, 0, '1', '2019-07-16 03:11:03', NULL, '2019-07-16 03:11:03'),
(67, 'Invoice', '0004', '1', '5', 0, 0, '1', '2019-07-16 03:11:03', NULL, '2019-07-16 03:11:03'),
(68, 'Invoice', '0004', '1', '5', 0, 0, '1', '2019-07-16 03:11:03', NULL, '2019-07-16 03:11:03'),
(69, 'Invoice', '0004', '1', '14', 0, 0, '1', '2019-07-16 03:12:14', NULL, '2019-07-16 03:12:14'),
(70, 'Invoice', '0004', '1', '13', 0, 0, '1', '2019-07-16 03:12:14', NULL, '2019-07-16 03:12:14'),
(71, 'Invoice', '0004', '1', '5', 0, 0, '1', '2019-07-16 03:12:14', NULL, '2019-07-16 03:12:14'),
(72, 'Invoice', '0004', '1', '5', 0, 0, '1', '2019-07-16 03:12:14', NULL, '2019-07-16 03:12:14'),
(73, 'Invoice', '0004', '1', '14', 0, 0, '4', '2019-07-16 03:17:13', NULL, '2019-07-16 03:17:13'),
(74, 'Invoice', '0004', '1', '13', 0, 0, '4', '2019-07-16 03:17:13', NULL, '2019-07-16 03:17:13'),
(75, 'Invoice', '0004', '1', '5', 0, 0, '4', '2019-07-16 03:17:13', NULL, '2019-07-16 03:17:13'),
(76, 'Invoice', '0004', '1', '5', 0, 0, '4', '2019-07-16 03:17:13', NULL, '2019-07-16 03:17:13'),
(77, 'Invoice', '0002', '1', '14', 0, 0, '4', '2019-07-16 04:35:19', NULL, '2019-07-16 04:35:19'),
(78, 'Invoice', '0002', '1', '13', 0, 0, '4', '2019-07-16 04:35:19', NULL, '2019-07-16 04:35:19'),
(79, 'Invoice', '0002', '1', '5', 0, 0, '4', '2019-07-16 04:35:19', NULL, '2019-07-16 04:35:19'),
(80, 'Invoice', '0002', '1', '5', 0, 0, '4', '2019-07-16 04:35:19', NULL, '2019-07-16 04:35:19'),
(81, 'Invoice', '0001', '1', '14', 0, 0, '1', '2019-07-16 05:38:28', NULL, '2019-07-16 05:38:28'),
(82, 'Invoice', '0001', '1', '13', 0, 0, '1', '2019-07-16 05:38:28', NULL, '2019-07-16 05:38:28'),
(83, 'Invoice', '0001', '1', '5', 0, 0, '1', '2019-07-16 05:38:28', NULL, '2019-07-16 05:38:28'),
(84, 'Invoice', '0001', '1', '5', 0, 0, '1', '2019-07-16 05:38:28', NULL, '2019-07-16 05:38:28'),
(85, 'Invoice', '0002', '1', '14', 0, 0, '1', '2019-07-16 05:38:28', NULL, '2019-07-16 05:38:28'),
(86, 'Invoice', '0002', '1', '13', 0, 0, '1', '2019-07-16 05:38:28', NULL, '2019-07-16 05:38:28'),
(87, 'Invoice', '0002', '1', '5', 0, 0, '1', '2019-07-16 05:38:28', NULL, '2019-07-16 05:38:28'),
(88, 'Invoice', '0002', '1', '5', 0, 0, '1', '2019-07-16 05:38:28', NULL, '2019-07-16 05:38:28'),
(89, 'Invoice', '0002', '5', '14', 0, 0, '4', '2019-07-16 05:41:21', NULL, '2019-07-16 05:41:21'),
(90, 'Invoice', '0002', '5', '13', 0, 0, '4', '2019-07-16 05:41:21', NULL, '2019-07-16 05:41:21'),
(91, 'Invoice', '0002', '5', '5', 0, 0, '4', '2019-07-16 05:41:21', NULL, '2019-07-16 05:41:21'),
(92, 'Invoice', '0002', '5', '5', 0, 0, '4', '2019-07-16 05:41:21', NULL, '2019-07-16 05:41:21'),
(93, 'Invoice', '0001', '1', '14', 0, 0, '1', '2019-07-16 05:41:59', NULL, '2019-07-16 05:41:59'),
(94, 'Invoice', '0001', '1', '13', 0, 0, '1', '2019-07-16 05:41:59', NULL, '2019-07-16 05:41:59'),
(95, 'Invoice', '0001', '1', '5', 0, 0, '1', '2019-07-16 05:41:59', NULL, '2019-07-16 05:41:59'),
(96, 'Invoice', '0001', '1', '5', 0, 0, '1', '2019-07-16 05:41:59', NULL, '2019-07-16 05:41:59'),
(97, 'Invoice', '0003', '1', '14', 0, 0, '1', '2019-07-16 05:48:54', NULL, '2019-07-16 05:48:54'),
(98, 'Invoice', '0003', '1', '13', 0, 0, '1', '2019-07-16 05:48:54', NULL, '2019-07-16 05:48:54'),
(99, 'Invoice', '0003', '1', '5', 0, 0, '1', '2019-07-16 05:48:54', NULL, '2019-07-16 05:48:54'),
(100, 'Invoice', '0003', '1', '14', 0, 0, '1', '2019-07-16 05:48:54', NULL, '2019-07-16 05:48:54'),
(101, 'Invoice', '0002', '5', '14', 0, 0, '11', '2019-07-16 05:49:25', NULL, '2019-07-16 05:49:25'),
(102, 'Invoice', '0002', '5', '13', 0, 0, '11', '2019-07-16 05:49:25', NULL, '2019-07-16 05:49:25'),
(103, 'Invoice', '0002', '5', '5', 0, 0, '11', '2019-07-16 05:49:25', NULL, '2019-07-16 05:49:25'),
(104, 'Invoice', '0002', '5', '5', 0, 0, '11', '2019-07-16 05:49:25', NULL, '2019-07-16 05:49:25'),
(105, 'Invoice', '0002', '5', '17', 1, 0, '11', '2019-07-16 11:20:14', NULL, '2019-07-16 05:50:14'),
(106, 'Invoice', '0001', '1', '14', 0, 0, '4', '2019-07-16 05:49:53', NULL, '2019-07-16 05:49:53'),
(107, 'Invoice', '0001', '1', '13', 0, 0, '4', '2019-07-16 05:49:53', NULL, '2019-07-16 05:49:53'),
(108, 'Invoice', '0001', '1', '5', 0, 0, '4', '2019-07-16 05:49:53', NULL, '2019-07-16 05:49:53'),
(109, 'Invoice', '0001', '1', '5', 0, 0, '4', '2019-07-16 05:49:53', NULL, '2019-07-16 05:49:53'),
(110, 'Invoice', '0001', '1', '14', 0, 0, '11', '2019-07-16 05:50:24', NULL, '2019-07-16 05:50:24'),
(111, 'Invoice', '0001', '1', '13', 0, 0, '11', '2019-07-16 05:50:24', NULL, '2019-07-16 05:50:24'),
(112, 'Invoice', '0001', '1', '5', 0, 0, '11', '2019-07-16 05:50:24', NULL, '2019-07-16 05:50:24'),
(113, 'Invoice', '0001', '1', '5', 0, 0, '11', '2019-07-16 05:50:24', NULL, '2019-07-16 05:50:24'),
(114, 'Invoice', '0001', '1', '16', 1, 0, '11', '2019-07-16 11:20:38', NULL, '2019-07-16 05:50:38'),
(115, 'Invoice', '0003', '1', '14', 0, 0, '4', '2019-07-16 06:14:46', NULL, '2019-07-16 06:14:46'),
(116, 'Invoice', '0003', '1', '13', 0, 0, '4', '2019-07-16 06:14:46', NULL, '2019-07-16 06:14:46'),
(117, 'Invoice', '0003', '1', '5', 1, 0, '4', '2019-07-16 12:19:00', NULL, '2019-07-16 06:49:00'),
(118, 'Invoice', '0003', '1', '14', 0, 0, '4', '2019-07-16 06:14:46', NULL, '2019-07-16 06:14:46'),
(119, 'Invoice', '0003', '1', '14', 0, 0, '11', '2019-07-16 06:57:56', NULL, '2019-07-16 06:57:56'),
(120, 'Invoice', '0003', '1', '13', 0, 0, '11', '2019-07-16 06:57:56', NULL, '2019-07-16 06:57:56'),
(121, 'Invoice', '0003', '1', '5', 0, 0, '11', '2019-07-16 06:57:56', NULL, '2019-07-16 06:57:56'),
(122, 'Invoice', '0003', '1', '14', 0, 0, '11', '2019-07-16 06:57:56', NULL, '2019-07-16 06:57:56'),
(123, 'Invoice', '0003', '1', '26', 1, 0, '11', '2019-07-16 12:32:36', NULL, '2019-07-16 07:02:36'),
(124, 'Invoice', '0004', '1', '14', 0, 0, '1', '2019-07-16 07:40:34', NULL, '2019-07-16 07:40:34'),
(125, 'Invoice', '0004', '1', '13', 0, 0, '1', '2019-07-16 07:40:34', NULL, '2019-07-16 07:40:34'),
(126, 'Invoice', '0004', '1', '5', 0, 0, '1', '2019-07-16 07:40:34', NULL, '2019-07-16 07:40:34'),
(127, 'Invoice', '0004', '1', '14', 0, 0, '1', '2019-07-16 07:40:34', NULL, '2019-07-16 07:40:34'),
(128, 'Invoice', '0005', '1', '14', 0, 0, '1', '2019-07-16 07:40:53', NULL, '2019-07-16 07:40:53'),
(129, 'Invoice', '0005', '1', '13', 0, 0, '1', '2019-07-16 07:40:53', NULL, '2019-07-16 07:40:53'),
(130, 'Invoice', '0005', '1', '5', 0, 0, '1', '2019-07-16 07:40:53', NULL, '2019-07-16 07:40:53'),
(131, 'Invoice', '0005', '1', '14', 0, 0, '1', '2019-07-16 07:40:53', NULL, '2019-07-16 07:40:53'),
(132, 'Invoice', '0006', '1', '14', 0, 0, '1', '2019-07-16 07:46:50', NULL, '2019-07-16 07:46:50'),
(133, 'Invoice', '0006', '1', '13', 0, 0, '1', '2019-07-16 07:46:50', NULL, '2019-07-16 07:46:50'),
(134, 'Invoice', '0006', '1', '5', 0, 0, '1', '2019-07-16 07:46:50', NULL, '2019-07-16 07:46:50'),
(135, 'Invoice', '0006', '1', '14', 0, 0, '1', '2019-07-16 07:46:50', NULL, '2019-07-16 07:46:50'),
(136, 'Invoice', '0007', '1', '14', 0, 0, '1', '2019-07-16 09:05:58', NULL, '2019-07-16 09:05:58'),
(137, 'Invoice', '0007', '1', '13', 0, 0, '1', '2019-07-16 09:05:58', NULL, '2019-07-16 09:05:58'),
(138, 'Invoice', '0007', '1', '5', 0, 0, '1', '2019-07-16 09:05:58', NULL, '2019-07-16 09:05:58'),
(139, 'Invoice', '0007', '1', '5', 0, 0, '1', '2019-07-16 09:05:58', NULL, '2019-07-16 09:05:58'),
(140, 'Invoice', '0008', '1', '14', 0, 0, '1', '2019-07-16 09:09:00', NULL, '2019-07-16 09:09:00'),
(141, 'Invoice', '0008', '1', '13', 0, 0, '1', '2019-07-16 09:09:00', NULL, '2019-07-16 09:09:00'),
(142, 'Invoice', '0008', '1', '5', 0, 0, '1', '2019-07-16 09:09:00', NULL, '2019-07-16 09:09:00'),
(143, 'Invoice', '0008', '1', '5', 0, 0, '1', '2019-07-16 09:09:00', NULL, '2019-07-16 09:09:00'),
(144, 'Invoice', '0008', '1', '14', 0, 0, '4', '2019-07-16 09:09:25', NULL, '2019-07-16 09:09:25'),
(145, 'Invoice', '0008', '1', '13', 0, 0, '4', '2019-07-16 09:09:25', NULL, '2019-07-16 09:09:25'),
(146, 'Invoice', '0008', '1', '5', 0, 0, '4', '2019-07-16 09:09:25', NULL, '2019-07-16 09:09:25'),
(147, 'Invoice', '0008', '1', '5', 0, 0, '4', '2019-07-16 09:09:25', NULL, '2019-07-16 09:09:25'),
(148, 'Invoice', '0008', '1', '14', 0, 0, '11', '2019-07-16 09:09:29', NULL, '2019-07-16 09:09:29'),
(149, 'Invoice', '0008', '1', '13', 0, 0, '11', '2019-07-16 09:09:29', NULL, '2019-07-16 09:09:29'),
(150, 'Invoice', '0008', '1', '5', 0, 0, '11', '2019-07-16 09:09:29', NULL, '2019-07-16 09:09:29'),
(151, 'Invoice', '0008', '1', '5', 0, 0, '11', '2019-07-16 09:09:29', NULL, '2019-07-16 09:09:29'),
(152, 'Invoice', '0008', '1', '16', 0, 0, '11', '2019-07-16 09:09:29', NULL, '2019-07-16 09:09:29'),
(153, 'Invoice', '0008', '1', '14', 0, 0, '1', '2019-07-16 12:32:31', NULL, '2019-07-16 12:32:31'),
(154, 'Invoice', '0008', '1', '13', 0, 0, '1', '2019-07-16 12:32:31', NULL, '2019-07-16 12:32:31'),
(155, 'Invoice', '0008', '1', '5', 0, 0, '1', '2019-07-16 12:32:31', NULL, '2019-07-16 12:32:31'),
(156, 'Invoice', '0008', '1', '5', 0, 0, '1', '2019-07-16 12:32:31', NULL, '2019-07-16 12:32:31'),
(157, 'Invoice', '0008', '1', '14', 0, 0, '15', '2019-07-16 12:43:15', NULL, '2019-07-16 12:43:15'),
(158, 'Invoice', '0008', '1', '13', 0, 0, '15', '2019-07-16 12:43:15', NULL, '2019-07-16 12:43:15'),
(159, 'Invoice', '0008', '1', '5', 0, 0, '15', '2019-07-16 12:43:15', NULL, '2019-07-16 12:43:15'),
(160, 'Invoice', '0008', '1', '5', 0, 0, '15', '2019-07-16 12:43:15', NULL, '2019-07-16 12:43:15'),
(161, 'Invoice', '0008', '1', '16', 0, 0, '15', '2019-07-16 12:43:15', NULL, '2019-07-16 12:43:15'),
(162, 'Invoice', '0008', '1', '14', 0, 0, '15', '2019-07-16 12:49:12', NULL, '2019-07-16 12:49:12'),
(163, 'Invoice', '0008', '1', '13', 0, 0, '15', '2019-07-16 12:49:12', NULL, '2019-07-16 12:49:12'),
(164, 'Invoice', '0008', '1', '5', 0, 0, '15', '2019-07-16 12:49:12', NULL, '2019-07-16 12:49:12'),
(165, 'Invoice', '0008', '1', '5', 0, 0, '15', '2019-07-16 12:49:12', NULL, '2019-07-16 12:49:12'),
(166, 'Invoice', '0008', '1', '16', 0, 0, '15', '2019-07-16 12:49:12', NULL, '2019-07-16 12:49:12'),
(167, 'Invoice', '0008', '1', '14', 0, 0, '15', '2019-07-16 12:50:41', NULL, '2019-07-16 12:50:41'),
(168, 'Invoice', '0008', '1', '13', 0, 0, '15', '2019-07-16 12:50:41', NULL, '2019-07-16 12:50:41'),
(169, 'Invoice', '0008', '1', '5', 0, 0, '15', '2019-07-16 12:50:41', NULL, '2019-07-16 12:50:41'),
(170, 'Invoice', '0008', '1', '5', 0, 0, '15', '2019-07-16 12:50:41', NULL, '2019-07-16 12:50:41'),
(171, 'Invoice', '0008', '1', '16', 0, 0, '15', '2019-07-16 12:50:41', NULL, '2019-07-16 12:50:41'),
(172, 'Invoice', '0008', '1', '14', 0, 0, '15', '2019-07-16 12:51:05', NULL, '2019-07-16 12:51:05'),
(173, 'Invoice', '0008', '1', '13', 0, 0, '15', '2019-07-16 12:51:05', NULL, '2019-07-16 12:51:05'),
(174, 'Invoice', '0008', '1', '5', 0, 0, '15', '2019-07-16 12:51:05', NULL, '2019-07-16 12:51:05'),
(175, 'Invoice', '0008', '1', '5', 0, 0, '15', '2019-07-16 12:51:05', NULL, '2019-07-16 12:51:05'),
(176, 'Invoice', '0008', '1', '16', 0, 0, '15', '2019-07-16 12:51:05', NULL, '2019-07-16 12:51:05'),
(177, 'Invoice', '0008', '1', '14', 0, 0, '9', '2019-07-16 12:52:57', NULL, '2019-07-16 12:52:57'),
(178, 'Invoice', '0008', '1', '13', 0, 0, '9', '2019-07-16 12:52:57', NULL, '2019-07-16 12:52:57'),
(179, 'Invoice', '0008', '1', '5', 0, 0, '9', '2019-07-16 12:52:57', NULL, '2019-07-16 12:52:57'),
(180, 'Invoice', '0008', '1', '5', 0, 0, '9', '2019-07-16 12:52:57', NULL, '2019-07-16 12:52:57'),
(181, 'Invoice', '0008', '1', '16', 0, 0, '9', '2019-07-16 12:52:57', NULL, '2019-07-16 12:52:57'),
(182, 'Invoice', '0008', '1', '14', 0, 0, '9', '2019-07-16 12:53:45', NULL, '2019-07-16 12:53:45'),
(183, 'Invoice', '0008', '1', '13', 0, 0, '9', '2019-07-16 12:53:45', NULL, '2019-07-16 12:53:45'),
(184, 'Invoice', '0008', '1', '5', 0, 0, '9', '2019-07-16 12:53:45', NULL, '2019-07-16 12:53:45'),
(185, 'Invoice', '0008', '1', '5', 0, 0, '9', '2019-07-16 12:53:45', NULL, '2019-07-16 12:53:45'),
(186, 'Invoice', '0008', '1', '16', 0, 0, '9', '2019-07-16 12:53:45', NULL, '2019-07-16 12:53:45'),
(187, 'Invoice', '0008', '1', '14', 0, 0, '1', '2019-07-16 12:56:13', NULL, '2019-07-16 12:56:13'),
(188, 'Invoice', '0008', '1', '13', 0, 0, '1', '2019-07-16 12:56:13', NULL, '2019-07-16 12:56:13'),
(189, 'Invoice', '0008', '1', '5', 0, 0, '1', '2019-07-16 12:56:13', NULL, '2019-07-16 12:56:13'),
(190, 'Invoice', '0008', '1', '5', 0, 0, '1', '2019-07-16 12:56:13', NULL, '2019-07-16 12:56:13'),
(191, 'Invoice', '0008', '1', '14', 0, 0, '5', '2019-07-16 12:56:29', NULL, '2019-07-16 12:56:29'),
(192, 'Invoice', '0008', '1', '13', 0, 0, '5', '2019-07-16 12:56:29', NULL, '2019-07-16 12:56:29'),
(193, 'Invoice', '0008', '1', '5', 0, 0, '5', '2019-07-16 12:56:29', NULL, '2019-07-16 12:56:29'),
(194, 'Invoice', '0008', '1', '5', 0, 0, '5', '2019-07-16 12:56:29', NULL, '2019-07-16 12:56:29'),
(195, 'Invoice', '0008', '1', '16', 0, 0, '5', '2019-07-16 12:56:29', NULL, '2019-07-16 12:56:29'),
(196, 'Invoice', '0008', '1', '14', 0, 0, '1', '2019-07-16 12:56:37', NULL, '2019-07-16 12:56:37'),
(197, 'Invoice', '0008', '1', '13', 0, 0, '1', '2019-07-16 12:56:37', NULL, '2019-07-16 12:56:37'),
(198, 'Invoice', '0008', '1', '5', 0, 0, '1', '2019-07-16 12:56:37', NULL, '2019-07-16 12:56:37'),
(199, 'Invoice', '0008', '1', '5', 0, 0, '1', '2019-07-16 12:56:37', NULL, '2019-07-16 12:56:37'),
(200, 'Invoice', '0008', '1', '16', 0, 0, '1', '2019-07-16 12:56:37', NULL, '2019-07-16 12:56:37'),
(201, 'Invoice', '0008', '1', '14', 0, 0, '5', '2019-07-16 12:56:46', NULL, '2019-07-16 12:56:46'),
(202, 'Invoice', '0008', '1', '13', 0, 0, '5', '2019-07-16 12:56:46', NULL, '2019-07-16 12:56:46'),
(203, 'Invoice', '0008', '1', '5', 0, 0, '5', '2019-07-16 12:56:46', NULL, '2019-07-16 12:56:46'),
(204, 'Invoice', '0008', '1', '5', 0, 0, '5', '2019-07-16 12:56:46', NULL, '2019-07-16 12:56:46'),
(205, 'Invoice', '0008', '1', '16', 0, 0, '5', '2019-07-16 12:56:46', NULL, '2019-07-16 12:56:46'),
(206, 'Invoice', '0008', '1', '14', 0, 0, '1', '2019-07-16 12:58:47', NULL, '2019-07-16 12:58:47'),
(207, 'Invoice', '0008', '1', '13', 0, 0, '1', '2019-07-16 12:58:47', NULL, '2019-07-16 12:58:47'),
(208, 'Invoice', '0008', '1', '5', 0, 0, '1', '2019-07-16 12:58:47', NULL, '2019-07-16 12:58:47'),
(209, 'Invoice', '0008', '1', '5', 0, 0, '1', '2019-07-16 12:58:47', NULL, '2019-07-16 12:58:47'),
(210, 'Invoice', '0008', '1', '16', 0, 0, '1', '2019-07-16 12:58:47', NULL, '2019-07-16 12:58:47'),
(211, 'Invoice', '0008', '1', '14', 0, 0, '4', '2019-07-16 12:58:55', NULL, '2019-07-16 12:58:55'),
(212, 'Invoice', '0008', '1', '13', 0, 0, '4', '2019-07-16 12:58:55', NULL, '2019-07-16 12:58:55'),
(213, 'Invoice', '0008', '1', '5', 0, 0, '4', '2019-07-16 12:58:55', NULL, '2019-07-16 12:58:55'),
(214, 'Invoice', '0008', '1', '5', 0, 0, '4', '2019-07-16 12:58:55', NULL, '2019-07-16 12:58:55'),
(215, 'Invoice', '0008', '1', '14', 0, 0, '1', '2019-07-17 01:27:39', NULL, '2019-07-17 01:27:39'),
(216, 'Invoice', '0008', '1', '13', 0, 0, '1', '2019-07-17 01:27:39', NULL, '2019-07-17 01:27:39'),
(217, 'Invoice', '0008', '1', '5', 0, 0, '1', '2019-07-17 01:27:39', NULL, '2019-07-17 01:27:39'),
(218, 'Invoice', '0008', '1', '5', 0, 0, '1', '2019-07-17 01:27:39', NULL, '2019-07-17 01:27:39'),
(219, 'Invoice', '0008', '1', '14', 0, 0, '5', '2019-07-17 01:27:56', NULL, '2019-07-17 01:27:56'),
(220, 'Invoice', '0008', '1', '13', 0, 0, '5', '2019-07-17 01:27:56', NULL, '2019-07-17 01:27:56'),
(221, 'Invoice', '0008', '1', '5', 0, 0, '5', '2019-07-17 01:27:56', NULL, '2019-07-17 01:27:56'),
(222, 'Invoice', '0008', '1', '5', 0, 0, '5', '2019-07-17 01:27:56', NULL, '2019-07-17 01:27:56'),
(223, 'Invoice', '0008', '1', '16', 0, 0, '5', '2019-07-17 01:27:56', NULL, '2019-07-17 01:27:56'),
(224, 'Invoice', '0008', '1', '14', 0, 0, '1', '2019-07-17 01:28:00', NULL, '2019-07-17 01:28:00'),
(225, 'Invoice', '0008', '1', '13', 0, 0, '1', '2019-07-17 01:28:00', NULL, '2019-07-17 01:28:00'),
(226, 'Invoice', '0008', '1', '5', 0, 0, '1', '2019-07-17 01:28:00', NULL, '2019-07-17 01:28:00'),
(227, 'Invoice', '0008', '1', '5', 0, 0, '1', '2019-07-17 01:28:00', NULL, '2019-07-17 01:28:00'),
(228, 'Invoice', '0008', '1', '16', 0, 0, '1', '2019-07-17 01:28:00', NULL, '2019-07-17 01:28:00'),
(229, 'Invoice', '0008', '1', '14', 0, 0, '4', '2019-07-17 01:28:10', NULL, '2019-07-17 01:28:10'),
(230, 'Invoice', '0008', '1', '13', 0, 0, '4', '2019-07-17 01:28:10', NULL, '2019-07-17 01:28:10'),
(231, 'Invoice', '0008', '1', '5', 0, 0, '4', '2019-07-17 01:28:10', NULL, '2019-07-17 01:28:10'),
(232, 'Invoice', '0008', '1', '5', 0, 0, '4', '2019-07-17 01:28:10', NULL, '2019-07-17 01:28:10'),
(233, 'Invoice', '0008', '1', '14', 0, 0, '9', '2019-07-17 01:28:46', NULL, '2019-07-17 01:28:46'),
(234, 'Invoice', '0008', '1', '13', 0, 0, '9', '2019-07-17 01:28:46', NULL, '2019-07-17 01:28:46'),
(235, 'Invoice', '0008', '1', '5', 0, 0, '9', '2019-07-17 01:28:46', NULL, '2019-07-17 01:28:46'),
(236, 'Invoice', '0008', '1', '5', 0, 0, '9', '2019-07-17 01:28:46', NULL, '2019-07-17 01:28:46'),
(237, 'Invoice', '0008', '1', '16', 0, 0, '9', '2019-07-17 01:28:46', NULL, '2019-07-17 01:28:46'),
(238, 'Invoice', '0008', '1', '14', 0, 0, '1', '2019-07-17 01:29:08', NULL, '2019-07-17 01:29:08'),
(239, 'Invoice', '0008', '1', '13', 0, 0, '1', '2019-07-17 01:29:08', NULL, '2019-07-17 01:29:08'),
(240, 'Invoice', '0008', '1', '5', 0, 0, '1', '2019-07-17 01:29:08', NULL, '2019-07-17 01:29:08'),
(241, 'Invoice', '0008', '1', '5', 0, 0, '1', '2019-07-17 01:29:08', NULL, '2019-07-17 01:29:08'),
(242, 'Invoice', '0008', '1', '14', 0, 0, '5', '2019-07-17 01:29:24', NULL, '2019-07-17 01:29:24'),
(243, 'Invoice', '0008', '1', '13', 0, 0, '5', '2019-07-17 01:29:24', NULL, '2019-07-17 01:29:24'),
(244, 'Invoice', '0008', '1', '5', 0, 0, '5', '2019-07-17 01:29:24', NULL, '2019-07-17 01:29:24'),
(245, 'Invoice', '0008', '1', '5', 0, 0, '5', '2019-07-17 01:29:24', NULL, '2019-07-17 01:29:24'),
(246, 'Invoice', '0008', '1', '16', 0, 0, '5', '2019-07-17 01:29:24', NULL, '2019-07-17 01:29:24'),
(247, 'Invoice', '0008', '1', '14', 0, 0, '1', '2019-07-17 01:29:27', NULL, '2019-07-17 01:29:27'),
(248, 'Invoice', '0008', '1', '13', 0, 0, '1', '2019-07-17 01:29:27', NULL, '2019-07-17 01:29:27'),
(249, 'Invoice', '0008', '1', '5', 0, 0, '1', '2019-07-17 01:29:27', NULL, '2019-07-17 01:29:27'),
(250, 'Invoice', '0008', '1', '5', 0, 0, '1', '2019-07-17 01:29:27', NULL, '2019-07-17 01:29:27'),
(251, 'Invoice', '0008', '1', '16', 0, 0, '1', '2019-07-17 01:29:27', NULL, '2019-07-17 01:29:27'),
(252, 'Invoice', '0008', '1', '14', 0, 0, '4', '2019-07-17 01:29:36', NULL, '2019-07-17 01:29:36'),
(253, 'Invoice', '0008', '1', '13', 0, 0, '4', '2019-07-17 01:29:36', NULL, '2019-07-17 01:29:36'),
(254, 'Invoice', '0008', '1', '5', 0, 0, '4', '2019-07-17 01:29:36', NULL, '2019-07-17 01:29:36'),
(255, 'Invoice', '0008', '1', '5', 0, 0, '4', '2019-07-17 01:29:36', NULL, '2019-07-17 01:29:36'),
(256, 'Invoice', '0008', '1', '14', 0, 0, '9', '2019-07-17 01:31:29', NULL, '2019-07-17 01:31:29'),
(257, 'Invoice', '0008', '1', '13', 0, 0, '9', '2019-07-17 01:31:29', NULL, '2019-07-17 01:31:29'),
(258, 'Invoice', '0008', '1', '5', 0, 0, '9', '2019-07-17 01:31:29', NULL, '2019-07-17 01:31:29'),
(259, 'Invoice', '0008', '1', '5', 0, 0, '9', '2019-07-17 01:31:29', NULL, '2019-07-17 01:31:29'),
(260, 'Invoice', '0008', '1', '16', 0, 0, '9', '2019-07-17 01:31:29', NULL, '2019-07-17 01:31:29'),
(261, 'Invoice', '0008', '1', '14', 0, 0, '1', '2019-07-17 01:31:48', NULL, '2019-07-17 01:31:48'),
(262, 'Invoice', '0008', '1', '13', 0, 0, '1', '2019-07-17 01:31:48', NULL, '2019-07-17 01:31:48'),
(263, 'Invoice', '0008', '1', '5', 0, 0, '1', '2019-07-17 01:31:48', NULL, '2019-07-17 01:31:48'),
(264, 'Invoice', '0008', '1', '5', 0, 0, '1', '2019-07-17 01:31:48', NULL, '2019-07-17 01:31:48'),
(265, 'Invoice', '0008', '1', '14', 0, 0, '5', '2019-07-17 01:32:01', NULL, '2019-07-17 01:32:01'),
(266, 'Invoice', '0008', '1', '13', 0, 0, '5', '2019-07-17 01:32:01', NULL, '2019-07-17 01:32:01'),
(267, 'Invoice', '0008', '1', '5', 0, 0, '5', '2019-07-17 01:32:01', NULL, '2019-07-17 01:32:01'),
(268, 'Invoice', '0008', '1', '5', 0, 0, '5', '2019-07-17 01:32:01', NULL, '2019-07-17 01:32:01'),
(269, 'Invoice', '0008', '1', '16', 0, 0, '5', '2019-07-17 01:32:01', NULL, '2019-07-17 01:32:01'),
(270, 'Invoice', '0008', '1', '14', 0, 0, '1', '2019-07-17 01:33:06', NULL, '2019-07-17 01:33:06'),
(271, 'Invoice', '0008', '1', '13', 0, 0, '1', '2019-07-17 01:33:06', NULL, '2019-07-17 01:33:06'),
(272, 'Invoice', '0008', '1', '5', 0, 0, '1', '2019-07-17 01:33:06', NULL, '2019-07-17 01:33:06'),
(273, 'Invoice', '0008', '1', '5', 0, 0, '1', '2019-07-17 01:33:06', NULL, '2019-07-17 01:33:06'),
(274, 'Invoice', '0008', '1', '16', 0, 0, '1', '2019-07-17 01:33:06', NULL, '2019-07-17 01:33:06'),
(275, 'Invoice', '0008', '1', '14', 0, 0, '4', '2019-07-17 01:34:08', NULL, '2019-07-17 01:34:08'),
(276, 'Invoice', '0008', '1', '13', 0, 0, '4', '2019-07-17 01:34:08', NULL, '2019-07-17 01:34:08'),
(277, 'Invoice', '0008', '1', '5', 0, 0, '4', '2019-07-17 01:34:08', NULL, '2019-07-17 01:34:08'),
(278, 'Invoice', '0008', '1', '5', 0, 0, '4', '2019-07-17 01:34:08', NULL, '2019-07-17 01:34:08'),
(279, 'Invoice', '0008', '1', '14', 0, 0, '11', '2019-07-17 01:34:11', NULL, '2019-07-17 01:34:11'),
(280, 'Invoice', '0008', '1', '13', 0, 0, '11', '2019-07-17 01:34:11', NULL, '2019-07-17 01:34:11'),
(281, 'Invoice', '0008', '1', '5', 0, 0, '11', '2019-07-17 01:34:11', NULL, '2019-07-17 01:34:11'),
(282, 'Invoice', '0008', '1', '5', 0, 0, '11', '2019-07-17 01:34:11', NULL, '2019-07-17 01:34:11'),
(283, 'Invoice', '0008', '1', '16', 0, 0, '11', '2019-07-17 01:34:11', NULL, '2019-07-17 01:34:11'),
(284, 'Invoice', '0008', '16', '14', 0, 0, '12', '2019-07-17 01:35:34', NULL, '2019-07-17 01:35:34'),
(285, 'Invoice', '0008', '16', '13', 0, 0, '12', '2019-07-17 01:35:34', NULL, '2019-07-17 01:35:34'),
(286, 'Invoice', '0008', '16', '5', 0, 0, '12', '2019-07-17 01:35:34', NULL, '2019-07-17 01:35:34'),
(287, 'Invoice', '0008', '16', '5', 0, 0, '12', '2019-07-17 01:35:34', NULL, '2019-07-17 01:35:34'),
(288, 'Invoice', '0008', '16', '16', 0, 0, '12', '2019-07-17 01:35:34', NULL, '2019-07-17 01:35:34'),
(289, 'Invoice', '0007', '1', '14', 0, 0, '4', '2019-07-17 01:46:25', NULL, '2019-07-17 01:46:25'),
(290, 'Invoice', '0007', '1', '13', 0, 0, '4', '2019-07-17 01:46:25', NULL, '2019-07-17 01:46:25'),
(291, 'Invoice', '0007', '1', '5', 0, 0, '4', '2019-07-17 01:46:25', NULL, '2019-07-17 01:46:25'),
(292, 'Invoice', '0007', '1', '5', 0, 0, '4', '2019-07-17 01:46:25', NULL, '2019-07-17 01:46:25'),
(293, 'Invoice', '0007', '1', '14', 0, 0, '11', '2019-07-17 01:46:30', NULL, '2019-07-17 01:46:30'),
(294, 'Invoice', '0007', '1', '13', 0, 0, '11', '2019-07-17 01:46:30', NULL, '2019-07-17 01:46:30'),
(295, 'Invoice', '0007', '1', '5', 0, 0, '11', '2019-07-17 01:46:30', NULL, '2019-07-17 01:46:30'),
(296, 'Invoice', '0007', '1', '5', 0, 0, '11', '2019-07-17 01:46:30', NULL, '2019-07-17 01:46:30'),
(297, 'Invoice', '0007', '1', '16', 0, 0, '11', '2019-07-17 01:46:30', NULL, '2019-07-17 01:46:30'),
(298, 'Invoice', '0007', '1', '14', 0, 0, '9', '2019-07-17 01:46:46', NULL, '2019-07-17 01:46:46'),
(299, 'Invoice', '0007', '1', '13', 0, 0, '9', '2019-07-17 01:46:46', NULL, '2019-07-17 01:46:46'),
(300, 'Invoice', '0007', '1', '5', 0, 0, '9', '2019-07-17 01:46:46', NULL, '2019-07-17 01:46:46'),
(301, 'Invoice', '0007', '1', '5', 0, 0, '9', '2019-07-17 01:46:46', NULL, '2019-07-17 01:46:46'),
(302, 'Invoice', '0007', '1', '16', 0, 0, '9', '2019-07-17 01:46:46', NULL, '2019-07-17 01:46:46'),
(303, 'Invoice', '0009', '1', '14', 0, 0, '1', '2019-07-17 03:51:48', NULL, '2019-07-17 03:51:48'),
(304, 'Invoice', '0009', '1', '13', 0, 0, '1', '2019-07-17 03:51:48', NULL, '2019-07-17 03:51:48'),
(305, 'Invoice', '0009', '1', '5', 0, 0, '1', '2019-07-17 03:51:48', NULL, '2019-07-17 03:51:48'),
(306, 'Invoice', '0009', '1', '5', 0, 0, '1', '2019-07-17 03:51:48', NULL, '2019-07-17 03:51:48'),
(307, 'Invoice', '0009', '1', '14', 0, 0, '4', '2019-07-17 03:52:12', NULL, '2019-07-17 03:52:12'),
(308, 'Invoice', '0009', '1', '13', 0, 0, '4', '2019-07-17 03:52:12', NULL, '2019-07-17 03:52:12'),
(309, 'Invoice', '0009', '1', '5', 0, 0, '4', '2019-07-17 03:52:12', NULL, '2019-07-17 03:52:12'),
(310, 'Invoice', '0009', '1', '5', 0, 0, '4', '2019-07-17 03:52:12', NULL, '2019-07-17 03:52:12'),
(311, 'Invoice', '0009', '1', '14', 0, 0, '11', '2019-07-17 03:52:16', NULL, '2019-07-17 03:52:16'),
(312, 'Invoice', '0009', '1', '13', 0, 0, '11', '2019-07-17 03:52:16', NULL, '2019-07-17 03:52:16'),
(313, 'Invoice', '0009', '1', '5', 0, 0, '11', '2019-07-17 03:52:16', NULL, '2019-07-17 03:52:16'),
(314, 'Invoice', '0009', '1', '5', 0, 0, '11', '2019-07-17 03:52:16', NULL, '2019-07-17 03:52:16'),
(315, 'Invoice', '0009', '1', '16', 1, 0, '11', '2019-07-17 09:22:29', NULL, '2019-07-17 03:52:29'),
(316, 'Invoice', '0009', '16', '14', 0, 0, '9', '2019-07-17 03:52:43', NULL, '2019-07-17 03:52:43'),
(317, 'Invoice', '0009', '16', '13', 0, 0, '9', '2019-07-17 03:52:43', NULL, '2019-07-17 03:52:43'),
(318, 'Invoice', '0009', '16', '5', 0, 0, '9', '2019-07-17 03:52:43', NULL, '2019-07-17 03:52:43'),
(319, 'Invoice', '0009', '16', '5', 1, 0, '9', '2019-07-17 09:23:22', NULL, '2019-07-17 03:53:22'),
(320, 'Invoice', '0009', '16', '16', 0, 0, '9', '2019-07-17 03:52:43', NULL, '2019-07-17 03:52:43'),
(321, 'Invoice', '0009', '5', '14', 0, 0, '1', '2019-07-17 03:53:55', NULL, '2019-07-17 03:53:55'),
(322, 'Invoice', '0009', '5', '13', 0, 0, '1', '2019-07-17 03:53:55', NULL, '2019-07-17 03:53:55'),
(323, 'Invoice', '0009', '5', '5', 0, 0, '1', '2019-07-17 03:53:55', NULL, '2019-07-17 03:53:55'),
(324, 'Invoice', '0009', '5', '5', 0, 0, '1', '2019-07-17 03:53:55', NULL, '2019-07-17 03:53:55'),
(325, 'Invoice', '0009', '5', '14', 0, 0, '4', '2019-07-17 03:54:24', NULL, '2019-07-17 03:54:24'),
(326, 'Invoice', '0009', '5', '13', 0, 0, '4', '2019-07-17 03:54:24', NULL, '2019-07-17 03:54:24'),
(327, 'Invoice', '0009', '5', '5', 0, 0, '4', '2019-07-17 03:54:24', NULL, '2019-07-17 03:54:24'),
(328, 'Invoice', '0009', '5', '5', 0, 0, '4', '2019-07-17 03:54:24', NULL, '2019-07-17 03:54:24'),
(329, 'Invoice', '0009', '5', '14', 0, 0, '11', '2019-07-17 03:54:28', NULL, '2019-07-17 03:54:28'),
(330, 'Invoice', '0009', '5', '13', 0, 0, '11', '2019-07-17 03:54:28', NULL, '2019-07-17 03:54:28'),
(331, 'Invoice', '0009', '5', '5', 0, 0, '11', '2019-07-17 03:54:28', NULL, '2019-07-17 03:54:28'),
(332, 'Invoice', '0009', '5', '5', 0, 0, '11', '2019-07-17 03:54:28', NULL, '2019-07-17 03:54:28'),
(333, 'Invoice', '0009', '5', '16', 1, 0, '11', '2019-07-17 09:25:07', NULL, '2019-07-17 03:55:07'),
(334, 'Invoice', '0007', '1', '14', 0, 0, '1', '2019-07-18 00:17:45', NULL, '2019-07-18 00:17:45'),
(335, 'Invoice', '0007', '1', '13', 0, 0, '1', '2019-07-18 00:17:45', NULL, '2019-07-18 00:17:45'),
(336, 'Invoice', '0007', '1', '5', 0, 0, '1', '2019-07-18 00:17:45', NULL, '2019-07-18 00:17:45'),
(337, 'Invoice', '0007', '1', '5', 0, 0, '1', '2019-07-18 00:17:45', NULL, '2019-07-18 00:17:45'),
(338, 'Invoice', '0007', '1', '14', 0, 0, '6', '2019-07-18 00:18:10', NULL, '2019-07-18 00:18:10'),
(339, 'Invoice', '0007', '1', '13', 0, 0, '6', '2019-07-18 00:18:10', NULL, '2019-07-18 00:18:10'),
(340, 'Invoice', '0007', '1', '5', 0, 0, '6', '2019-07-18 00:18:10', NULL, '2019-07-18 00:18:10'),
(341, 'Invoice', '0007', '1', '5', 0, 0, '6', '2019-07-18 00:18:10', NULL, '2019-07-18 00:18:10'),
(342, 'Invoice', '0007', '1', '16', 0, 0, '6', '2019-07-18 00:18:10', NULL, '2019-07-18 00:18:10'),
(343, 'Invoice', '0007', '1', '14', 0, 0, '1', '2019-07-18 00:18:26', NULL, '2019-07-18 00:18:26'),
(344, 'Invoice', '0007', '1', '13', 0, 0, '1', '2019-07-18 00:18:26', NULL, '2019-07-18 00:18:26'),
(345, 'Invoice', '0007', '1', '5', 0, 0, '1', '2019-07-18 00:18:26', NULL, '2019-07-18 00:18:26'),
(346, 'Invoice', '0007', '1', '5', 0, 0, '1', '2019-07-18 00:18:26', NULL, '2019-07-18 00:18:26'),
(347, 'Invoice', '0007', '1', '14', 0, 0, '4', '2019-07-18 00:18:37', NULL, '2019-07-18 00:18:37'),
(348, 'Invoice', '0007', '1', '13', 0, 0, '4', '2019-07-18 00:18:37', NULL, '2019-07-18 00:18:37'),
(349, 'Invoice', '0007', '1', '5', 0, 0, '4', '2019-07-18 00:18:37', NULL, '2019-07-18 00:18:37'),
(350, 'Invoice', '0007', '1', '5', 0, 0, '4', '2019-07-18 00:18:37', NULL, '2019-07-18 00:18:37'),
(351, 'Invoice', '0007', '1', '14', 0, 0, '11', '2019-07-18 00:18:40', NULL, '2019-07-18 00:18:40'),
(352, 'Invoice', '0007', '1', '13', 0, 0, '11', '2019-07-18 00:18:40', NULL, '2019-07-18 00:18:40'),
(353, 'Invoice', '0007', '1', '5', 0, 0, '11', '2019-07-18 00:18:40', NULL, '2019-07-18 00:18:40'),
(354, 'Invoice', '0007', '1', '5', 0, 0, '11', '2019-07-18 00:18:40', NULL, '2019-07-18 00:18:40'),
(355, 'Invoice', '0007', '1', '16', 1, 0, '11', '2019-07-18 05:49:46', NULL, '2019-07-18 00:19:46'),
(356, 'Invoice', '0007', '16', '14', 0, 0, '12', '2019-07-18 00:22:41', NULL, '2019-07-18 00:22:41'),
(357, 'Invoice', '0007', '16', '13', 0, 0, '12', '2019-07-18 00:22:41', NULL, '2019-07-18 00:22:41'),
(358, 'Invoice', '0007', '16', '5', 0, 0, '12', '2019-07-18 00:22:41', NULL, '2019-07-18 00:22:41'),
(359, 'Invoice', '0007', '16', '5', 0, 0, '12', '2019-07-18 00:22:41', NULL, '2019-07-18 00:22:41'),
(360, 'Invoice', '0007', '16', '16', 0, 0, '12', '2019-07-18 00:22:41', NULL, '2019-07-18 00:22:41'),
(361, 'Invoice', '0010', '1', '14', 0, 0, '1', '2019-07-18 00:34:03', NULL, '2019-07-18 00:34:03'),
(362, 'Invoice', '0010', '1', '13', 0, 0, '1', '2019-07-18 00:34:03', NULL, '2019-07-18 00:34:03'),
(363, 'Invoice', '0010', '1', '5', 0, 0, '1', '2019-07-18 00:34:03', NULL, '2019-07-18 00:34:03'),
(364, 'Invoice', '0010', '1', '5', 0, 0, '1', '2019-07-18 00:34:03', NULL, '2019-07-18 00:34:03'),
(365, 'Invoice', '0010', '1', '14', 0, 0, '4', '2019-07-18 00:34:13', NULL, '2019-07-18 00:34:13'),
(366, 'Invoice', '0010', '1', '13', 0, 0, '4', '2019-07-18 00:34:13', NULL, '2019-07-18 00:34:13'),
(367, 'Invoice', '0010', '1', '5', 0, 0, '4', '2019-07-18 00:34:13', NULL, '2019-07-18 00:34:13'),
(368, 'Invoice', '0010', '1', '5', 0, 0, '4', '2019-07-18 00:34:13', NULL, '2019-07-18 00:34:13'),
(369, 'Invoice', '0010', '1', '14', 0, 0, '9', '2019-07-18 00:34:30', NULL, '2019-07-18 00:34:30'),
(370, 'Invoice', '0010', '1', '13', 0, 0, '9', '2019-07-18 00:34:30', NULL, '2019-07-18 00:34:30'),
(371, 'Invoice', '0010', '1', '5', 0, 0, '9', '2019-07-18 00:34:30', NULL, '2019-07-18 00:34:30'),
(372, 'Invoice', '0010', '1', '5', 0, 0, '9', '2019-07-18 00:34:30', NULL, '2019-07-18 00:34:30'),
(373, 'Invoice', '0010', '1', '16', 0, 0, '9', '2019-07-18 00:34:30', NULL, '2019-07-18 00:34:30'),
(374, 'Invoice', '0010', '1', '14', 0, 0, '1', '2019-07-18 00:35:21', NULL, '2019-07-18 00:35:21'),
(375, 'Invoice', '0010', '1', '13', 0, 0, '1', '2019-07-18 00:35:21', NULL, '2019-07-18 00:35:21'),
(376, 'Invoice', '0010', '1', '5', 0, 0, '1', '2019-07-18 00:35:21', NULL, '2019-07-18 00:35:21'),
(377, 'Invoice', '0010', '1', '5', 0, 0, '1', '2019-07-18 00:35:21', NULL, '2019-07-18 00:35:21'),
(378, 'Invoice', '0010', '1', '14', 0, 0, '4', '2019-07-18 00:35:30', NULL, '2019-07-18 00:35:30'),
(379, 'Invoice', '0010', '1', '13', 0, 0, '4', '2019-07-18 00:35:30', NULL, '2019-07-18 00:35:30'),
(380, 'Invoice', '0010', '1', '5', 0, 0, '4', '2019-07-18 00:35:30', NULL, '2019-07-18 00:35:30'),
(381, 'Invoice', '0010', '1', '5', 0, 0, '4', '2019-07-18 00:35:30', NULL, '2019-07-18 00:35:30'),
(382, 'Invoice', '0010', '1', '14', 0, 0, '11', '2019-07-18 00:35:51', NULL, '2019-07-18 00:35:51'),
(383, 'Invoice', '0010', '1', '13', 0, 0, '11', '2019-07-18 00:35:51', NULL, '2019-07-18 00:35:51'),
(384, 'Invoice', '0010', '1', '5', 0, 0, '11', '2019-07-18 00:35:51', NULL, '2019-07-18 00:35:51'),
(385, 'Invoice', '0010', '1', '5', 0, 0, '11', '2019-07-18 00:35:51', NULL, '2019-07-18 00:35:51'),
(386, 'Invoice', '0010', '1', '16', 0, 0, '11', '2019-07-18 00:35:51', NULL, '2019-07-18 00:35:51'),
(387, 'Invoice', '0010', '16', '14', 0, 0, '9', '2019-07-18 00:36:18', NULL, '2019-07-18 00:36:18'),
(388, 'Invoice', '0010', '16', '13', 0, 0, '9', '2019-07-18 00:36:18', NULL, '2019-07-18 00:36:18'),
(389, 'Invoice', '0010', '16', '5', 0, 0, '9', '2019-07-18 00:36:18', NULL, '2019-07-18 00:36:18'),
(390, 'Invoice', '0010', '16', '5', 0, 0, '9', '2019-07-18 00:36:18', NULL, '2019-07-18 00:36:18'),
(391, 'Invoice', '0010', '16', '16', 0, 0, '9', '2019-07-18 00:36:18', NULL, '2019-07-18 00:36:18'),
(392, 'Invoice', '0010', '1', '14', 0, 0, '1', '2019-07-18 00:36:56', NULL, '2019-07-18 00:36:56'),
(393, 'Invoice', '0010', '1', '13', 0, 0, '1', '2019-07-18 00:36:56', NULL, '2019-07-18 00:36:56'),
(394, 'Invoice', '0010', '1', '5', 0, 0, '1', '2019-07-18 00:36:56', NULL, '2019-07-18 00:36:56'),
(395, 'Invoice', '0010', '1', '5', 0, 0, '1', '2019-07-18 00:36:56', NULL, '2019-07-18 00:36:56'),
(396, 'Invoice', '0010', '1', '14', 0, 0, '1', '2019-07-18 00:38:52', NULL, '2019-07-18 00:38:52'),
(397, 'Invoice', '0010', '1', '13', 0, 0, '1', '2019-07-18 00:38:52', NULL, '2019-07-18 00:38:52'),
(398, 'Invoice', '0010', '1', '5', 0, 0, '1', '2019-07-18 00:38:52', NULL, '2019-07-18 00:38:52'),
(399, 'Invoice', '0010', '1', '5', 0, 0, '1', '2019-07-18 00:38:52', NULL, '2019-07-18 00:38:52'),
(400, 'Invoice', '0010', '1', '14', 0, 0, '4', '2019-07-18 00:39:02', NULL, '2019-07-18 00:39:02'),
(401, 'Invoice', '0010', '1', '13', 0, 0, '4', '2019-07-18 00:39:02', NULL, '2019-07-18 00:39:02'),
(402, 'Invoice', '0010', '1', '5', 0, 0, '4', '2019-07-18 00:39:02', NULL, '2019-07-18 00:39:02'),
(403, 'Invoice', '0010', '1', '5', 0, 0, '4', '2019-07-18 00:39:02', NULL, '2019-07-18 00:39:02'),
(404, 'Invoice', '0010', '1', '14', 0, 0, '11', '2019-07-18 00:39:06', NULL, '2019-07-18 00:39:06'),
(405, 'Invoice', '0010', '1', '13', 0, 0, '11', '2019-07-18 00:39:06', NULL, '2019-07-18 00:39:06'),
(406, 'Invoice', '0010', '1', '5', 0, 0, '11', '2019-07-18 00:39:06', NULL, '2019-07-18 00:39:06'),
(407, 'Invoice', '0010', '1', '5', 0, 0, '11', '2019-07-18 00:39:06', NULL, '2019-07-18 00:39:06'),
(408, 'Invoice', '0010', '1', '16', 0, 0, '11', '2019-07-18 00:39:06', NULL, '2019-07-18 00:39:06'),
(409, 'Invoice', '0010', '1', '14', 0, 0, '9', '2019-07-18 00:39:43', NULL, '2019-07-18 00:39:43'),
(410, 'Invoice', '0010', '1', '13', 0, 0, '9', '2019-07-18 00:39:43', NULL, '2019-07-18 00:39:43'),
(411, 'Invoice', '0010', '1', '5', 0, 0, '9', '2019-07-18 00:39:43', NULL, '2019-07-18 00:39:43'),
(412, 'Invoice', '0010', '1', '5', 0, 0, '9', '2019-07-18 00:39:43', NULL, '2019-07-18 00:39:43'),
(413, 'Invoice', '0010', '1', '16', 0, 0, '9', '2019-07-18 00:39:43', NULL, '2019-07-18 00:39:43'),
(414, 'Invoice', '0010', '1', '14', 0, 0, '1', '2019-07-18 00:40:15', NULL, '2019-07-18 00:40:15'),
(415, 'Invoice', '0010', '1', '13', 0, 0, '1', '2019-07-18 00:40:15', NULL, '2019-07-18 00:40:15'),
(416, 'Invoice', '0010', '1', '5', 0, 0, '1', '2019-07-18 00:40:15', NULL, '2019-07-18 00:40:15'),
(417, 'Invoice', '0010', '1', '5', 0, 0, '1', '2019-07-18 00:40:15', NULL, '2019-07-18 00:40:15'),
(418, 'Invoice', '0010', '1', '14', 0, 0, '4', '2019-07-18 00:40:26', NULL, '2019-07-18 00:40:26'),
(419, 'Invoice', '0010', '1', '13', 0, 0, '4', '2019-07-18 00:40:26', NULL, '2019-07-18 00:40:26'),
(420, 'Invoice', '0010', '1', '5', 0, 0, '4', '2019-07-18 00:40:26', NULL, '2019-07-18 00:40:26'),
(421, 'Invoice', '0010', '1', '5', 0, 0, '4', '2019-07-18 00:40:26', NULL, '2019-07-18 00:40:26'),
(422, 'Invoice', '0010', '1', '14', 0, 0, '11', '2019-07-18 00:40:41', NULL, '2019-07-18 00:40:41'),
(423, 'Invoice', '0010', '1', '13', 0, 0, '11', '2019-07-18 00:40:41', NULL, '2019-07-18 00:40:41'),
(424, 'Invoice', '0010', '1', '5', 0, 0, '11', '2019-07-18 00:40:41', NULL, '2019-07-18 00:40:41'),
(425, 'Invoice', '0010', '1', '5', 0, 0, '11', '2019-07-18 00:40:41', NULL, '2019-07-18 00:40:41'),
(426, 'Invoice', '0010', '1', '16', 0, 0, '11', '2019-07-18 00:40:41', NULL, '2019-07-18 00:40:41'),
(427, 'Invoice', '0010', '16', '14', 0, 0, '12', '2019-07-18 00:40:57', NULL, '2019-07-18 00:40:57'),
(428, 'Invoice', '0010', '16', '13', 0, 0, '12', '2019-07-18 00:40:57', NULL, '2019-07-18 00:40:57'),
(429, 'Invoice', '0010', '16', '5', 0, 0, '12', '2019-07-18 00:40:57', NULL, '2019-07-18 00:40:57'),
(430, 'Invoice', '0010', '16', '5', 0, 0, '12', '2019-07-18 00:40:57', NULL, '2019-07-18 00:40:57'),
(431, 'Invoice', '0010', '16', '16', 0, 0, '12', '2019-07-18 00:40:57', NULL, '2019-07-18 00:40:57'),
(432, 'Invoice', '0010', '1', '14', 0, 0, '8', '2019-07-18 00:41:23', NULL, '2019-07-18 00:41:23'),
(433, 'Invoice', '0010', '1', '13', 0, 0, '8', '2019-07-18 00:41:23', NULL, '2019-07-18 00:41:23'),
(434, 'Invoice', '0010', '1', '5', 0, 0, '8', '2019-07-18 00:41:23', NULL, '2019-07-18 00:41:23'),
(435, 'Invoice', '0010', '1', '5', 0, 0, '8', '2019-07-18 00:41:23', NULL, '2019-07-18 00:41:23'),
(436, 'Invoice', '0010', '1', '16', 0, 0, '8', '2019-07-18 00:41:23', NULL, '2019-07-18 00:41:23'),
(437, 'Invoice', '0009', '16', '14', 0, 0, '12', '2019-07-18 01:25:34', NULL, '2019-07-18 01:25:34'),
(438, 'Invoice', '0009', '16', '13', 0, 0, '12', '2019-07-18 01:25:34', NULL, '2019-07-18 01:25:34'),
(439, 'Invoice', '0009', '16', '5', 0, 0, '12', '2019-07-18 01:25:34', NULL, '2019-07-18 01:25:34'),
(440, 'Invoice', '0009', '16', '5', 0, 0, '12', '2019-07-18 01:25:34', NULL, '2019-07-18 01:25:34'),
(441, 'Invoice', '0009', '16', '16', 0, 0, '12', '2019-07-18 01:25:34', NULL, '2019-07-18 01:25:34'),
(442, 'Invoice', '0011', '1', '14', 0, 0, '1', '2019-07-18 03:58:16', NULL, '2019-07-18 03:58:16'),
(443, 'Invoice', '0011', '1', '13', 0, 0, '1', '2019-07-18 03:58:16', NULL, '2019-07-18 03:58:16'),
(444, 'Invoice', '0011', '1', '5', 0, 0, '1', '2019-07-18 03:58:16', NULL, '2019-07-18 03:58:16'),
(445, 'Invoice', '0011', '1', '5', 0, 0, '1', '2019-07-18 03:58:16', NULL, '2019-07-18 03:58:16'),
(446, 'Invoice', '0011', '1', '14', 0, 0, '4', '2019-07-18 03:59:53', NULL, '2019-07-18 03:59:53'),
(447, 'Invoice', '0011', '1', '13', 0, 0, '4', '2019-07-18 03:59:53', NULL, '2019-07-18 03:59:53'),
(448, 'Invoice', '0011', '1', '5', 0, 0, '4', '2019-07-18 03:59:53', NULL, '2019-07-18 03:59:53'),
(449, 'Invoice', '0011', '1', '5', 0, 0, '4', '2019-07-18 03:59:53', NULL, '2019-07-18 03:59:53'),
(450, 'Invoice', '0012', '1', '14', 0, 0, '1', '2019-07-22 00:46:38', NULL, '2019-07-22 00:46:38'),
(451, 'Invoice', '0012', '1', '13', 0, 0, '1', '2019-07-22 00:46:38', NULL, '2019-07-22 00:46:38'),
(452, 'Invoice', '0012', '1', '5', 0, 0, '1', '2019-07-22 00:46:38', NULL, '2019-07-22 00:46:38'),
(453, 'Invoice', '0012', '1', '5', 0, 0, '1', '2019-07-22 00:46:38', NULL, '2019-07-22 00:46:38'),
(454, 'Invoice', '0013', '1', '14', 0, 0, '1', '2019-07-22 01:17:05', NULL, '2019-07-22 01:17:05'),
(455, 'Invoice', '0013', '1', '13', 0, 0, '1', '2019-07-22 01:17:05', NULL, '2019-07-22 01:17:05'),
(456, 'Invoice', '0013', '1', '5', 0, 0, '1', '2019-07-22 01:17:05', NULL, '2019-07-22 01:17:05'),
(457, 'Invoice', '0013', '1', '5', 0, 0, '1', '2019-07-22 01:17:05', NULL, '2019-07-22 01:17:05'),
(458, 'Invoice', '0014', '1', '14', 0, 0, '1', '2019-07-22 01:18:10', NULL, '2019-07-22 01:18:10'),
(459, 'Invoice', '0014', '1', '13', 0, 0, '1', '2019-07-22 01:18:10', NULL, '2019-07-22 01:18:10'),
(460, 'Invoice', '0014', '1', '5', 0, 0, '1', '2019-07-22 01:18:10', NULL, '2019-07-22 01:18:10'),
(461, 'Invoice', '0014', '1', '14', 0, 0, '1', '2019-07-22 01:18:10', NULL, '2019-07-22 01:18:10'),
(462, 'Invoice', '0015', '1', '14', 0, 0, '1', '2019-07-22 01:19:03', NULL, '2019-07-22 01:19:03'),
(463, 'Invoice', '0015', '1', '13', 0, 0, '1', '2019-07-22 01:19:03', NULL, '2019-07-22 01:19:03'),
(464, 'Invoice', '0015', '1', '5', 0, 0, '1', '2019-07-22 01:19:03', NULL, '2019-07-22 01:19:03'),
(465, 'Invoice', '0015', '1', '14', 0, 0, '1', '2019-07-22 01:19:03', NULL, '2019-07-22 01:19:03'),
(466, 'Invoice', '0016', '1', '14', 0, 0, '1', '2019-07-27 03:27:03', NULL, '2019-07-27 03:27:03'),
(467, 'Invoice', '0016', '1', '13', 0, 0, '1', '2019-07-27 03:27:03', NULL, '2019-07-27 03:27:03'),
(468, 'Invoice', '0016', '1', '5', 0, 0, '1', '2019-07-27 03:27:03', NULL, '2019-07-27 03:27:03'),
(469, 'Invoice', '0016', '1', '5', 0, 0, '1', '2019-07-27 03:27:03', NULL, '2019-07-27 03:27:03');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_payment`
--

CREATE TABLE `tbl_payment` (
  `id` int(11) NOT NULL,
  `invoice_id` varchar(50) NOT NULL,
  `payment_date` date NOT NULL,
  `payment_mode` varchar(100) NOT NULL,
  `paid_amount` decimal(20,2) NOT NULL,
  `due_amount` decimal(20,2) DEFAULT NULL,
  `clearence_date` date DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `status` varchar(20) NOT NULL,
  `is_delete` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_payment`
--

INSERT INTO `tbl_payment` (`id`, `invoice_id`, `payment_date`, `payment_mode`, `paid_amount`, `due_amount`, `clearence_date`, `created_by`, `created_at`, `updated_at`, `updated_by`, `status`, `is_delete`) VALUES
(1, '0001', '2019-07-13', 'Credit-card', '1000.00', '21000.00', NULL, 1, '2019-07-12 12:18:34', '2019-07-12 12:18:34', NULL, '8', 0),
(2, '0001', '2019-07-12', 'Check', '1000.00', '20000.00', NULL, 1, '2019-07-12 12:23:28', '2019-07-12 12:23:28', NULL, '8', 0),
(3, '0001', '2019-07-13', 'Check', '20000.00', '0.00', NULL, 1, '2019-07-12 12:23:57', '2019-07-12 12:23:57', NULL, '10', 0),
(4, '0010', '2019-07-18', 'Credit-card', '20000.00', '30000.00', NULL, 1, '2019-07-18 00:41:23', '2019-07-18 00:41:23', NULL, '8', 0),
(5, '0010', '2019-07-20', 'Check', '15000.00', '15000.00', NULL, 1, '2019-07-18 00:41:58', '2019-07-18 00:41:58', NULL, '8', 0),
(6, '0010', '2019-07-24', 'Credit-card', '14100.00', '900.00', NULL, 1, '2019-07-18 00:42:16', '2019-07-18 00:42:16', NULL, '8', 0),
(7, '0010', '2019-07-18', 'Debit-card', '450.25', '449.75', NULL, 1, '2019-07-18 00:42:32', '2019-07-18 00:42:32', NULL, '8', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_permission`
--

CREATE TABLE `tbl_permission` (
  `id` int(11) NOT NULL,
  `group_id` int(11) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  `controller_name` varchar(50) NOT NULL,
  `action_name` varchar(50) NOT NULL,
  `status` smallint(2) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_permission`
--

INSERT INTO `tbl_permission` (`id`, `group_id`, `name`, `controller_name`, `action_name`, `status`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(2, NULL, 'test', 'test', 'test', 1, 1, '2019-06-10 05:20:31', NULL, '2019-06-10 05:55:32'),
(3, 2, 'test', 'test', 'test', 1, 1, '2019-06-12 09:02:35', NULL, '2019-06-12 03:32:35'),
(5, 5, 'Roles', 'roles', 'roles', 1, 1, '2019-07-04 10:57:07', NULL, '2019-07-04 05:27:07'),
(6, 5, 'Permission', 'PermissionController', 'permission', 1, 1, '2019-07-04 10:57:15', NULL, '2019-07-04 05:27:15'),
(7, 5, 'Permission Group', 'PermissionGroupController', 'permission_group', 1, 1, '2019-07-04 10:57:22', NULL, '2019-07-04 05:27:22'),
(8, 5, 'Role Permission', 'RolePermissionController', 'role_permission', 1, 1, '2019-07-04 10:57:48', NULL, '2019-07-04 05:27:48'),
(9, 5, 'User', 'UserController', 'user', 1, 1, '2019-07-04 10:57:58', NULL, '2019-07-04 05:27:58'),
(10, 6, 'Warehouse', 'WarehouseController', 'warehouse', 1, 1, '2019-07-04 10:58:38', NULL, '2019-07-04 05:28:38'),
(11, 7, 'Revenue', 'Revenue', 'revenue', 1, 1, '2019-07-04 10:58:54', NULL, '2019-07-04 05:28:54'),
(12, 8, 'Client', 'ClientController', 'client', 1, 1, '2019-07-04 10:59:09', NULL, '2019-07-04 05:29:09'),
(13, 9, 'Invoice', 'InvoiceController', 'invoice', 1, 1, '2019-07-04 10:59:16', NULL, '2019-07-04 05:29:16'),
(14, 10, 'Ticket', 'TicketController', 'ticket', 1, 1, '2019-07-06 06:19:32', NULL, '2019-07-06 06:19:32'),
(15, 9, 'Print Invoice', 'InvoiceController', 'print_invoice', 1, 1, '2019-07-08 05:17:07', NULL, '2019-07-08 05:17:07'),
(16, 9, 'Follow up', 'InvoiceController', 'follow_up', 1, 1, '2019-07-08 05:17:32', NULL, '2019-07-08 05:17:32'),
(17, 9, 'Payment', 'InvoiceController', 'payment', 1, 1, '2019-07-08 05:18:30', NULL, '2019-07-08 05:18:30'),
(18, 9, 'Payment History', 'InvoiceController', 'payment_history', 1, 1, '2019-07-08 05:18:55', NULL, '2019-07-08 05:18:55'),
(19, 9, 'Cancel', 'InvoiceController', 'cancel', 1, 1, '2019-07-08 05:19:16', NULL, '2019-07-08 05:19:16'),
(20, 9, 'Approve Invoice', 'InvoiceController', 'approve_invoice', 1, 1, '2019-07-08 05:19:37', NULL, '2019-07-08 05:19:37'),
(21, 9, 'Modify Status', 'InvoiceController', 'modify_status', 1, 1, '2019-07-08 05:19:59', NULL, '2019-07-08 05:19:59'),
(22, 8, 'Delete', 'ClientController', 'delete', 1, 1, '2019-07-08 13:10:51', NULL, '2019-07-08 07:40:51'),
(23, 9, 'Create', 'InvoiceController', 'create', 1, 1, '2019-07-08 05:51:35', NULL, '2019-07-08 05:51:35'),
(24, 9, 'Edit', 'InvoiceController', 'edit', 1, 1, '2019-07-08 07:11:01', NULL, '2019-07-08 07:11:01'),
(25, 9, 'Reject', 'InvoiceController', 'reject_invoice', 1, 1, '2019-07-08 12:49:49', NULL, '2019-07-08 07:19:49'),
(26, 6, 'Create', 'WarehouseController', 'create', 1, 1, '2019-07-08 07:33:47', NULL, '2019-07-08 07:33:47'),
(27, 6, 'Delete', 'WarehouseController', 'delete', 1, 1, '2019-07-08 07:35:34', NULL, '2019-07-08 07:35:34'),
(28, 6, 'Edit', 'WarehouseController', 'edit', 1, 1, '2019-07-08 07:35:57', NULL, '2019-07-08 07:35:57'),
(29, 6, 'View', 'WarehouseController', 'view', 1, 1, '2019-07-08 07:36:23', NULL, '2019-07-08 07:36:23'),
(30, 5, 'Create', 'UserController', 'create', 1, 1, '2019-07-08 07:36:49', NULL, '2019-07-08 07:36:49'),
(31, 5, 'Edit', 'UserController', 'edit', 1, 1, '2019-07-08 07:37:15', NULL, '2019-07-08 07:37:15'),
(32, 5, 'Delete', 'UserController', 'delete', 1, 1, '2019-07-08 07:37:34', NULL, '2019-07-08 07:37:34'),
(33, 7, 'Create', 'RevenueController', 'create', 1, 1, '2019-07-08 07:38:06', NULL, '2019-07-08 07:38:06'),
(34, 7, 'Delete', 'RevenueController', 'delete', 1, 1, '2019-07-08 07:38:32', NULL, '2019-07-08 07:38:32'),
(35, 7, 'Edit', 'RevenueController', 'edit', 1, 1, '2019-07-08 07:38:57', NULL, '2019-07-08 07:38:57'),
(36, 8, 'Create', 'ClientController', 'create', 1, 1, '2019-07-08 07:39:27', NULL, '2019-07-08 07:39:27'),
(37, 8, 'Edit', 'ClientController', 'edit', 1, 1, '2019-07-08 07:39:52', NULL, '2019-07-08 07:39:52'),
(38, 8, 'Delete', 'ClientController', 'delete', 1, 1, '2019-07-08 07:40:14', NULL, '2019-07-08 07:40:14'),
(39, 10, 'Create', 'TicketController', 'create', 1, 1, '2019-07-08 07:41:37', NULL, '2019-07-08 07:41:37'),
(40, 10, 'Delete', 'TicketController', 'delete', 1, 1, '2019-07-08 07:42:00', NULL, '2019-07-08 07:42:00'),
(41, 10, 'Edit', 'TicketController', 'edit', 1, 1, '2019-07-08 07:42:27', NULL, '2019-07-08 07:42:27'),
(43, 12, 'Profile', 'HomeController', 'profile', 1, 1, '2019-07-09 02:16:19', NULL, '2019-07-09 02:16:19'),
(44, 13, 'Company Wings', 'CompanyWingsController', 'company_wings', 1, 1, '2019-07-10 07:01:32', NULL, '2019-07-10 07:01:32'),
(45, 13, 'Create', 'CompanyWingsController', 'create', 1, 1, '2019-07-10 07:02:02', NULL, '2019-07-10 07:02:02'),
(46, 13, 'Edit', 'CompanyWingsController', 'edit', 1, 1, '2019-07-10 07:02:23', NULL, '2019-07-10 07:02:23'),
(47, 13, 'Delete', 'CompanyWingsController', 'delete', 1, 1, '2019-07-10 07:02:41', NULL, '2019-07-10 07:02:41'),
(48, 9, 'View', 'InvoiceController', 'view', 1, 1, '2019-07-12 07:16:55', NULL, '2019-07-12 07:16:55'),
(49, 9, 'Update Spoc', 'InvoiceController', 'update_spoc', 1, 1, '2019-07-12 07:26:35', NULL, '2019-07-12 07:26:35'),
(50, 9, 'Approve At Client', 'InvoiceController', 'approvetoclient', 1, 1, '2019-07-12 12:17:18', NULL, '2019-07-12 12:17:18'),
(51, 5, 'View', 'UserController', 'view', 1, 1, '2019-07-13 01:34:28', NULL, '2019-07-13 01:34:28'),
(52, 8, 'View', 'ClientController', 'view', 1, 1, '2019-07-13 01:52:47', NULL, '2019-07-13 01:52:47'),
(54, 9, 'Send To Client', 'InvoiceController', 'sendtoclient', 1, 1, '2019-07-16 06:04:58', NULL, '2019-07-16 06:04:58'),
(55, 9, 'Delete', 'InvoiceController', 'delete', 1, 1, '2019-07-17 01:42:12', NULL, '2019-07-17 01:42:12');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_permission_group`
--

CREATE TABLE `tbl_permission_group` (
  `id` int(11) NOT NULL,
  `permission_name` varchar(100) NOT NULL,
  `status` smallint(2) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_permission_group`
--

INSERT INTO `tbl_permission_group` (`id`, `permission_name`, `status`, `updated_at`, `created_at`) VALUES
(5, 'User Management', 1, '2019-07-02 06:01:37', '2019-07-02 06:01:37'),
(6, 'Warehouse Management', 1, '2019-07-04 05:05:43', '2019-07-04 05:05:43'),
(7, 'Revenue Master', 1, '2019-07-04 05:05:57', '2019-07-04 05:05:57'),
(8, 'Client', 1, '2019-07-04 05:06:05', '2019-07-04 05:06:05'),
(9, 'Invoice Management', 1, '2019-07-04 05:06:19', '2019-07-04 05:06:19'),
(10, 'Ticket', 1, '2019-07-06 06:18:57', '2019-07-06 06:18:57'),
(12, 'Home Controller', 1, '2019-07-09 02:15:36', '2019-07-09 02:15:36'),
(13, 'Company Wings', 1, '2019-07-10 07:00:54', '2019-07-10 07:00:54');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_process`
--

CREATE TABLE `tbl_process` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `status` int(11) NOT NULL,
  `created_by` timestamp NULL DEFAULT NULL,
  `updated_by` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_process`
--

INSERT INTO `tbl_process` (`id`, `name`, `status`, `created_by`, `updated_by`) VALUES
(1, 'Pending', 1, NULL, NULL),
(2, 'Edit', 1, NULL, NULL),
(3, 'Delete', 1, NULL, NULL),
(4, 'Approved', 1, NULL, NULL),
(5, 'Cancel', 1, NULL, NULL),
(6, 'Reject', 1, NULL, NULL),
(7, 'Active', 1, NULL, NULL),
(8, 'PendingPayment', 1, NULL, NULL),
(9, 'ModifyRequest', 1, NULL, NULL),
(10, 'AllClear', 1, NULL, NULL),
(11, 'ApproveATFinance', 1, NULL, NULL),
(12, 'ApproveAtClient', 1, NULL, NULL),
(13, 'PaymentHistory', 1, NULL, NULL),
(14, 'PrintInvoice', 1, NULL, NULL),
(15, 'FollowUp', 1, NULL, NULL),
(16, 'UpdateSpoc', 1, NULL, NULL),
(17, 'TicketForApprovedFromClient', 1, NULL, NULL),
(18, 'TicketForDues', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_process_mapping`
--

CREATE TABLE `tbl_process_mapping` (
  `id` int(11) NOT NULL,
  `process_id` int(11) NOT NULL,
  `abbr` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_process_mapping`
--

INSERT INTO `tbl_process_mapping` (`id`, `process_id`, `abbr`, `status`) VALUES
(3, 1, 3, 1),
(4, 1, 2, 1),
(5, 1, 6, 1),
(6, 4, 11, 1),
(7, 5, 7, 1),
(8, 7, 1, 1),
(9, 8, 10, 1),
(10, 9, 2, 1),
(11, 6, 2, 1),
(12, 2, 1, 1),
(14, 1, 4, 1),
(15, 1, 5, 1),
(20, 11, 12, 1),
(21, 12, 8, 1),
(22, 12, 13, 1),
(23, 12, 14, 1),
(24, 12, 15, 1),
(25, 9, 9, 1),
(28, 4, 9, 1),
(30, 2, 2, 1),
(31, 2, 9, 1),
(32, 11, 9, 1),
(33, 8, 10, 1),
(34, 8, 13, 1),
(35, 8, 14, 1),
(36, 8, 15, 1),
(37, 8, 8, 0),
(38, 10, 13, 1),
(39, 10, 14, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_process_step_history`
--

CREATE TABLE `tbl_process_step_history` (
  `id` int(11) NOT NULL,
  `process_step` int(11) NOT NULL,
  `process_time` date DEFAULT NULL,
  `submit_time` date NOT NULL,
  `invoice_id` varchar(150) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_process_step_history`
--

INSERT INTO `tbl_process_step_history` (`id`, `process_step`, `process_time`, `submit_time`, `invoice_id`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 1, '2019-07-16', '2019-07-16', '0001', 1, '2019-07-16 11:11:59', '2019-07-16 05:41:59'),
(2, 1, '2019-07-16', '2019-07-16', '0002', 1, '2019-07-16 11:11:21', '2019-07-16 05:41:21'),
(3, 4, '2019-07-16', '2019-07-16', '0002', 5, '2019-07-16 11:19:25', '2019-07-16 05:49:25'),
(4, 1, '2019-07-16', '2019-07-16', '0001', 1, '2019-07-16 11:19:53', '2019-07-16 05:49:53'),
(5, 1, '2019-07-16', '2019-07-16', '0003', 1, '2019-07-16 11:44:46', '2019-07-16 06:14:46'),
(6, 11, NULL, '2019-07-16', '0002', 5, '2019-07-16 05:49:25', '2019-07-16 05:49:25'),
(7, 4, '2019-07-16', '2019-07-16', '0001', 1, '2019-07-16 11:20:24', '2019-07-16 05:50:24'),
(8, 11, NULL, '2019-07-16', '0001', 1, '2019-07-16 05:50:24', '2019-07-16 05:50:24'),
(9, 4, '2019-07-16', '2019-07-16', '0003', 1, '2019-07-16 12:27:56', '2019-07-16 06:57:56'),
(10, 11, NULL, '2019-07-16', '0003', 1, '2019-07-16 06:57:56', '2019-07-16 06:57:56'),
(11, 1, NULL, '2019-07-16', '0005', 1, '2019-07-16 07:40:57', '2019-07-16 07:40:57'),
(12, 1, NULL, '2019-07-16', '0006', 1, '2019-07-16 07:46:52', '2019-07-16 07:46:52'),
(13, 1, '2019-07-17', '2019-07-16', '0007', 1, '2019-07-17 07:16:25', '2019-07-17 01:46:25'),
(14, 1, '2019-07-16', '2019-07-16', '0008', 1, '2019-07-16 14:39:25', '2019-07-16 09:09:25'),
(15, 4, '2019-07-16', '2019-07-16', '0008', 1, '2019-07-16 14:39:29', '2019-07-16 09:09:29'),
(16, 11, '2019-07-16', '2019-07-16', '0008', 1, '2019-07-16 18:02:31', '2019-07-16 12:32:31'),
(17, 1, '2019-07-16', '2019-07-16', '0008', 1, '2019-07-16 18:22:57', '2019-07-16 12:52:57'),
(18, 9, '2019-07-16', '2019-07-16', '0008', 1, '2019-07-16 18:23:45', '2019-07-16 12:53:45'),
(19, 9, '2019-07-16', '2019-07-16', '0008', 1, '2019-07-16 18:26:13', '2019-07-16 12:56:13'),
(20, 1, '2019-07-16', '2019-07-16', '0008', 1, '2019-07-16 18:26:29', '2019-07-16 12:56:29'),
(21, 5, '2019-07-16', '2019-07-16', '0008', 1, '2019-07-16 18:26:37', '2019-07-16 12:56:37'),
(22, 1, '2019-07-16', '2019-07-16', '0008', 1, '2019-07-16 18:26:46', '2019-07-16 12:56:46'),
(23, 5, '2019-07-16', '2019-07-16', '0008', 1, '2019-07-16 18:28:47', '2019-07-16 12:58:47'),
(24, 1, '2019-07-16', '2019-07-16', '0008', 1, '2019-07-16 18:28:55', '2019-07-16 12:58:55'),
(25, 4, '2019-07-17', '2019-07-16', '0008', 1, '2019-07-17 06:57:39', '2019-07-17 01:27:39'),
(26, 1, '2019-07-17', '2019-07-17', '0008', 1, '2019-07-17 06:57:56', '2019-07-17 01:27:56'),
(27, 5, '2019-07-17', '2019-07-17', '0008', 1, '2019-07-17 06:58:00', '2019-07-17 01:28:00'),
(28, 1, '2019-07-17', '2019-07-17', '0008', 1, '2019-07-17 06:58:10', '2019-07-17 01:28:10'),
(29, 4, '2019-07-17', '2019-07-17', '0008', 1, '2019-07-17 06:58:46', '2019-07-17 01:28:46'),
(30, 9, '2019-07-17', '2019-07-17', '0008', 1, '2019-07-17 06:59:08', '2019-07-17 01:29:08'),
(31, 1, '2019-07-17', '2019-07-17', '0008', 1, '2019-07-17 06:59:24', '2019-07-17 01:29:24'),
(32, 5, '2019-07-17', '2019-07-17', '0008', 1, '2019-07-17 06:59:27', '2019-07-17 01:29:27'),
(33, 1, '2019-07-17', '2019-07-17', '0008', 1, '2019-07-17 06:59:36', '2019-07-17 01:29:36'),
(34, 4, '2019-07-17', '2019-07-17', '0008', 1, '2019-07-17 07:01:29', '2019-07-17 01:31:29'),
(35, 9, '2019-07-17', '2019-07-17', '0008', 1, '2019-07-17 07:01:48', '2019-07-17 01:31:48'),
(36, 1, '2019-07-17', '2019-07-17', '0008', 1, '2019-07-17 07:02:01', '2019-07-17 01:32:01'),
(37, 5, '2019-07-17', '2019-07-17', '0008', 1, '2019-07-17 07:03:06', '2019-07-17 01:33:06'),
(38, 1, '2019-07-17', '2019-07-17', '0008', 1, '2019-07-17 07:04:08', '2019-07-17 01:34:08'),
(39, 4, '2019-07-17', '2019-07-17', '0008', 1, '2019-07-17 07:04:11', '2019-07-17 01:34:11'),
(40, 11, '2019-07-17', '2019-07-17', '0008', 1, '2019-07-17 07:05:34', '2019-07-17 01:35:34'),
(41, 12, NULL, '2019-07-17', '0008', 16, '2019-07-17 01:35:34', '2019-07-17 01:35:34'),
(42, 4, '2019-07-17', '2019-07-17', '0007', 1, '2019-07-17 07:16:30', '2019-07-17 01:46:30'),
(43, 11, '2019-07-17', '2019-07-17', '0007', 1, '2019-07-17 07:16:46', '2019-07-17 01:46:46'),
(44, 9, '2019-07-18', '2019-07-17', '0007', 1, '2019-07-18 05:47:45', '2019-07-18 00:17:45'),
(45, 1, '2019-07-17', '2019-07-17', '0009', 1, '2019-07-17 09:22:12', '2019-07-17 03:52:12'),
(46, 4, '2019-07-17', '2019-07-17', '0009', 1, '2019-07-17 09:22:16', '2019-07-17 03:52:16'),
(47, 11, '2019-07-17', '2019-07-17', '0009', 1, '2019-07-17 09:22:43', '2019-07-17 03:52:43'),
(48, 9, '2019-07-17', '2019-07-17', '0009', 16, '2019-07-17 09:23:55', '2019-07-17 03:53:55'),
(49, 1, '2019-07-17', '2019-07-17', '0009', 5, '2019-07-17 09:24:24', '2019-07-17 03:54:24'),
(50, 4, '2019-07-17', '2019-07-17', '0009', 5, '2019-07-17 09:24:28', '2019-07-17 03:54:28'),
(51, 11, '2019-07-18', '2019-07-17', '0009', 5, '2019-07-18 06:55:34', '2019-07-18 01:25:34'),
(52, 1, '2019-07-18', '2019-07-18', '0007', 1, '2019-07-18 05:48:10', '2019-07-18 00:18:10'),
(53, 6, '2019-07-18', '2019-07-18', '0007', 1, '2019-07-18 05:48:26', '2019-07-18 00:18:26'),
(54, 1, '2019-07-18', '2019-07-18', '0007', 1, '2019-07-18 05:48:37', '2019-07-18 00:18:37'),
(55, 4, '2019-07-18', '2019-07-18', '0007', 1, '2019-07-18 05:48:40', '2019-07-18 00:18:40'),
(56, 11, '2019-07-18', '2019-07-18', '0007', 1, '2019-07-18 05:52:41', '2019-07-18 00:22:41'),
(57, 12, NULL, '2019-07-18', '0007', 16, '2019-07-18 00:22:41', '2019-07-18 00:22:41'),
(58, 1, '2019-07-18', '2019-07-18', '0010', 1, '2019-07-18 06:04:13', '2019-07-18 00:34:13'),
(59, 4, '2019-07-18', '2019-07-18', '0010', 1, '2019-07-18 06:04:30', '2019-07-18 00:34:30'),
(60, 9, '2019-07-18', '2019-07-18', '0010', 1, '2019-07-18 06:05:21', '2019-07-18 00:35:21'),
(61, 1, '2019-07-18', '2019-07-18', '0010', 1, '2019-07-18 06:05:30', '2019-07-18 00:35:30'),
(62, 4, '2019-07-18', '2019-07-18', '0010', 1, '2019-07-18 06:05:51', '2019-07-18 00:35:51'),
(63, 11, '2019-07-18', '2019-07-18', '0010', 1, '2019-07-18 06:06:18', '2019-07-18 00:36:18'),
(64, 9, '2019-07-18', '2019-07-18', '0010', 16, '2019-07-18 06:06:56', '2019-07-18 00:36:56'),
(65, 1, '2019-07-18', '2019-07-18', '0010', 1, '2019-07-18 06:08:52', '2019-07-18 00:38:52'),
(66, 1, '2019-07-18', '2019-07-18', '0010', 1, '2019-07-18 06:09:02', '2019-07-18 00:39:02'),
(67, 4, '2019-07-18', '2019-07-18', '0010', 1, '2019-07-18 06:09:06', '2019-07-18 00:39:06'),
(68, 11, '2019-07-18', '2019-07-18', '0010', 1, '2019-07-18 06:09:43', '2019-07-18 00:39:43'),
(69, 9, '2019-07-18', '2019-07-18', '0010', 1, '2019-07-18 06:10:15', '2019-07-18 00:40:15'),
(70, 1, '2019-07-18', '2019-07-18', '0010', 1, '2019-07-18 06:10:26', '2019-07-18 00:40:26'),
(71, 4, '2019-07-18', '2019-07-18', '0010', 1, '2019-07-18 06:10:41', '2019-07-18 00:40:41'),
(72, 11, '2019-07-18', '2019-07-18', '0010', 1, '2019-07-18 06:10:57', '2019-07-18 00:40:57'),
(73, 12, '2019-07-18', '2019-07-18', '0010', 16, '2019-07-18 06:11:23', '2019-07-18 00:41:23'),
(74, 8, NULL, '2019-07-18', '0010', 1, '2019-07-18 00:41:23', '2019-07-18 00:41:23'),
(75, 12, NULL, '2019-07-18', '0009', 16, '2019-07-18 01:25:34', '2019-07-18 01:25:34'),
(76, 1, '2019-07-18', '2019-07-18', '0011', 1, '2019-07-18 09:29:53', '2019-07-18 03:59:53'),
(77, 4, NULL, '2019-07-18', '0011', 1, '2019-07-18 03:59:53', '2019-07-18 03:59:53'),
(78, 1, NULL, '2019-07-22', '0012', 1, '2019-07-22 00:46:38', '2019-07-22 00:46:38'),
(79, 1, NULL, '2019-07-22', '0013', 1, '2019-07-22 01:17:05', '2019-07-22 01:17:05'),
(80, 1, NULL, '2019-07-22', '0014', 1, '2019-07-22 01:18:10', '2019-07-22 01:18:10'),
(81, 1, NULL, '2019-07-22', '0015', 1, '2019-07-22 01:19:03', '2019-07-22 01:19:03'),
(82, 1, NULL, '2019-07-27', '0016', 1, '2019-07-27 03:27:03', '2019-07-27 03:27:03');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product`
--

CREATE TABLE `tbl_product` (
  `id` int(11) NOT NULL,
  `invoice_id` varchar(150) NOT NULL,
  `head` varchar(255) NOT NULL,
  `sub_head` varchar(255) DEFAULT NULL,
  `value` decimal(20,2) NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `status` varchar(50) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_product`
--

INSERT INTO `tbl_product` (`id`, `invoice_id`, `head`, `sub_head`, `value`, `from_date`, `to_date`, `status`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(6, '0001', '9', 'dsa', '4000.00', '2019-07-16', '2019-07-15', '1', 1, '2019-07-16 05:41:59', NULL, '2019-07-16 05:41:59'),
(5, '0001', '9', 'Aditya', '3000.00', '2019-07-16', '2019-07-16', '1', 1, '2019-07-16 05:41:59', NULL, '2019-07-16 05:41:59'),
(3, '0002', '5', 'Testing Again', '150000.00', '2019-07-16', '2019-07-20', '1', 1, '2019-07-16 05:38:28', NULL, '2019-07-16 05:38:28'),
(4, '0002', '5', 'Test 2', '15000.00', '2019-07-16', '2019-07-19', '1', 1, '2019-07-16 05:38:28', NULL, '2019-07-16 05:38:28'),
(7, '0003', '9', 'Aditya', '3000.00', '2019-07-16', '2019-07-16', '1', 1, '2019-07-16 05:48:54', NULL, '2019-07-16 05:48:54'),
(8, '0003', '9', 'Aditya', '50000.00', '2019-07-16', '2019-07-16', '1', 1, '2019-07-16 05:48:54', NULL, '2019-07-16 05:48:54'),
(9, '0004', '9', 'Aditya', '20000.00', '2019-07-10', '2019-07-16', '1', 1, '2019-07-16 07:40:34', NULL, '2019-07-16 07:40:34'),
(10, '0006', '5', 'Manpower Services', '1000.00', '2019-07-16', '2019-07-17', '1', 1, '2019-07-16 07:46:50', NULL, '2019-07-16 07:46:50'),
(29, '0007', '4', 'Storage', '30000.00', '2019-07-18', '2019-07-18', '1', 1, '2019-07-18 00:18:26', NULL, '2019-07-18 00:18:26'),
(17, '0008', '9', 'Aditya', '200000.00', '2019-07-16', '2019-07-16', '1', 1, '2019-07-17 01:31:48', NULL, '2019-07-17 01:31:48'),
(23, '0009', '5', 'Manpower Services', '20000.00', '2019-07-17', '2019-07-17', '1', 1, '2019-07-17 03:53:55', NULL, '2019-07-17 03:53:55'),
(22, '0009', '5', 'Testing Again', '2000.00', '2019-07-17', '2019-07-17', '1', 1, '2019-07-17 03:53:55', NULL, '2019-07-17 03:53:55'),
(21, '0009', '5', 'abc', '300.00', '2019-07-17', '2019-07-17', '1', 1, '2019-07-17 03:53:55', NULL, '2019-07-17 03:53:55'),
(28, '0007', '9', 'Aditya', '2000.00', '2019-07-18', '2019-07-18', '1', 1, '2019-07-18 00:18:26', NULL, '2019-07-18 00:18:26'),
(27, '0007', '5', 'Manpower Services', '200000.00', '2019-07-09', '2019-07-16', '1', 1, '2019-07-18 00:18:26', NULL, '2019-07-18 00:18:26'),
(49, '0011', '14', 'Normal Storage', '100000.00', '2019-07-02', '2019-07-16', '1', 1, '2019-07-18 03:58:16', NULL, '2019-07-18 03:58:16'),
(47, '0010', '5', 'abcd213', '10000.00', '2019-07-18', '2019-07-18', '1', 1, '2019-07-18 00:40:15', NULL, '2019-07-18 00:40:15'),
(48, '0011', '11', 'Manpower Services', '20000.00', '2019-07-01', '2019-07-01', '1', 1, '2019-07-18 03:58:16', NULL, '2019-07-18 03:58:16'),
(46, '0010', '9', 'dsa', '20000.00', '2019-07-18', '2019-07-18', '1', 1, '2019-07-18 00:40:15', NULL, '2019-07-18 00:40:15'),
(45, '0010', '9', 'Aditya', '20000.00', '2019-07-18', '2019-07-18', '1', 1, '2019-07-18 00:40:15', NULL, '2019-07-18 00:40:15'),
(50, '0011', '14', 'Cold Storage', '10000.00', '2019-07-01', '2019-07-17', '1', 1, '2019-07-18 03:58:16', NULL, '2019-07-18 03:58:16'),
(51, '0012', '5', 'Manpower Services', '2000000.00', '2019-07-23', '2019-07-23', '1', 1, '2019-07-22 00:46:38', NULL, '2019-07-22 00:46:38'),
(52, '0013', '9', 'Aditya', '15000000.00', '2019-07-24', '2019-07-24', '1', 1, '2019-07-22 01:17:05', NULL, '2019-07-22 01:17:05'),
(53, '0015', '5', 'Manpower Services', '280000000.00', '2019-07-25', '2019-07-25', '1', 1, '2019-07-22 01:19:03', NULL, '2019-07-22 01:19:03'),
(54, '0016', '5', 'Manpower Services', '200000.00', '2019-07-27', '2019-07-27', '1', 1, '2019-07-27 03:27:03', NULL, '2019-07-27 03:27:03');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product_temp`
--

CREATE TABLE `tbl_product_temp` (
  `id` int(11) NOT NULL,
  `invoice_id` varchar(150) NOT NULL,
  `head` varchar(255) NOT NULL,
  `sub_head` varchar(255) NOT NULL,
  `value` decimal(20,2) NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `status` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_product_temp`
--

INSERT INTO `tbl_product_temp` (`id`, `invoice_id`, `head`, `sub_head`, `value`, `from_date`, `to_date`, `status`, `created_at`, `updated_at`) VALUES
(5, '0002', '5', 'Testing Again', '150000.00', '2019-07-16', '2019-07-20', '1', '2019-07-16 05:39:04', '2019-07-16 05:39:04'),
(6, '0002', '5', 'Test 2', '15000.00', '2019-07-16', '2019-07-19', '1', '2019-07-16 05:39:04', '2019-07-16 05:39:04'),
(14, '0003', '5', 'Testing Again', '3000.00', '2019-07-16', '2019-07-16', '1', '2019-07-16 07:23:31', '2019-07-16 07:23:31'),
(11, '0003', '9', 'Aditya', '3000.00', '2019-07-16', '2019-07-16', '1', '2019-07-16 05:49:05', '2019-07-16 05:49:05'),
(27, '0008', '9', 'Aditya', '200000.00', '2019-07-16', '2019-07-16', '1', '2019-07-17 03:37:17', '2019-07-17 03:37:17');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_revenue`
--

CREATE TABLE `tbl_revenue` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_revenue`
--

INSERT INTO `tbl_revenue` (`id`, `name`, `status`, `parent_id`, `created_at`, `updated_at`) VALUES
(4, 'Warehouse Assets Backed Charges', 1, NULL, '2019-06-19 07:02:52', '2019-06-19 07:02:52'),
(5, 'Aditya test1', 1, NULL, '2019-06-20 00:25:35', '2019-06-20 00:25:35'),
(9, 'Aditiya test test', 1, NULL, '2019-06-29 03:29:53', '2019-06-29 03:29:53'),
(10, 'abcd', 1, NULL, '2019-06-29 03:46:47', '2019-06-29 03:46:47'),
(11, 'Test head', 1, NULL, '2019-07-02 05:30:41', '2019-07-02 05:30:41'),
(12, 'abcd213', 1, 5, '2019-07-04 01:20:51', '2019-07-04 01:20:51'),
(13, 'Warehouse', 1, 8, '2019-07-04 01:39:47', '2019-07-04 01:39:47'),
(14, 'Warehouse', 1, NULL, '2019-07-05 00:02:16', '2019-07-05 00:02:16'),
(15, 'dsa', 1, 9, '2019-07-05 01:28:46', '2019-07-05 01:28:46'),
(16, 'abc', 1, 5, '2019-07-06 06:39:39', '2019-07-06 06:39:39'),
(17, 'DEF', 1, 5, '2019-07-06 06:40:01', '2019-07-06 06:40:01'),
(31, 'Normal Storage', 1, 14, '2019-07-18 03:56:09', '2019-07-18 03:56:09'),
(19, 'Storage', 1, 4, '2019-07-09 05:52:16', '2019-07-09 05:52:16'),
(20, 'Manpower Services', 1, 5, '2019-07-09 05:53:54', '2019-07-09 05:53:54'),
(21, 'Testing It', 1, 5, '2019-07-10 02:24:34', '2019-07-10 02:24:34'),
(22, 'Testing Again', 1, 5, '2019-07-10 02:25:02', '2019-07-10 02:25:02'),
(23, 'Aditya', 1, 9, '2019-07-15 07:25:02', '2019-07-15 07:25:02'),
(24, '12', 1, 5, '2019-07-15 08:06:58', '2019-07-15 08:06:58'),
(25, 'Test 101', 1, 11, '2019-07-15 08:54:57', '2019-07-15 08:54:57'),
(30, 'Cold Storage', 1, 14, '2019-07-18 03:55:49', '2019-07-18 03:55:49'),
(29, 'Storage', 1, NULL, '2019-07-18 03:50:00', '2019-07-18 03:50:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_roles`
--

CREATE TABLE `tbl_roles` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `status` smallint(2) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_roles`
--

INSERT INTO `tbl_roles` (`id`, `name`, `status`, `updated_at`, `created_at`) VALUES
(1, 'Administrator', 1, '2019-06-17 20:17:18', '2019-06-17 20:17:18'),
(2, 'Finance', 1, '2019-06-20 20:51:05', '2019-06-17 20:17:39'),
(3, 'OPS', 1, '2019-06-20 20:51:16', '2019-06-17 20:17:47'),
(4, 'Business Development', 1, '2019-06-20 20:51:28', '2019-06-17 20:17:54'),
(5, 'Client', 1, '2019-06-22 00:09:45', '2019-06-22 00:09:45'),
(7, 'Warehouse Manager', 1, '2019-07-09 05:26:44', '2019-07-09 05:26:44');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_role_permission`
--

CREATE TABLE `tbl_role_permission` (
  `id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `status` smallint(2) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_role_permission`
--

INSERT INTO `tbl_role_permission` (`id`, `permission_id`, `role_id`, `status`, `created_by`, `created_at`, `updated_at`, `updated_by`) VALUES
(19, 12, 4, 1, 1, '2019-07-04 05:09:30', '2019-07-04 05:09:30', NULL),
(921, 46, 1, 1, 1, '2019-07-17 01:42:29', '2019-07-17 01:42:29', NULL),
(920, 45, 1, 1, 1, '2019-07-17 01:42:29', '2019-07-17 01:42:29', NULL),
(919, 44, 1, 1, 1, '2019-07-17 01:42:29', '2019-07-17 01:42:29', NULL),
(918, 43, 1, 1, 1, '2019-07-17 01:42:29', '2019-07-17 01:42:29', NULL),
(917, 41, 1, 1, 1, '2019-07-17 01:42:29', '2019-07-17 01:42:29', NULL),
(916, 40, 1, 1, 1, '2019-07-17 01:42:29', '2019-07-17 01:42:29', NULL),
(915, 39, 1, 1, 1, '2019-07-17 01:42:29', '2019-07-17 01:42:29', NULL),
(914, 14, 1, 1, 1, '2019-07-17 01:42:29', '2019-07-17 01:42:29', NULL),
(913, 55, 1, 1, 1, '2019-07-17 01:42:29', '2019-07-17 01:42:29', NULL),
(762, 50, 5, 1, 1, '2019-07-16 02:27:23', '2019-07-16 02:27:23', NULL),
(319, 41, 3, 1, 1, '2019-07-09 04:13:48', '2019-07-09 04:13:48', NULL),
(318, 39, 3, 1, 1, '2019-07-09 04:13:48', '2019-07-09 04:13:48', NULL),
(317, 14, 3, 1, 1, '2019-07-09 04:13:48', '2019-07-09 04:13:48', NULL),
(316, 23, 3, 1, 1, '2019-07-09 04:13:48', '2019-07-09 04:13:48', NULL),
(20, 13, 4, 1, 1, '2019-07-04 05:09:30', '2019-07-04 05:09:30', NULL),
(912, 54, 1, 1, 1, '2019-07-17 01:42:29', '2019-07-17 01:42:29', NULL),
(922, 47, 1, 1, 1, '2019-07-17 01:42:29', '2019-07-17 01:42:29', NULL),
(315, 19, 3, 1, 1, '2019-07-09 04:13:48', '2019-07-09 04:13:48', NULL),
(911, 50, 1, 1, 1, '2019-07-17 01:42:29', '2019-07-17 01:42:29', NULL),
(910, 49, 1, 1, 1, '2019-07-17 01:42:29', '2019-07-17 01:42:29', NULL),
(909, 48, 1, 1, 1, '2019-07-17 01:42:29', '2019-07-17 01:42:29', NULL),
(908, 25, 1, 1, 1, '2019-07-17 01:42:29', '2019-07-17 01:42:29', NULL),
(907, 24, 1, 1, 1, '2019-07-17 01:42:29', '2019-07-17 01:42:29', NULL),
(906, 23, 1, 1, 1, '2019-07-17 01:42:29', '2019-07-17 01:42:29', NULL),
(905, 21, 1, 1, 1, '2019-07-17 01:42:29', '2019-07-17 01:42:29', NULL),
(873, 41, 2, 1, 1, '2019-07-16 06:48:43', '2019-07-16 06:48:43', NULL),
(872, 40, 2, 1, 1, '2019-07-16 06:48:43', '2019-07-16 06:48:43', NULL),
(871, 39, 2, 1, 1, '2019-07-16 06:48:43', '2019-07-16 06:48:43', NULL),
(870, 14, 2, 1, 1, '2019-07-16 06:48:43', '2019-07-16 06:48:43', NULL),
(869, 50, 2, 1, 1, '2019-07-16 06:48:43', '2019-07-16 06:48:43', NULL),
(868, 49, 2, 1, 1, '2019-07-16 06:48:43', '2019-07-16 06:48:43', NULL),
(867, 48, 2, 1, 1, '2019-07-16 06:48:43', '2019-07-16 06:48:43', NULL),
(761, 48, 5, 1, 1, '2019-07-16 02:27:23', '2019-07-16 02:27:23', NULL),
(760, 25, 5, 1, 1, '2019-07-16 02:27:23', '2019-07-16 02:27:23', NULL),
(759, 21, 5, 1, 1, '2019-07-16 02:27:23', '2019-07-16 02:27:23', NULL),
(904, 20, 1, 1, 1, '2019-07-17 01:42:29', '2019-07-17 01:42:29', NULL),
(314, 18, 3, 1, 1, '2019-07-09 04:13:48', '2019-07-09 04:13:48', NULL),
(313, 17, 3, 1, 1, '2019-07-09 04:13:48', '2019-07-09 04:13:48', NULL),
(312, 16, 3, 1, 1, '2019-07-09 04:13:48', '2019-07-09 04:13:48', NULL),
(311, 15, 3, 1, 1, '2019-07-09 04:13:48', '2019-07-09 04:13:48', NULL),
(310, 13, 3, 1, 1, '2019-07-09 04:13:48', '2019-07-09 04:13:48', NULL),
(309, 12, 3, 1, 1, '2019-07-09 04:13:48', '2019-07-09 04:13:48', NULL),
(308, 11, 3, 1, 1, '2019-07-09 04:13:48', '2019-07-09 04:13:48', NULL),
(307, 10, 3, 1, 1, '2019-07-09 04:13:48', '2019-07-09 04:13:48', NULL),
(903, 19, 1, 1, 1, '2019-07-17 01:42:29', '2019-07-17 01:42:29', NULL),
(902, 18, 1, 1, 1, '2019-07-17 01:42:29', '2019-07-17 01:42:29', NULL),
(901, 17, 1, 1, 1, '2019-07-17 01:42:29', '2019-07-17 01:42:29', NULL),
(900, 16, 1, 1, 1, '2019-07-17 01:42:29', '2019-07-17 01:42:29', NULL),
(899, 15, 1, 1, 1, '2019-07-17 01:42:29', '2019-07-17 01:42:29', NULL),
(898, 13, 1, 1, 1, '2019-07-17 01:42:29', '2019-07-17 01:42:29', NULL),
(897, 52, 1, 1, 1, '2019-07-17 01:42:29', '2019-07-17 01:42:29', NULL),
(896, 38, 1, 1, 1, '2019-07-17 01:42:29', '2019-07-17 01:42:29', NULL),
(895, 37, 1, 1, 1, '2019-07-17 01:42:29', '2019-07-17 01:42:29', NULL),
(894, 36, 1, 1, 1, '2019-07-17 01:42:29', '2019-07-17 01:42:29', NULL),
(893, 22, 1, 1, 1, '2019-07-17 01:42:29', '2019-07-17 01:42:29', NULL),
(892, 12, 1, 1, 1, '2019-07-17 01:42:29', '2019-07-17 01:42:29', NULL),
(891, 35, 1, 1, 1, '2019-07-17 01:42:29', '2019-07-17 01:42:29', NULL),
(890, 34, 1, 1, 1, '2019-07-17 01:42:29', '2019-07-17 01:42:29', NULL),
(889, 33, 1, 1, 1, '2019-07-17 01:42:29', '2019-07-17 01:42:29', NULL),
(888, 11, 1, 1, 1, '2019-07-17 01:42:29', '2019-07-17 01:42:29', NULL),
(887, 29, 1, 1, 1, '2019-07-17 01:42:29', '2019-07-17 01:42:29', NULL),
(886, 28, 1, 1, 1, '2019-07-17 01:42:29', '2019-07-17 01:42:29', NULL),
(885, 27, 1, 1, 1, '2019-07-17 01:42:29', '2019-07-17 01:42:29', NULL),
(866, 25, 2, 1, 1, '2019-07-16 06:48:43', '2019-07-16 06:48:43', NULL),
(865, 24, 2, 1, 1, '2019-07-16 06:48:43', '2019-07-16 06:48:43', NULL),
(864, 23, 2, 1, 1, '2019-07-16 06:48:43', '2019-07-16 06:48:43', NULL),
(320, 43, 3, 1, 1, '2019-07-09 04:13:48', '2019-07-09 04:13:48', NULL),
(321, 10, 7, 1, 1, '2019-07-09 05:28:43', '2019-07-09 05:28:43', NULL),
(322, 26, 7, 1, 1, '2019-07-09 05:28:43', '2019-07-09 05:28:43', NULL),
(323, 27, 7, 1, 1, '2019-07-09 05:28:43', '2019-07-09 05:28:43', NULL),
(324, 28, 7, 1, 1, '2019-07-09 05:28:43', '2019-07-09 05:28:43', NULL),
(325, 29, 7, 1, 1, '2019-07-09 05:28:43', '2019-07-09 05:28:43', NULL),
(326, 12, 7, 1, 1, '2019-07-09 05:28:43', '2019-07-09 05:28:43', NULL),
(327, 22, 7, 1, 1, '2019-07-09 05:28:43', '2019-07-09 05:28:43', NULL),
(328, 36, 7, 1, 1, '2019-07-09 05:28:43', '2019-07-09 05:28:43', NULL),
(329, 37, 7, 1, 1, '2019-07-09 05:28:43', '2019-07-09 05:28:43', NULL),
(330, 13, 7, 1, 1, '2019-07-09 05:28:43', '2019-07-09 05:28:43', NULL),
(331, 15, 7, 1, 1, '2019-07-09 05:28:43', '2019-07-09 05:28:43', NULL),
(332, 16, 7, 1, 1, '2019-07-09 05:28:43', '2019-07-09 05:28:43', NULL),
(333, 17, 7, 1, 1, '2019-07-09 05:28:43', '2019-07-09 05:28:43', NULL),
(334, 18, 7, 1, 1, '2019-07-09 05:28:43', '2019-07-09 05:28:43', NULL),
(335, 21, 7, 1, 1, '2019-07-09 05:28:43', '2019-07-09 05:28:43', NULL),
(336, 23, 7, 1, 1, '2019-07-09 05:28:43', '2019-07-09 05:28:43', NULL),
(337, 24, 7, 1, 1, '2019-07-09 05:28:43', '2019-07-09 05:28:43', NULL),
(338, 25, 7, 1, 1, '2019-07-09 05:28:43', '2019-07-09 05:28:43', NULL),
(339, 14, 7, 1, 1, '2019-07-09 05:28:43', '2019-07-09 05:28:43', NULL),
(340, 39, 7, 1, 1, '2019-07-09 05:28:43', '2019-07-09 05:28:43', NULL),
(341, 40, 7, 1, 1, '2019-07-09 05:28:43', '2019-07-09 05:28:43', NULL),
(342, 41, 7, 1, 1, '2019-07-09 05:28:43', '2019-07-09 05:28:43', NULL),
(343, 43, 7, 1, 1, '2019-07-09 05:28:43', '2019-07-09 05:28:43', NULL),
(884, 26, 1, 1, 1, '2019-07-17 01:42:29', '2019-07-17 01:42:29', NULL),
(758, 18, 5, 1, 1, '2019-07-16 02:27:23', '2019-07-16 02:27:23', NULL),
(883, 10, 1, 1, 1, '2019-07-17 01:42:29', '2019-07-17 01:42:29', NULL),
(882, 51, 1, 1, 1, '2019-07-17 01:42:29', '2019-07-17 01:42:29', NULL),
(881, 32, 1, 1, 1, '2019-07-17 01:42:29', '2019-07-17 01:42:29', NULL),
(880, 31, 1, 1, 1, '2019-07-17 01:42:29', '2019-07-17 01:42:29', NULL),
(879, 30, 1, 1, 1, '2019-07-17 01:42:29', '2019-07-17 01:42:29', NULL),
(878, 9, 1, 1, 1, '2019-07-17 01:42:29', '2019-07-17 01:42:29', NULL),
(757, 15, 5, 1, 1, '2019-07-16 02:27:23', '2019-07-16 02:27:23', NULL),
(756, 13, 5, 1, 1, '2019-07-16 02:27:23', '2019-07-16 02:27:23', NULL),
(877, 8, 1, 1, 1, '2019-07-17 01:42:29', '2019-07-17 01:42:29', NULL),
(876, 7, 1, 1, 1, '2019-07-17 01:42:29', '2019-07-17 01:42:29', NULL),
(875, 6, 1, 1, 1, '2019-07-17 01:42:29', '2019-07-17 01:42:29', NULL),
(874, 5, 1, 1, 1, '2019-07-17 01:42:29', '2019-07-17 01:42:29', NULL),
(863, 21, 2, 1, 1, '2019-07-16 06:48:43', '2019-07-16 06:48:43', NULL),
(862, 20, 2, 1, 1, '2019-07-16 06:48:43', '2019-07-16 06:48:43', NULL),
(861, 19, 2, 1, 1, '2019-07-16 06:48:43', '2019-07-16 06:48:43', NULL),
(860, 18, 2, 1, 1, '2019-07-16 06:48:43', '2019-07-16 06:48:43', NULL),
(859, 17, 2, 1, 1, '2019-07-16 06:48:43', '2019-07-16 06:48:43', NULL),
(858, 16, 2, 1, 1, '2019-07-16 06:48:43', '2019-07-16 06:48:43', NULL),
(763, 43, 5, 1, 1, '2019-07-16 02:27:23', '2019-07-16 02:27:23', NULL),
(857, 15, 2, 1, 1, '2019-07-16 06:48:43', '2019-07-16 06:48:43', NULL),
(856, 13, 2, 1, 1, '2019-07-16 06:48:43', '2019-07-16 06:48:43', NULL),
(855, 12, 2, 1, 1, '2019-07-16 06:48:43', '2019-07-16 06:48:43', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ticket`
--

CREATE TABLE `tbl_ticket` (
  `id` int(11) NOT NULL,
  `remark` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_role_id` int(11) NOT NULL,
  `invoice_id` varchar(50) DEFAULT NULL,
  `status` varchar(20) NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_warehouse`
--

CREATE TABLE `tbl_warehouse` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `city` varchar(200) DEFAULT NULL,
  `state` varchar(200) DEFAULT NULL,
  `country` varchar(200) NOT NULL,
  `lat` float(10,6) DEFAULT NULL,
  `lng` float(10,6) DEFAULT NULL,
  `reck_management` varchar(3) DEFAULT NULL,
  `slug` varchar(200) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tbl_warehouse`
--

INSERT INTO `tbl_warehouse` (`id`, `name`, `address`, `city`, `state`, `country`, `lat`, `lng`, `reck_management`, `slug`, `status`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(5, 'Warehouse', 'Bhopal', 'Bhopal', 'madhya pradesh', 'India', NULL, NULL, NULL, NULL, 1, 1, '2019-06-21 02:06:39', NULL, '2019-06-21 07:36:39'),
(6, 'Warehouse test', 'Bhopal', 'Bhopal', 'madhya pradesh', 'India', NULL, NULL, 'yes', NULL, 1, 1, '2019-06-29 03:27:04', NULL, '2019-06-29 08:57:04'),
(8, 'Mundka MFC', 'Mundka', 'Delhi', 'Delhi', 'India', NULL, NULL, 'yes', NULL, 1, 1, '2019-07-09 05:43:09', NULL, '2019-07-09 11:13:09'),
(9, 'Warehouse_Demo', 'C-21A, ANAND VIHAR UTTAM NAGAR WEST, NEAR HOLY CONVENT PUBLIC SCHOOL', 'NEW DELHI', 'DELHI', 'India', NULL, NULL, 'yes', NULL, 1, 1, '2019-07-15 07:40:42', NULL, '2019-07-15 13:10:42'),
(10, 'Warehouse Company a', 'Bhopal', 'Bhopal', 'Madhya Pardesh', 'India', NULL, NULL, 'no', NULL, 1, 1, '2019-07-16 01:20:20', 1, '2019-07-16 06:57:16');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `email` varchar(150) NOT NULL,
  `password` varchar(150) DEFAULT NULL,
  `remember_token` varchar(255) DEFAULT NULL,
  `status` varchar(15) NOT NULL,
  `role_id` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `email`, `password`, `remember_token`, `status`, `role_id`, `created_at`, `updated_at`) VALUES
(1, 'Admin1', '', 'admin@gmail.com', '$2y$10$s/7EvRuneqtCnCXOICXUh.cHtTsFyHvtw6txos3RzUoEp8vsb95MC', 'OQcYNiKGhLNqshfH1g6GbY55AGWFedYBWXcVj3zgQYZtJc51Hng9OpP8WkbP', 'Active', 1, '2019-05-21', '2019-06-20'),
(7, 'client 02', NULL, 'client02@gmail.com', '$2y$10$mZFNKafAC1LGEmGpVBEtWuvPC3VSFGMm.0UUBKS/LJYEbCu7C4a6y', 'igyLAH2jDHpT6KHTmbk7SOnX6JvtpEPr9xIieCErwmuRpzkJokuVK7FnO2GK', 'Active', 5, '2019-06-29', '2019-06-29'),
(6, 'Client 1', NULL, 'client@gmail.com', '$2y$10$LgB/4EEwpQNl4VasqrjR6umdfuxaJJRqRDGp0nLJqPpliE0M12kJ.', 'TX3K3CIzEO19aftuxRC4vHhrL1QutUXhBPUNx5z9sHqi2BSs3GJGuswh7c6S', 'Active', 5, '2019-06-29', '2019-06-29'),
(5, 'Finance', NULL, 'finance@gmail.com', '$2y$10$8PM/EPlapP4iSL449yC6jONhW2IMFsjqg8w3i6D8AbRdLhpJl7Yqi', 'ZEaniIj6x6UPD11jMh4ASHPke4pMxMCZyhtwVu8WwXFGicSuy88iidPwLEya', 'Active', 2, '2019-06-29', '2019-06-29'),
(8, 'User ops', NULL, 'user_ops@gmail.com', '$2y$10$8Zsam/JDCHBvWtEEght5du3FD/wuQzpTNSdqHrMYf9iNuCNgX6eY2', 'QiM9z9Le1lzXTRKZRT3alzqpv3VqrwETtrx9jVSUTeFf3pPGzOuzTujtleai', 'Active', 3, '2019-06-29', '2019-06-29'),
(9, 'Business Development user', NULL, 'user_business_dev@gmail.com', '$2y$10$ZN5bXMMkkjjHf14EL460NOBSHJ0ARyljt3KJPujpDuv3CYGVAsgmW', 'y9nJ86D4sqV9OXIH6CHB6bKrtWShQktDTBIUs1sM2iUFpKSxoLEYYL7CD3Js', 'Active', 4, '2019-06-29', '2019-06-29'),
(10, 'client004', NULL, 'client004@gmail.com', '$2y$10$e9pfAR1GpP4eNP/BAtB2ZO/gZaD9/3YkE9DS/vCMwCxFxPaDflNtq', 'kjMV80kLmX9gIiXYnntZVgciK2TMFAxuaqE717Yx', 'Active', 5, '2019-07-08', '2019-07-08'),
(11, 'Arya Aditya', NULL, 'arya.aditya@holisollogistics.com', '$2y$10$bBJtT42CrjQRmxl0fx2eeuWAQReI/0in0FoQcbq4glo8DjdGWvNaC', 'YTrc1T7SZZn1zS3MDfNmqrWUmbZDGQVo2Q9MNQU9HkNhnktZU1REsH5HFXe0', 'Active', 7, '2019-07-09', '2019-07-09'),
(12, 'client005', NULL, 'client005@gmail.com', '$2y$10$5TTBrcH8Tyhqtc0U7svfn.zw7nOH2bkQTvx.cxjxENp2UAyBHKgO.', 'KmcNE74RcAcQO1dN0er8ufVrbDpqIAIeDDn3cAedUYjzTz6vXcQhwlUQfeqD', 'Active', 5, '2019-07-09', '2019-07-09'),
(13, 'Saroj Kumar', NULL, 'saroj.kumar@holisollogistics.com', '$2y$10$CEyGRNcBMPRR5y/2W9IPxOMsk9tkvS79jj/vnbZCT3zPjvlRYapaO', 'U1HejoNt5vEj90fzXP5L3pHxNvYX8dmAKD2WdZTx', 'Active', 2, '2019-07-09', '2019-07-09'),
(14, 'Rashmi', NULL, 'rashmi@gmail.com', '$2y$10$46VnGzB9J0YOJmLWuKQCf.fbirCEmoCiwfMGPwKla7Dquz7/sYD7a', '9isN3MT6YvU1wsiq8OZJyINigSHnk54VI6AHek2q', 'Active', 2, '2019-07-15', '2019-07-15'),
(18, 'Rashmi', NULL, 'rashmi@jithvar.com', '$2y$10$6nrQOM65jkSqj2wFgCQC2O89VFRIXBstU8TXT2Gevklg92/H27fSC', 'zIQSmD5juEol7rJPm8Z2jassEV4lvEcQsRk9ddpr', 'Active', 5, '2019-07-16', '2019-07-16'),
(16, 'Rashmi', NULL, 'rashmi_pathak@gmail.com', '$2y$10$U8w7OfE6lpPo8gxlZ8Hgg.x85L5ySPUwheuAKgKdBraPu/OjWfXK6', 'hRPyf45ikYjYFTcG7vXRE7sSxrxdIrR8tiyZDdBRgQVQgg3Jkwr8SRl04Tn4', 'Active', 5, '2019-07-16', '2019-07-16'),
(17, 'Sachin', NULL, 'sachin@ptindia.org', '$2y$10$ffbQ5nYz2X7EF9tc2I/ZJOyF0UYNqmT3DCzc6qcjaDC/9FGXLFbMS', 'qYOdbsFKpLFOZk6JZy2VpIHhBTOqmg3VLjgEc6hY', 'Active', 5, '2019-07-16', '2019-07-16'),
(26, 'Rashmi', NULL, 'rashmipathak0202@gmail.com', '$2y$10$klH2qbRfc3ejPK.WM6mzEOjwA7oQgxoUYgnG6CSBqpM4NHjLBlv6i', 'zIQSmD5juEol7rJPm8Z2jassEV4lvEcQsRk9ddpr', 'Active', 5, '2019-07-16', '2019-07-16'),
(27, 'Rashmi', NULL, 'rashmimalviya1994@gmail.com', '$2y$10$u3/NcXeCmlxzYAToDiv3lezSMeLTvXiLh40QZkBm8UZlclFIQhkg6', 'Pu8V6vk3suc3IUnxVTYoTSjP4lkt0NfI8rAA9h3Y', 'Active', 5, '2019-07-16', '2019-07-16');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `tbl_client`
--
ALTER TABLE `tbl_client`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_client_warehouse`
--
ALTER TABLE `tbl_client_warehouse`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_company_wings`
--
ALTER TABLE `tbl_company_wings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_follow_up`
--
ALTER TABLE `tbl_follow_up`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`);

--
-- Indexes for table `tbl_invoice`
--
ALTER TABLE `tbl_invoice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_invoice_status`
--
ALTER TABLE `tbl_invoice_status`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `tbl_message`
--
ALTER TABLE `tbl_message`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`);

--
-- Indexes for table `tbl_notification`
--
ALTER TABLE `tbl_notification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_payment`
--
ALTER TABLE `tbl_payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_permission`
--
ALTER TABLE `tbl_permission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_permission_group`
--
ALTER TABLE `tbl_permission_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_process`
--
ALTER TABLE `tbl_process`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_process_mapping`
--
ALTER TABLE `tbl_process_mapping`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_process_step_history`
--
ALTER TABLE `tbl_process_step_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_product`
--
ALTER TABLE `tbl_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_product_temp`
--
ALTER TABLE `tbl_product_temp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_revenue`
--
ALTER TABLE `tbl_revenue`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_roles`
--
ALTER TABLE `tbl_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_role_permission`
--
ALTER TABLE `tbl_role_permission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_ticket`
--
ALTER TABLE `tbl_ticket`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_warehouse`
--
ALTER TABLE `tbl_warehouse`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_client`
--
ALTER TABLE `tbl_client`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tbl_client_warehouse`
--
ALTER TABLE `tbl_client_warehouse`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `tbl_company_wings`
--
ALTER TABLE `tbl_company_wings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_follow_up`
--
ALTER TABLE `tbl_follow_up`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tbl_invoice`
--
ALTER TABLE `tbl_invoice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `tbl_invoice_status`
--
ALTER TABLE `tbl_invoice_status`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `tbl_message`
--
ALTER TABLE `tbl_message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `tbl_notification`
--
ALTER TABLE `tbl_notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=470;

--
-- AUTO_INCREMENT for table `tbl_payment`
--
ALTER TABLE `tbl_payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tbl_permission`
--
ALTER TABLE `tbl_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `tbl_permission_group`
--
ALTER TABLE `tbl_permission_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `tbl_process`
--
ALTER TABLE `tbl_process`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `tbl_process_mapping`
--
ALTER TABLE `tbl_process_mapping`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `tbl_process_step_history`
--
ALTER TABLE `tbl_process_step_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;

--
-- AUTO_INCREMENT for table `tbl_product`
--
ALTER TABLE `tbl_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `tbl_product_temp`
--
ALTER TABLE `tbl_product_temp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT for table `tbl_revenue`
--
ALTER TABLE `tbl_revenue`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `tbl_roles`
--
ALTER TABLE `tbl_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tbl_role_permission`
--
ALTER TABLE `tbl_role_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=923;

--
-- AUTO_INCREMENT for table `tbl_ticket`
--
ALTER TABLE `tbl_ticket`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_warehouse`
--
ALTER TABLE `tbl_warehouse`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
