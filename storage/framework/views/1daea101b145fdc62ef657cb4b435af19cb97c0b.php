<?php echo $__env->make('header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php
$total = number_format($item->total,2);
$paid_amount = number_format($item->paid_amount,2);
$due_amount = number_format($item->due_amount,2);

?>
<style type="text/css" media="print">
    @page  
    {
        size: auto;   /* auto is the initial value */
        margin: 0mm;  /* this affects the margin in the printer settings */
    }
</style>
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class="pull-left" style="margin-top: 20px;">
                  <?php if(in_array("$process[1]", $process_step)): ?>
                     <?php if(in_array("InvoiceController@edit", $routeArray)): ?>
                          <a href="<?php echo e(action('InvoiceController@edit',$id)); ?>"   title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                     <?php endif; ?>
                 <?php endif; ?>
                <?php if(in_array("$process[2]", $process_step)): ?>
                     <?php if(in_array("InvoiceController@delete", $routeArray)): ?>
                          <a href="<?php echo e(action('InvoiceController@destroy',$id)); ?>"  ><i class="fa fa-trash" aria-hidden="true" title="Delete"></i></a>
                     <?php endif; ?>
                 <?php endif; ?>
                <?php if(in_array("$process[8]", $process_step)): ?>
                     <?php if(in_array("InvoiceController@modify_status", $routeArray)): ?>
                         <a href="<?php echo e(action('InvoiceController@modify_status',$id)); ?>"   title="Modify Request"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> </a>
                     <?php endif; ?>
                <?php endif; ?>  
                <?php if(in_array("$process[3]", $process_step)): ?>   
                     <?php if(in_array("InvoiceController@approve_invoice", $routeArray)): ?>
                         <a href="<?php echo e(action('InvoiceController@approve_invoice',['id'=>$id,'invoice_id'=>$item->invoice_id,'client_user_id'=>$client->user_id])); ?>"   Onclick="return Confirmapprove();" title="Approve"><i class="fa fa-check" aria-hidden="true" ></i></a>
                    <?php endif; ?>
                <?php endif; ?>

                <?php if(in_array("$process[11]", $process_step)): ?>   
                     <?php if(in_array("InvoiceController@approvetoclient", $routeArray)): ?>
                         <a data-toggle="modal" data-target="#myModelApproveConfirm"  title="Send To Approve"><i class="fa fa-check" aria-hidden="true" ></i></a>
                    <?php endif; ?>
                <?php endif; ?>
                <?php if(in_array("$process[10]", $process_step)): ?>   
                     <?php if(in_array("InvoiceController@sendtoclient", $routeArray)): ?>
                         <a data-toggle="modal" data-target="#myModelApproveConfirm"  title="Send To Client"><i class="fa fa-check" aria-hidden="true" ></i></a>
                    <?php endif; ?>
                <?php endif; ?>
                <?php if(in_array("$process[4]", $process_step)): ?>
                    <?php if(in_array("InvoiceController@cancel", $routeArray)): ?>
                        <a   data-toggle="modal" data-target="#myModal" title="Cancel"><i class="fa fa-times" aria-hidden="true"></i>
                        </a>
                    <?php endif; ?>    
                <?php endif; ?>    
                <?php if(in_array("$process[5]", $process_step)): ?>
                  <?php if(in_array("InvoiceController@reject_invoice", $routeArray)): ?>
                        <a   data-toggle="modal" data-target="#myModelReject" title="Reject"><i class="fa fa-ban" aria-hidden="true"></i></a>
                  <?php endif; ?>    
                <?php endif; ?>
                <?php if(in_array("$process[7]", $process_step)): ?>
                  <?php if(in_array("InvoiceController@payment", $routeArray)): ?>
                    <a   data-toggle="modal" data-target="#myModelPayment" title="Payment"><i class="fa fa-money" aria-hidden="true"></i></a>
                 <?php endif; ?>    
                <?php endif; ?> 
                <?php if(in_array("$process[14]", $process_step)): ?>
                  <?php if(in_array("InvoiceController@follow_up", $routeArray)): ?>
                          <a href="<?php echo e(action('InvoiceController@follow_up',$id)); ?>"   title="Follow up"><i class="fa fa-arrow-up" aria-hidden="true"></i></a>
                   <?php endif; ?>
                <?php endif; ?>
                <?php if(in_array("$process[12]", $process_step)): ?>
                  <?php if(in_array("InvoiceController@payment_history", $routeArray)): ?>
                    <a   data-toggle="modal" data-target="#myPaymentHistory" title="Payment History"><i class="fa fa-history" aria-hidden="true"></i> 
                    </a>
                  <?php endif; ?>
                <?php endif; ?>

           <?php if(in_array("$process[6]", $process_step)): ?>
              <?php if(in_array("InvoiceController@cancel", $routeArray)): ?>
                 <a href="<?php echo e(action('InvoiceController@active_cancel_invoice',['id'=>$id,'invoice_id'=>$item->invoice_id,'client_user_id'=>$client->user_id])); ?>"  type="button" title="Active"><i class="fa fa-toggle-on" aria-hidden="true"></i> </a>     
              <?php endif; ?> 
           <?php endif; ?>
           <?php if(in_array("$process[13]", $process_step)): ?>
             <?php if(in_array("InvoiceController@print_invoice", $routeArray)): ?>
             <!--href="<?php echo e(action('InvoiceController@print_invoice',$id)); ?>" --->
                    <a type="button" onclick="window.print()" title="Print"><span><i class="fa fa-print"></i> </span> </a>
            <?php endif; ?>
          <?php endif; ?>

             <?php if(in_array("InvoiceController@update_spoc", $routeArray)): ?>
                    <a type="button" class="btn btn-warning"  data-toggle="modal" data-target="#myAddspoc" title="Change Client's First Spoc">Update SPOC</a> 
            <?php endif; ?>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="<?php echo e(url('home')); ?>">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li><a class="parent-item" href="<?php echo e(url('invoice')); ?>"> Invoice</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active">View Invoice</li>
                </ol>
            </div>
        </div>
        <?php echo csrf_field(); ?> <?php if(Session::has('message')): ?>
        <div class='alert alert-success'>
            <?php echo e(Session::get('message')); ?> <?php Session::forget('message'); ?>
        </div>
        <?php endif; ?>
        <div class="row">
            <div class="col-md-12">

</div>
</div>

<div class="container">
  <div class="card">
<div class="card-header">
Invoice
<strong><?php echo e($new_invoice_date); ?></strong> 
  <span class="float-right"> <strong>Status:</strong> <?php echo e($status->name); ?></span>

</div>
<div class="card-body">
<div class="row mb-4">
<div class="col-sm-6">
<h6 class="mb-3"><strong>Invoice No: &nbsp;&nbsp;<?php echo e($item->invoice_id); ?></strong></h6>
<div>
<strong><?php echo e($company->company_name); ?></strong>
</div>
<div><?php echo e($company->first_name); ?> <?php echo e($company->last_name); ?></div>
<div><?php echo e($company->address_line_1); ?> &nbsp;<?php echo e($company->city); ?> &nbsp;<?php echo e($company->state); ?> &nbsp;<?php echo e($company->pincode); ?></div>
<div>Email: <?php echo e($company->email); ?></div>
<div>Phone: <?php echo e($company->mobile); ?></div>
</div>

<div class="col-sm-6">
<h6 class="mb-3"><br></h6>
<div>
<strong>To,</strong>
</div>
<div><?php echo e($client->first_name); ?>&nbsp;&nbsp;<?php echo e($client->last_name); ?></div>
<div><?php echo e($client->address_line_1); ?> &nbsp;<?php echo e($client->city); ?> &nbsp;<?php echo e($client->state); ?> &nbsp;<?php echo e($client->pincode); ?></div>
<div>Email: <?php echo e($client->email); ?></div>
<div>Phone: <?php echo e($client->mobile); ?></div>
</div>



</div>

<div class="table-responsive-sm">
<table class="table table-striped">
<thead>
<tr>
<th class="center">#</th>
<th>Item</th>
<th>Value</th>
<th class="right">From Date</th>
  <th class="center">To Date</th>

</tr>
</thead>
<tbody>
    <?php $i = 0 ?> <?php $__currentLoopData = $product; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pro): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php $i++?>
        <?php 
            $value = number_format($pro->value,2);
                ?>
                    <tr>
                            <td class="text-center"><?php echo e($i); ?></td>
                            <td class="text-left"><?php echo e($pro->sub_head); ?>&nbsp;&nbsp;&nbsp;(<?php echo e($pro->head_name); ?>)</td>
                            <td class="text-right"><?php echo e($value); ?></td>
                            <td class="text-right"><?php echo e($pro->from_date); ?></td>
                            <td class="text-right"><?php echo e($pro->to_date); ?></td>

                    </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</tbody>
</table>
</div>
<div class="row">
<div class="col-lg-4 col-sm-5">

</div>

<div class="col-lg-4 col-sm-5 ml-auto">
<table class="table table-clear">
<tbody>
<tr>
<td class="left">
<strong>Total amount</strong>
</td>
<td class="right"><?php echo e($total); ?></td>
</tr>
<tr>
<td class="left">
<strong>Paid Amount:</strong>
</td>
<td class="right"><?php echo e($paid_amount); ?></td>
</tr>
<tr>
<td class="left">
 <strong>Due Amount:</strong>
</td>
<td class="right"><?php echo e($due_amount); ?></td>
</tr>
<tr>
<td class="left">
<strong>Total</strong>
</td>
<td class="right">
<strong><?php echo e($total); ?></strong>
</td>
</tr>
</tbody>
</table>

</div>

</div>

</div>
</div>
</div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">Cancel Invoice
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <form method="POST" action="<?php echo e(url('invoice/cancel_invoice')); ?>">
                    <?php echo csrf_field(); ?> <?php if(Session::has('message')): ?>
                    <div class='alert alert-success'>
                        <?php echo e(Session::get('message')); ?> <?php Session::forget('message'); ?>
                    </div>
                    <?php endif; ?>
                    <div class="form-group row">

                        <div class="col">

                            <label>Reason</label>

                            <span class="text-danger"><?php echo e($errors->first('status')); ?></span>
                        </div>

                    </div>
                    <div class="form-group row" id="cancel_button" style="">
                        <div class="col">
                            <textarea class="form-control" id="remark" name="remark"></textarea>

                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col">
<input type="hidden" class="form-control" id="invoice_id" value="<?php echo e($item->invoice_id); ?>" name="invoice_id">
    <input type="hidden" class="form-control" id="id" value="<?php echo e($item->id); ?>" name="id">
    <input type="hidden" class="form-control" id="client_user_id" value="<?php echo e($client->user_id); ?>" name="client_user_id">

                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col">
                            <button type="submit" class="btn btn-primary mr-2">Submit</button>
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<!-----------------------------reject invoice------------------------------->
<div class="modal fade" id="myModelReject" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">Reject Invoice
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <form method="POST" action="<?php echo e(url('invoice/reject_invoice')); ?>">
                    <?php echo csrf_field(); ?> <?php if(Session::has('message')): ?>
                    <div class='alert alert-success'>
                        <?php echo e(Session::get('message')); ?> <?php Session::forget('message'); ?>
                    </div>
                    <?php endif; ?>
                    <div class="form-group row">

                        <div class="col">

                            <label>Reason</label>

                            <span class="text-danger"><?php echo e($errors->first('status')); ?></span>
                        </div>

                    </div>
                    <div class="form-group row" id="cancel_button" style="">
                        <div class="col">
                            <textarea class="form-control" id="remark" name="remark"></textarea>

                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col">
<input type="hidden" class="form-control" id="invoice_id" value="<?php echo e($item->invoice_id); ?>" name="invoice_id">
    <input type="hidden" class="form-control" id="id" value="<?php echo e($item->id); ?>" name="id">
    <input type="hidden" class="form-control" id="client_user_id" value="<?php echo e($client->user_id); ?>" name="client_user_id">

                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col">
                            <button type="submit" class="btn btn-primary mr-2">Submit</button>
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<!----payment------------------------------->
<div class="modal fade" id="myModelPayment" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content" style="width:150%">
            <div class="modal-header">Make Payment
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <form method="POST" action="<?php echo e(url('invoice/invoice_payment')); ?>">
                    <?php echo csrf_field(); ?> <?php if(Session::has('message')): ?>
                    <div class='alert alert-success'>
                        <?php echo e(Session::get('message')); ?> <?php Session::forget('message'); ?>
                    </div>
                    <?php endif; ?>
                    <div class="form-group row">
                        <div class="col">
                            <label>Payment Date:</label>
                             <input   type="text" id="payment_date" name="payment_date" placeholder="Payment date"  class="form-control form_date" data-date-format="dd-mm-yyyy">
    
                        </div>
                        <div class="col">
                            <label>Payment Mode:</label>
                            <select class="form-control" name="payment_mode" id="payment_mode">
                                <option disabled selected>Select Status</option>
                                <option value="Cash">Cash </option>
                                <option value="Check">Check</option>
                                <option value="Credit-card">Credit Card </option>
                                <option value="Debit-card">Debit Card </option>
                                <option value="Online-banking">Online Banking </option>
                            </select>

                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col">
                            <label>Paid Amount: </label>
                            <input type="text" class="form-control" id="total_paid_amount" placeholder="Enter Paid Amount" name="paid_amount">

                        </div>

                        <div class="col">
                            <label>Due Amount:</label>
                            <input type="text" class="form-control" id="due_amount" placeholder="Enter Due Amount" name="due_amount" value="<?php echo e($item->due_amount); ?>">

                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6" style="display: none;" id="check_clearence_date">
                            <label>Check Clearnence Date:</label>
                            <input   type="text" id="clearence_date" name="clearence_date" placeholder="Check Clearnence Date"  class="form-control form_date" data-date-format="dd-mm-yyyy">

                        </div>
                        <div class="col-md-6">
                            <label>Payment Status:</label>
                            <select class="form-control" name="status" id="status">
                                <option disabled selected>Select Payment Status</option>
                                <option value="10">All Clear </option>
                                <option value="8">Pending</option>
                            </select>

                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col">

            <input type="hidden" class="form-control" id="invoice_id" value="<?php echo e($item->invoice_id); ?>" name="invoice_id">
                <input type="hidden" class="form-control" id="id" value="<?php echo e($item->id); ?>" name="id">
            <input type="hidden" class="form-control" id="client_user_id" value="<?php echo e($client->user_id); ?>" name="client_user_id">

                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col">
                            <button type="submit" class="btn btn-primary mr-2">Submit</button>
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<!----------------------Payment History ----------------------->
<div class="modal fade" id="myPaymentHistory" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">Payment History
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <table class="table">
                  <thead class="thead-light">
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">Date</th>
                      <th scope="col">Paid Amount</th>
                      <th scope="col">Due Amount</th>
                      <th scope="col">Payment Mode</th>
                    </tr>
                  </thead>
                  <tbody>
                <?php $i = 0
                    ?>
                     <?php $__currentLoopData = $payment_history; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $payment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                      <?php $i++
                    ?>
                    <tr>
                      <th scope="row"><?php echo e($i); ?></th>
                      <td><?php echo e($payment->payment_date); ?></td>
                      <?php
                        $payment_paid_amount = number_format($payment->paid_amount,2);
                        $payment_due_amount = number_format($payment->due_amount,2);
                     ?> 
                      <td><?php echo e($payment_paid_amount); ?></td>
                      <td><?php echo e($payment_due_amount); ?></td>
                      <td><?php echo e($payment->payment_mode); ?></td>
                      
                    </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                 <tfoot>
                    <tr>
                     <th></th>
                     <th></th>
                     <th></th>
                     <th>Total Amount</th>
                     <td><?php echo e($total); ?></td>
                    </tr> 

                    <tr>
                     <th></th>
                     <th></th>
                     <th></th>
                     <th>Total Paid Amount</th>
                     <td><?php echo e($paid_amount); ?></td>
                    </tr>
                    
                    <tr>
                     <th></th>
                     <th></th>
                     <th></th>
                     <th>Total Due Amount</th>
                     <td><?php echo e($due_amount); ?></td>
                   </tr>
                 </tfoot>   

                  </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<!-----------------Approve confirmation---------------------------------------->
<!-----------------------------reject invoice------------------------------->
<div class="modal fade" id="myModelApproveConfirm" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">Approve Confirmation
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
               
                    <?php echo csrf_field(); ?> <?php if(Session::has('message')): ?>
                    <div class='alert alert-success'>
                        <?php echo e(Session::get('message')); ?> <?php Session::forget('message'); ?>
                    </div>
                    <?php endif; ?>
                    <div class="form-group row">

                        <div class="col-md-3"> 
                         
                        
                          <?php if($role->name =='Client'): ?>
                          <a href="<?php echo e(action('InvoiceController@invoice_sent_to_finance',['id'=>$id,'invoice_id'=>$item->invoice_id,'client_user_id'=>$client->user_id])); ?>" class="btn btn-round btn-primary" type="submit">Approve </a>
                       
                        <?php else: ?>
                        
                         <a href="<?php echo e(action('InvoiceController@invoice_sent_to_client',['id'=>$id,'invoice_id'=>$item->invoice_id,'client_user_id'=>$client->user_id])); ?>" class="btn btn-round btn-primary" type="submit">Send To client </a>
                         <?php endif; ?>
                      </div>
                      &nbsp;&nbsp;&nbsp;
                      <div class="col-md-3">
                           <button type="button" class="btn btn-default" data-dismiss="modal">Ask me later</button>
                    </div>

                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<!----spoc------------------------------->
<div class="modal fade" id="myAddspoc" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content" style="width:150%">
            <div class="modal-header">Update Spoc
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <form method="POST" action="<?php echo e(url('invoice/update_spoc')); ?>">
                    <?php echo csrf_field(); ?> <?php if(Session::has('message')): ?>
                    <div class='alert alert-success'>
                        <?php echo e(Session::get('message')); ?> <?php Session::forget('message'); ?>
                    </div>
                    <?php endif; ?>
                    <div class="form-group row">

                        <div class="col">
                            <label>First Contact SPOC:</label>
                                    <select name="first_contact_spoc" id="first_contact_spoc" class="form-control">
                                         <option disabled selected>Select SPOC</option>
                                        <?php if(isset($userdropdown)): ?>
                                      <?php $__currentLoopData = $userdropdown; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $dropdownGroup): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                      <option value ="<?php echo e($key); ?>"><?php echo e($dropdownGroup); ?> </option>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                       <?php endif; ?>
                        </select>

                        </div>
                    </div>

                    <div class="form-group row">
            <div class="col">
            <input type="hidden" class="form-control" id="invoice_id" value="<?php echo e($item->invoice_id); ?>" name="invoice_id">
            <input type="hidden" class="form-control" id="id" value="<?php echo e($item->id); ?>" name="id">
            <input type="hidden" class="form-control" id="client_user_id" value="<?php echo e($client->id); ?>" name="client_id">
            </div>
                    </div>
                    <div class="form-group row">
                        <div class="col">
                            <button type="submit" class="btn btn-primary mr-2">Submit</button>
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<?php echo $__env->make('footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<script type="text/javascript">
    $('#payment_mode').on('change', function() {
        var selValue = $('#payment_mode').val();

        if (selValue == 'Check') {
            $("#check_clearence_date").show();
        } else {
            $("#check_clearence_date").hide();
        }

    })
    
    $('#total_paid_amount').change(function() {

        $curent_paid_amount=$('#total_paid_amount').val();
        $due_amount=$('#due_amount').val();
        if($due_amount > $curent_paid_amount){
        $new_due_amount=parseFloat($due_amount) - parseFloat($curent_paid_amount);
            $('#due_amount').val($new_due_amount);    
        }
   

});

</script>
<?php if(!empty(Session::get('error_code')) && Session::get('error_code') 
 == 5): ?>
  <script>
    $(function() {
      $('#myModelApproveConfirm').modal('show');
    });
</script>
<script>
function myFunction() {
  window.print();
}
</script>
<?php endif; ?><?php /**PATH /home/ptindiao/public_html/demo/finance/resources/views/invoice/view.blade.php ENDPATH**/ ?>