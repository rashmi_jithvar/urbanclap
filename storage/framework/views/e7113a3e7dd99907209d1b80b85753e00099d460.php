
<?php echo $__env->make('header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
$role_id = Auth::user()->role_id;

<div class="page-content-wrapper">
                <div class="page-content" style="min-height:1271px">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Invoice</div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="<?php echo e(url('home')); ?>">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li><a class="parent-item" href="<?php echo e(url('invoice/create')); ?>">Add Invoice</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active">Invoice</li>
                            </ol>
                        </div>
                    </div>
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="card">

            <div class="card-body">
            <?php if(in_array("InvoiceController@create", $routeArray)): ?>
              <a href="<?php echo e(url('invoice/create')); ?>" class="btn btn-info" role="button">Create Invoice</a>
            <?php endif; ?>
<br><br>
              <h4 class="card-title">Invoice</h4>
              <div class="row">

                <div class="col-12">                            
                  <?php if(session()->has('message')): ?>
                                <div class="alert alert-success">
                                    <?php echo e(session()->get('message')); ?>

                                </div>
                  <?php endif; ?>
                  <div class="table-responsive">
                    <table id="example1" class="display" style="width:100%;">
                      <thead>
                        <tr>
                            <th>#</th>
                            <th>Invoice No.</th>
                            <th>Invoice Date</th>
                            <th>Client Name</th>
                            <th>Total Amount</th>
                            <th>Paid Amount</th>
                            <th>Due  Amount</th>
                            <th>File</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php $i = 0
                         ?>
                        <?php $__currentLoopData = $invoice; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $role): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php $i++
                         ?>

                        <tr>
                            <td><?php echo e($i); ?></td>

                            <td><?php echo e($role->invoice_id); ?></td>
                            <td><?php echo e($new_invoice_date); ?></td>
                            <td><?php echo e($role->first_name); ?> &nbsp;&nbsp;<?php echo e($role->last_name); ?></td>
                            <?php
                                $total = number_format($role->total,2);
                                $paid_amount = number_format($role->paid_amount,2);
                                $due_amount = number_format($role->due_amount,2);
                            ?>
                            <td><?php echo e($total); ?></td>
                            <td><?php echo e($paid_amount); ?></td>
                            <td><?php echo e($due_amount); ?></td>
                            
                            <td><?php if($role->file !=''): ?>  <a href="<?php echo e(asset('public/invoice_file/' . $role->file)); ?>" target="_blank">View</a> <?php endif; ?></td>
                            
                         
                            <td>
                            <?php if($role->status =='Pending'): ?>
                               <label class="badge badge-info"><?php echo e($role->status); ?></label>          
                            </td>
                            <?php endif; ?>
                            <?php if($role->status =='Reject'): ?>
                               <label class="badge badge-danger"><?php echo e($role->status); ?></label>          
                     
                            <?php endif; ?>
                            <?php if($role->status =='Approved'): ?>
                               <label class="badge badge-success"><?php echo e($role->status); ?></label>          
                   
                            <?php endif; ?>
                            <?php if($role->status =='Cancel'): ?>
                               <label class="badge badge-warning"><?php echo e($role->status); ?></label>          
                     
                            <?php endif; ?>
                            <?php if($role->status =='Edit'): ?>
                               <label class="badge badge-warning"><?php echo e($role->status); ?></label>          
                     
                            <?php endif; ?>
                            <?php if($role->status =='PendingPayment'): ?>
                               <label class="badge badge-primary"><?php echo e($role->status); ?></label>          
                     
                            <?php endif; ?>
                            <?php if($role->status =='Active'): ?>
                               <label class="badge badge-success"><?php echo e($role->status); ?></label>          
                    
                            <?php endif; ?>
                            <?php if($role->status =='ApproveATFinance'): ?>
                               <label class="badge badge-info">At finance</label>          
                         
                            <?php endif; ?>
                            <?php if($role->status =='ApproveAtClient'): ?>
                               <label class="badge badge-info">At Client</label>          
                      
                            <?php endif; ?>
                            <?php if($role->status =='ModifyRequest'): ?>
                               <label class="badge badge-info"><?php echo e($role->status); ?></label>          
                           
                            <?php endif; ?>
                            <td>
            
                    <form action="<?php echo e(action('InvoiceController@destroy', $role->id)); ?>" method="post">
                    <?php echo e(csrf_field()); ?>


                      
                         <?php if(in_array("InvoiceController@view", $routeArray)): ?>
                          <a href="<?php echo e(action('InvoiceController@view',$role->id)); ?>"  ><i class="fa fa-eye" aria-hidden="true" title="view"></i></a>
                         <?php endif; ?>
                         <?php if(in_array("InvoiceController@edit", $routeArray)): ?>
                          <a href="<?php echo e(action('InvoiceController@edit',$role->id)); ?>"  ><i class="fa fa-pencil" aria-hidden="true" title="Edit"></i></a>
                          <?php endif; ?>
                        <?php if(Auth::user()->role_id !='5'): ?>  
                        <?php if($role->status == 'Approved' || $role->status == 'ModifyRequest' || $role->status == 'ApproveATFinance'): ?>
                          <?php if(in_array("InvoiceController@modify_status", $routeArray)): ?>
                          <a href="<?php echo e(action('InvoiceController@modify_status',$role->id)); ?>"  ><i class="fa fa-pencil-square-o" aria-hidden="true" title="Modify Request"></i></a>
                          <?php endif; ?>
                        <?php endif; ?>
                        <?php endif; ?>
                         <?php if(in_array("InvoiceController@follow_up", $routeArray)): ?>
                          <a href="<?php echo e(action('InvoiceController@follow_up',$role->id)); ?>" ><i class="fa fa-arrow-up" aria-hidden="true" title="Follow Up"></i></a>
                         <?php endif; ?>
                         <?php if(in_array("InvoiceController@delete", $routeArray)): ?>
                         <a href="<?php echo e(action('InvoiceController@destroy', $role->id)); ?>"  Onclick="return ConfirmDelete();" ><i class="fa fa-trash" aria-hidden="true" title="view"></i></a>
                        
                         <?php endif; ?> 
                        
                    </form>
                              
                            </td>
                        </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

<?php echo $__env->make('footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/ptindiao/public_html/demo/finance/resources/views/invoice/index.blade.php ENDPATH**/ ?>