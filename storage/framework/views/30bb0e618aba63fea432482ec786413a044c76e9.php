<?php echo $__env->make('header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>  
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/additional-methods.min.js"></script>
  <style>
   .error{ color:red; } 
  </style>
<div class="page-content-wrapper">
                <div class="page-content" style="min-height:1271px">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Add Client </div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="<?php echo e(url('home')); ?>">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                            <li><i class="fa fa-list"></i>&nbsp;<a class="parent-item" href="<?php echo e(url('client')); ?>"> Client</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active">Add Client</li>
                            </ol>
                        </div>
                    </div>
<div class="main-panel">
    <div class="content-wrapper">
       
            <div class="row">

                <div class="col-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Add Client</h4>
                          <form  id="customerform" method="POST" action="<?php echo e(url('client/store')); ?>" enctype="multipart/form-data">
                                            <?php echo csrf_field(); ?>
                                            <?php if(Session::has('message')): ?>
                                                    <div class='alert alert-success'>
                                                    <?php echo e(Session::get('message')); ?>

                                                    <?php
                                                    Session::forget('message');
                                                    ?>
                                                    </div>
                                            <?php endif; ?>
                                <div class="form-group row">
                
                                    <div class="col">
                                        <label>Company Name:<span class="required-mark">*</span></label>
                                        <input type="text"  id="company_name" class="form-control"  placeholder="Enter company name" name="company_name" >
                                         <span class="text-danger"><?php echo e($errors->first('company_name')); ?></span> 
                                    </div>  
                                    <div class="col">
                                        <label>First Name:<span class="required-mark">*</span></label>
                                        <input type="text" id="first_name" class="form-control"  placeholder="Enter First Name" name="first_name" >
                                         <span class="text-danger"><?php echo e($errors->first('first_name')); ?></span>
                                        
                                    </div>
                                    <div class="col">
                                        <label>Last Name:</label>
                                        <input type="text" id="last_name" class="form-control"  placeholder="Enter Last name" name="last_name" >
                                         <span class="text-danger"><?php echo e($errors->first('last_name')); ?></span>
                                        
                                    </div>
                              </div>
                              <div class="form-group row">    
                                    <div class="col">
                                        <label>Mobile:<span class="required-mark">*</span></label>
                                        <input  id="mobile" class="form-control"  placeholder="Enter Mobile Name" name="mobile"  maxlength="10">
                                         <span class="text-danger"><?php echo e($errors->first('mobile')); ?></span>
                                        
                                    </div>
                                   
                                    <div class="col">
                                        <label>Email :<span class="required-mark">*</span></label>
                                        <input type="text" id="email" class="form-control"  placeholder="Enter Email" name="email" >
                                         <span class="text-danger"><?php echo e($errors->first('email')); ?></span>
                                        
                                    </div>
                  
                                    <div class="col">
                                        <label>Website :</label>
                                        <input type="text" id="website" class="form-control"  placeholder="Enter Website" name="website" >
                                         <span class="text-danger"><?php echo e($errors->first('website')); ?></span>
                                        
                                    </div>
                              </div> 
                                   
                              <div class="form-group row">  
                                 
                                    <div class="col">
                                      <label>Address Line 1 :<span class="required-mark">*</span></label>
                                        <input type="text" id="address_line_1" class="form-control"  placeholder="Enter Address Line 1" name="address_line_1" >
                                         <span class="text-danger"><?php echo e($errors->first('address_line_1')); ?></span>
                                        
                                    </div>
                             
                                    <div class="col">
                                      <label>Address Line 2 :</label>
                                        <input type="text" id="address_line_2" class="form-control"  placeholder="Enter Address Line 2" name="address_line_2" >
                                         <span class="text-danger"><?php echo e($errors->first('address_line_2')); ?></span>
                                        
                                    </div>

                                  <div class="col">
                                                <label for="">State<span class="required-mark">*</span>
                                                </label>
                                <input type="text"  id="state" class="form-control"  placeholder="Enter State" name="state" >
                                <span class="text-danger"><?php echo e($errors->first('state')); ?></span>  
                                </div>
                              </div>
                              <div class="form-group row">  
                                <div class="col">
                                        <label for="">City<span class="required-mark">*</span>
                                        </label>
                                          <input type="text"  id="city" class="form-control"  placeholder="Enter City" name="city" >
                                              <span class="text-danger"><?php echo e($errors->first('city')); ?></span> 
                                </div>
                                <div class="col">
                                    <label>Pincode<span class="required-mark">*</span>
                                    </label>
                                    <input type="text" class="form-control" id="pincode" placeholder="Enter Pincode" name="pincode" maxlength="6" minlength="6">
                                    <span class="text-danger"><?php echo e($errors->first('pincode')); ?>

                                    </span>      
                                  </div>

                    
                                <div class="col">
                                        <label>Logo:</label>
                                        <input type="file" class="form-control" id="image" placeholder="Enter image" name="image" >
                                      <span class="text-danger"><?php echo e($errors->first('image')); ?></span>    
                                </div>

                          </div>
                          <div class="form-group row">
                
                                    <div class="col">
                                        <label>GST Number:</label>
                                        <input type="text"  id="gst_number" class="form-control"  placeholder="Enter gst number" name="gst_number" >
                                         <span class="text-danger"><?php echo e($errors->first('gst_number')); ?></span> 
                                    </div>  
                                    <div class="col">
                                        <label>Credit Period:</label>
                                        <input type="text" id="credit_period" class="form-control"  placeholder="Enter credit period  example 15 days" name="credit_period" >
                                         <span class="text-danger"><?php echo e($errors->first('credit_period')); ?></span>
                                        
                                    </div>
                                    <div class="col">
                                        <label>Credit Period Defination:<span class="required-mark">*</span></label>
                                       <select class="form-control" name="credit_period_defination">
                                         <option disabled selected>--select---</option>
                                         <option value='1'>Calculation from billing date</option>
                                         <option value='2'>Calculation from Acceptance</option>
                                         <option value='3'>Calculation from Finance</option>
                                         
                                       </select>
                                         <span class="text-danger"><?php echo e($errors->first('credit_period_defination')); ?></span>
                                        
                                    </div>
                              </div>
                              <div class="form-group row">
                
                                    <div class="col">
                                        <label>SPOC from Business Development:</label>
                                      <select name="spoc_business" id="spoc_business" class="form-control select2">
                                         <option disabled selected>Select</option>
                                        <?php if(isset($role_business)): ?>
                                      <?php $__currentLoopData = $role_business; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $dropdownGroup): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                      <option value ="<?php echo e($key); ?>"><?php echo e($dropdownGroup); ?> </option>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                       <?php endif; ?>
                                    </select>
                                      
                                         <span class="text-danger"><?php echo e($errors->first('spoc_business')); ?></span> 
                                    </div>  
                                    <div class="col">
                                        <label>SPOC from OPS:</label>
                                    <select name="spoc_ops" id="spoc_ops" class="form-control select2">
                                         <option disabled selected>Select</option>
                                        <?php if(isset($role_ops)): ?>
                                      <?php $__currentLoopData = $role_ops; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $dropdownGroup): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                      <option value ="<?php echo e($key); ?>"><?php echo e($dropdownGroup); ?> </option>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                       <?php endif; ?>
                                    </select>
                                    
                                         <span class="text-danger"><?php echo e($errors->first('spoc_ops')); ?></span>
                                        
                                    </div>
                                    <div class="col">
                                        <label>SPCO from Finance:</label>
                                      <select name="spoc_finance" id="spoc_finance" class="form-control select2">
                                         <option disabled selected>Select</option>
                                        <?php if(isset($role_finance)): ?>
                                      <?php $__currentLoopData = $role_finance; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $dropdownGroup): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                      <option value ="<?php echo e($key); ?>"><?php echo e($dropdownGroup); ?> </option>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                       <?php endif; ?>
                                    </select>
                            
                                         <span class="text-danger"><?php echo e($errors->first('spoc_finance')); ?></span>
                                        
                                    </div>
                              </div>
                           
                              <div class="form-group row">
                
                                    <div class="col-md-4">
                                        <label>First Contact SPOC:</label>
                                      <select name="first_contact_spoc" id="first_contact_spoc" class="form-control select2">
                                         <option disabled selected>Select SPOC</option>
                                        <?php if(isset($userdropdown)): ?>
                                      <?php $__currentLoopData = $userdropdown; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $dropdownGroup): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                      <option value ="<?php echo e($key); ?>"><?php echo e($dropdownGroup); ?> </option>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                       <?php endif; ?>
                                    </select>
                                         <span class="text-danger"><?php echo e($errors->first('first_contact_spoc')); ?></span> 
                                    </div>  
                                    <!--<div class="col">
                                        <label>Password:</label>
                                        <input type="password" id="password" class="form-control"  placeholder="Enter password" name="password" >
                                         <span class="text-danger"><?php echo e($errors->first('password')); ?></span>
                                        
                                    </div>
                                    <div class="col">
                                        <label>Confirm Password:</label>
                                       <input type="password"  id="confirm_password " class="form-control"  placeholder="Enter Comfirm Password" name="confirm_password" >
                                         <span class="text-danger"><?php echo e($errors->first('confirm_password')); ?></span>
                                        
                                    </div>-->
                                  <div class="col-md-4">
                                        <label>Warehouse:</label>
                                      <select name="warehouse_id[]" id="warehouse_id" class="warehouse form-control select2-multiple" multiple required="">
                                         <option disabled selected>Select Warehouse</option>
                                        <?php if(isset($warehousedropdown)): ?>
                                      <?php $__currentLoopData = $warehousedropdown; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $dropdownGroup): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                      <option value ="<?php echo e($key); ?>"><?php echo e($dropdownGroup); ?> </option>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                       <?php endif; ?>
                                    </select>
                                         <span class="text-danger"><?php echo e($errors->first('warehouse_id')); ?></span> 
                                    </div> 
                              </div>
                              <div class="form-group row">
                
                                
                      
                              </div>
                              <div class="form-group row">
                               <button type="submit" class="btn btn-primary mr-2">Submit</button>
                             </div>
                              </div>
   
                            </form>
                        </div>
                    </div>
                </div>

            </div>
    </div>
</div>
</div>
</div>
<script>
   if ($("#customerform").length > 0) {
    $("#customerform").validate({
     
    rules: {
    first_name: {
        required: true,
        maxlength: 150
      },
    company_name: {
        required: true,
        maxlength: 255
      },
    mobile: {
        required: true,
        maxlength: 10,
        number: true,

      },
    email: {
        required: true,
        email: true,
      },
    address_line_1: {
        required: true,
      },
   
    state: {
          required: true,
          maxlength: 150,
    }, 
    city: {
        required: true,
      },
   
    pincode: {
          required: true,
          maxlength: 6,
    }, 
    credit_period_defination: {
          required: true,
          maxlength: 50,
    }, 
    website: {  
        url: true
    },
    first_contact_spoc: {
          required: true,
          maxlength: 255,
    }, 
    warehouse_id: {
          required: true,
          
    }, 
   
  },
    messages: {
       
      first_name: {
        required: "Please Enter First name",
        maxlength: "Your last name maxlength should be 50 characters long."
      },

    company_name: {
        required: "Please Enter Company name",
        maxlength: "Your company_name should be 255 characters long."
      },
    mobile: {
        required: "Please Enter Mobile number",
        maxlength: "Your mobile should be 10 characters long."
      },  
    email: {
        required: "Please Enter Email address",
          email: "Please enter valid email",
        
      },
    address_line_1: {
        required: "Please Enter Address",
       
      }, 
    state: {
          required: "Please Enter state",
          
        },
    city: {
          required: "Please Enter City",
          
      },
    pincode: {
          required: "Please Enter pincode",
          
      },
    credit_period_defination: {
          required: "Please Enter credit_period_defination",
          
        },
    first_contact_spoc: {
          required: "Please Select First Contact SPOC",
          
        },
    website: {
          required: "Please Enter valid url",
          
        },
    warehouse_id: {
          required: "Please Select Warehouse",
          
        },    
   
       
    },
    })
  }
</script>
<?php echo $__env->make('footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/ct/public_html/finance/resources/views/client/create.blade.php ENDPATH**/ ?>