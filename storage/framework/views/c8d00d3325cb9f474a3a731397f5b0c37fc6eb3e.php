<?php echo $__env->make('header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="page-content-wrapper">
                <div class="page-content" style="min-height:1271px">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Create Permission</div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="<?php echo e(url('home')); ?>">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li><a class="parent-item" href="<?php echo e(url('permission')); ?>"> Permission</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>                            
                                </li>
                                <li class="active">Add Permission </li>
                            </ol>
                        </div>
                    </div>
<div class="main-panel">
    <div class="content-wrapper">
       
            <div class="row">

                <div class="col-6 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Add Permission </h4>
                          <form method="POST" action="<?php echo e(url('permission/store')); ?>">
                                            <?php echo csrf_field(); ?>
                                            <?php if(Session::has('message')): ?>
                                                    <div class='alert alert-success'>
                                                    <?php echo e(Session::get('message')); ?>

                                                    <?php
                                                    Session::forget('message');
                                                    ?>
                                                    </div>
                                            <?php endif; ?>
                                <div class="form-group">
                                      <label for="City">Choose GroupName</label>
                                      <select name="group_id"  class="form-control">
                                         <option disabled selected >Select Permission Group </option>
                                <?php if(isset($groupdropdown)): ?>
                                <?php $__currentLoopData = $groupdropdown; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $dropdownGroup): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    
                                      <option value ="<?php echo e($key); ?>"><?php echo e($dropdownGroup); ?> </option>
                                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                       <?php endif; ?>
                                    </select>
                                    <span class="text-danger"><?php echo e($errors->first('group_id')); ?></span> 
                                </div>            
                                <div class="form-group row">
                
                                    <div class="col">
                                        <label>Name:</label>
                                        <input type="text" class="form-control"  placeholder="Enter Name" name="name" >
                                        <span class="text-danger"><?php echo e($errors->first('name')); ?></span>
                                    </div>
                                </div>
                                <div class="form-group row">
                
                                    <div class="col">
                                        <label>Controller Name:</label>
                                        <input type="text" class="form-control" id="roles" placeholder="Enter Controller Name" name="controller_name"  >
                                        <span class="text-danger"><?php echo e($errors->first('controller_name')); ?></span>
                                    </div>
                                </div>
                            <div class="form-group row">
                
                                    <div class="col">
                                        <label>Action Name:</label>
                                        <input type="text" class="form-control" id="roles" placeholder="Enter Action name" name="action_name"  >
                                        <span class="text-danger"><?php echo e($errors->first('action_name')); ?></span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col">
                                        <label for="exampleSelectGender">Status</label>
                                        <select class="form-control"  name="status" required>
                                            <option disabled selected >Select Status</option>
                                            <option value="1" >Active</option>
                                            <option value="0" >Inactive</option>
                                        </select>
                                        <span class="text-danger"><?php echo e($errors->first('status')); ?></span>
                                    </div>

                                </div>
                               <button type="submit" class="btn btn-primary mr-2">Submit</button>
                                
                            </form>
                        </div>
                    </div>
                </div>

            </div>
    </div>
</div>
</div>
</div>

<?php echo $__env->make('footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/ptindiao/public_html/demo/finance/resources/views/permission/create.blade.php ENDPATH**/ ?>