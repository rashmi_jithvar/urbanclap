<?php echo $__env->make('header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small> </small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo e(url('home')); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>
	<div class="col-md-12">
	
	<section class="content">
      <div class="row">
        <!-- left column -->
         <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Member</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="<?php echo e(url('update')); ?>/<?php echo e($user->id); ?>" method="post">
              <input type="hidden" name="_token" id="csrf-token" value="<?php echo e(Session::token()); ?>" />
              <div class="box-body">

                <div class="row">
                  <div class="col-md-12">
                    <?php if($errors->any()): ?>
                        <div class="alert alert-danger">
                            <ul>
                                <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <li><?php echo e($error); ?></li>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </ul>
                        </div>
                    <?php endif; ?>
                  </div>
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1">Name</label>
                  <input type="text" name="name" value="<?php echo e($user->name); ?>" class="form-control" placeholder="Enter Name">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Email address</label>
                  <input type="email" name="email"  value="<?php echo e($user->email); ?>" class="form-control" placeholder="Enter email">
                </div> 
                <div class="form-group">
                  <label for="exampleInputEmail1">Member type</label>
                  <select class="form-control" name="user_type">
                    <option <?php if( $user->user_type == 'Resident' ) echo 'selected' ?> value="Resident">
                      Resident                   
                    </option>
                    <option <?php if( $user->user_type == 'Staff' ) echo 'selected' ?>  value="Staff">
                      Staff                   
                    </option>
                    <option <?php if( $user->user_type == 'Secretary' ) echo 'selected' ?>  value="Secretary">
                      Secretary                   
                    </option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">User Status</label>
                  <select class="form-control" name="status">
                    <option value="Active" <?php if( $user->status == 'Active' ) echo 'selected' ?>>
                      Active                   
                    </option>
                    <option value="Inactive" <?php if( $user->status == 'Inactive' ) echo 'selected' ?>>
                      Inactive                   
                    </option> 
                  </select>
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
		</div> 
	</section>
	</div>
</div>
<?php echo $__env->make('footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\wamp\www\mcgill-laravel\resources\views/member/edit.blade.php ENDPATH**/ ?>