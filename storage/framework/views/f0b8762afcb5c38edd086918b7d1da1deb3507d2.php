<?php echo $__env->make('header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="page-content-wrapper">
                <div class="page-content" style="min-height:1271px">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">User </div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.html">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                            
                                </li>
                                <li class="active">User</li>
                            </ol>
                        </div>
                    </div>
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="card">

            <div class="card-body">
            	<a href="<?php echo e(url('user/create')); ?>" class="btn btn-info" role="button">Create User</a>

<br><br>
              <h4 class="card-title">User </h4>
              <div class="row">

                <div class="col-12">
                  <div class="table-responsive">
                    <table id="order-listing" class="table">
                      <thead>
                        <tr>
                            <th>S.no. #</th>
                            <th>Name </th>
                            <th>Username</th>
                            <th>Email</th>
                            <th>Status</th>
                            <th>Role Id</th>
                            <th>Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php $__currentLoopData = $user; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td><?php echo e($user->id); ?></td>
                            <td><?php echo e($user->name); ?></td>
                            <td><?php echo e($user->username); ?></td>
                            <td><?php echo e($user->email); ?></td>
                            <td>
                         <?php if($user->status == '1'): ?>
                               <label class="badge badge-success">Active</label>
                          
                          <?php else: ?>
                                <label class="badge badge-danger">Pending</label>
                           <?php endif; ?>
                              
                            </td>
                             <td><?php echo e($user->role_id); ?></td>
                            <td>
            
                             
                    <form action="<?php echo e(action('UserController@destroy', $user->id)); ?>" method="post">
                    <?php echo e(csrf_field()); ?>

                        <input name="_method" type="hidden" value="DELETE">
                              <a href="<?php echo e(action('UserController@edit',$user->id)); ?>" class="btn btn-info" role="button"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                              <a href="<?php echo e(action('UserController@view',$user->id)); ?>" class="btn btn-info" role="button"><i class="fa fa-eye"></i>

</a>
                        <button class="btn btn-danger" type="submit"><i class="fa fa-trash" ></i></button>
                    </form>
                              
                            </td>
                        </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
<?php echo $__env->make('footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\wamp\www\finance_laravel\finance\resources\views/user/index.blade.php ENDPATH**/ ?>