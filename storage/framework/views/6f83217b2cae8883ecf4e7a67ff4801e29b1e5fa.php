<?php echo $__env->make('header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="page-content-wrapper">
                <div class="page-content" style="min-height:1271px">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Create Permission Group </div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="<?php echo e(url('/home')); ?>">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li><a class="parent-item" href="<?php echo e(url('/permission_group')); ?>">Permission Group</a>&nbsp;<i class="fa fa-angle-right"></i>                               
                                </li>
                                <li class="active">Add Permission Group</li>
                            </ol>
                        </div>
                    </div>
<div class="main-panel">
    <div class="content-wrapper">
       
            <div class="row">

                <div class="col-6 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Add Permission Group</h4>
                          <form method="POST" action="<?php echo e(url('permissiongroup/store')); ?>">
                                            <?php echo csrf_field(); ?>
                                            <?php if(Session::has('message')): ?>
                                                    <div class='alert alert-success'>
                                                    <?php echo e(Session::get('message')); ?>

                                                    <?php
                                                    Session::forget('message');
                                                    ?>
                                                    </div>
                                            <?php endif; ?>
                                <div class="form-group row">
                
                                    <div class="col">
                                        <label>Permission:</label>
                                        <input type="text" class="form-control" id="name" placeholder="Enter name" name="name" >
                                        <span class="text-danger"><?php echo e($errors->first('name')); ?></span> 
                                    </div>
                                </div>
                                <div class="form-group row">
                                   <!-- <div class="col">
                                        <label for="exampleSelectGender">Status</label>
                                        <select class="form-control"  name="status" >
                                            <option disabled selected>Select Status</option>
                                            <option value="1">Active</option>
                                            <option value="0">Inactive</option>
                                        </select>
                                        <span class="text-danger"><?php echo e($errors->first('status')); ?></span> 
                                    </div>-->
                                    
                                </div>
                               <button type="submit" class="btn btn-primary mr-2">Submit</button>
                                
                            </form>
                        </div>
                    </div>
                </div>

            </div>
    </div>
</div>
</div>
</div>

<?php echo $__env->make('footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/ptindiao/public_html/demo/finance/resources/views/permission_group/create.blade.php ENDPATH**/ ?>