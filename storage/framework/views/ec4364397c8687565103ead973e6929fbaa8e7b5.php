<?php echo $__env->make('header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="page-content-wrapper">
                <div class="page-content" style="min-height:1271px">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">User </div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                              <?php if(in_array("UserController@create", $routeArray)): ?>
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="<?php echo e(url('/home')); ?>">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <?php endif; ?>
                                <li class="active">User</li>
                            </ol>
                        </div>
                    </div>
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="card">

            <div class="card-body">
        <?php if(in_array("UserController@create", $routeArray)): ?>
            	<a href="<?php echo e(url('user/create')); ?>" class="btn btn-info" role="button">Create User</a>
              <?php endif; ?>
              

<br><br>
              <h4 class="card-title">User </h4>
              <div class="row">

                <div class="col-12">
                  <div class="table-responsive">
                    <table id="example1"  class="table" style="width:100%;">
                      <thead>
                        <tr>
                            <th>Sr.no.</th>
                            <th>Name </th>
                          
                            <th>Email</th>
                            <th>Status</th>
                            <th>Role Id</th>
                            <th>Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php $i = 0
                         ?>
                          <?php $__currentLoopData = $user; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                          <?php $i++
                        ?>
                        <tr>
                            <td><?php echo e($i); ?></td>
                            <td><?php echo e($user->name); ?></td>
                           
                            <td><?php echo e($user->email); ?></td>
                             <td><?php echo e($user->role_name); ?></td>
                            <td>
                         <?php if($user->status == 'Active'): ?>
                               <label class="badge badge-success">Active</label>
                          
                          <?php else: ?>
                                <label class="badge badge-danger">Pending</label>
                           <?php endif; ?>
                              
                            </td>
                             <!--<td><?php echo e($user->role_id); ?></td>-->
                            <td>
            
                             
                  
                  
                        <?php if(in_array("UserController@edit", $routeArray)): ?>
                              <a href="<?php echo e(action('UserController@edit',$user->id)); ?>" ><i class="fa fa-pencil" aria-hidden="true"></i></a>
                              <?php endif; ?>
                              <?php if(in_array("UserController@view", $routeArray)): ?>
                              <a href="<?php echo e(action('UserController@view',$user->id)); ?>"  ><i class="fa fa-eye"></i> </a>
                             <?php endif; ?>  
                             <?php if(in_array("UserController@delete", $routeArray)): ?>
                             <a href="<?php echo e(action('UserController@destroy', $user->id)); ?>"  ><i class="fa fa-trash" aria-hidden="true" title="Delete" Onclick="return ConfirmDelete();"></i></a>
                             <?php endif; ?>

        
                    
                              
                            </td>
                        </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div></div>
    
<?php echo $__env->make('footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/ct/public_html/finance/resources/views/user/index.blade.php ENDPATH**/ ?>