<style type="text/css">
    th{
        /*font-weight: bold;*/
        
        font-size: 10px;

    }


    #table {
        border: #000 solid 1px;
      border-collapse: collapse;
     padding:5px 0 5px 5px;
    }


    td{
        padding:5px 0 5px 5px;
        text-align: left; 
        font-size: 14px;
    }
    #div2{
        padding-left:10px;
        
    }

    span{
        font-weight: bold;
    }
    span, p{
       font-family: Roboto, "Helvetica Neue", Helvetica, Arial, sans-serif;    
       font-size: 12px;
       line-height: 8px;
    }
    #tbl3{
         font-family: Roboto, "Helvetica Neue", Helvetica, Arial, sans-serif; 
          font-size: 12px;
          line-height: 10px;
    }
    h4,h2{
        font-family: Roboto, "Helvetica Neue", Helvetica, Arial, sans-serif;
        font-size:  14px;
    }

    #tbl2{
        border-right: #000 solid 1px; border-left: #000 solid 1px;
    }
</style>

<div class="container" >
<div align="center"><h4 ><u>TAX INVOICE</u></h4>
<p style="line-height:1px;"></p>
<h2 style="line-height:4px;">Jitu and Sons</h2>
<p style="line-height:4px;">Ph:984548484</p></div>
<div class="row">
    <div class="col-md-12" style="">
<table border="1"  width="100%" id="table">
    <thead id="table">
        <tr id="table">
            <td ><strong>Billing Detail</strong></td>
            <td ><strong>Shipping Detail</strong></td>
            <td ><strong>Invoice Detail</strong></td>
            
        </tr>
    </thead>
    <tbody>
        <tr>             
    <td>
        <div align="left" id="div2"><p><span>Client Name</span> &nbsp;&nbsp;&nbsp;&nbsp; : <?php echo e($client->first_name); ?>&nbsp;&nbsp;<?php echo e($client->last_name); ?></p>
<p><span id="text" style="">Address</span><p style="padding-left: 150px;"><?php echo e($client->address_line_1); ?> <br><br><br><?php echo e($client->city); ?>, <br><br><br> <?php echo e($client->state); ?> - <?php echo e($client->pincode); ?></p></p>
            <p><span id="text">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : </p>
         <p><span id="text">Mobile</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:<?php echo e($client->mobile); ?>  </p>

        </div>
    </td>

    <td><div align="left" id="div2"><p><span id="text">Consignee</span> &nbsp;&nbsp;&nbsp;&nbsp; :Rashmi</p>
    <p><span id="text">Consignee</span> &nbsp;&nbsp;&nbsp;&nbsp; :</p>
    <p><span id="text">Consignee</span> &nbsp;&nbsp;&nbsp;&nbsp; :</p>
    <p><span id="text">Consignee</span> &nbsp;&nbsp;&nbsp;&nbsp; :</p></div></td>
     
       <td><div align="left" id="div2"><p><span id="text">Chailian Number</span> &nbsp;&nbsp;&nbsp;&nbsp;:</p>
         <p><span id="text">Chailian Number</span> &nbsp;&nbsp;&nbsp;&nbsp; :</p>
         <p><span id="text">Chailian Number</span> &nbsp;&nbsp;&nbsp;&nbsp; :</p>
        <p><span id="text">Chailian Number</span> &nbsp;&nbsp;&nbsp;&nbsp; :</p></div></td>
    </tr>   
           </tbody>
        </table>
        <!------table 2--->
        <table width="100%" id="table"> 
    <thead>
        <tr id="table">
            <th id="table"><span>Sr No.</span></th>
            <th id="table"><span>Item Description</span></th>
            <th id="table"><span>Date From</span></th>
            <th id="table"><span>Date To</span></th>
            <th id="table"><span>Price</span></th>
            <th id="table"><span>Total</span></th>

            
        </tr>
    </thead>
    <tbody >

            <tr id="table" >
            <td id="table"><p></p></td>
            <td id="table"><p></p></td>
            <td id="table"><p></p></td>
            <td id="table"><p></p></td>
            <td id="table"><p></p></td>
            <td id="table"><p></p></td>
        </tr>
        <tr  >
            <td id="tbl2" ><p></p></th>
            <td id="tbl2"><p></p></td>
            <td id="tbl2"><p></p></td>
            <td id="tbl2"><p></p></td>
            <td id="tbl2"><p></p></td>
            <td id="tbl2"><p></p> </td>


        </tr>
        <tbody>
        <tfoot>
            <tr>
            
            <th  id="table"><span></span></th>
            <th id="table"><span></span></th>
            <th id="table"><span></span></th>
             <th id="table"><span></span></th>
             
            <th id="table"><span></span></th>
            <th id="table"><span></span></th>

            
        </tr>
        </tfoot>
</table>
<div class="row">
    <div class="col-md-3" style="float: left;"> 
        <p>Note &nbsp;&nbsp;&nbsp;: </p>
    </div>
    <div class="col-md-9">
                        <div class="table-bordered no-border" style="float: right;">
                                <table class="table" >
                                    <tbody>
                                        <tr>
                                            <th colspan="9" style="border:none;">&nbsp;</th>
                                            <th style="width: 50%; text-align: left;"><p id="tbl3">Total Amount:</p></th>
                                            <td style="width: 30%;" class="text-right"><p id="tbl3"></p></td>
                                        </tr>
                                        <tr>
                                            <th colspan="9" style="border:none;"></th>
                                            <th style="text-align: left;" id="tbl3">Freight:</th>
                                            <td class="text-right" id="tbl3"></td>
                                        </tr>
                                        <tr>
                                            <th colspan="9" style="border:none;"></th>
                                            <th style="text-align: left;" id="tbl3"> Discount:</th>
                                            <td class="text-right" id="tbl3"></td>
                                        </tr>
                                        <tr>
                                            <th colspan="9" style="border:none;"></th>
                                            <th style="text-align: left;" id="tbl3"> Net Amount:</th>
                                            <td class="text-right" id="tbl3"></td>
                                        </tr>

                                    
                                        <tr>
                                            <th colspan="9" style="border:none;" ></th>
                                            <th style="text-align: left;" id="tbl3">Payable Amount (Round Off):</th>
                                            <td class="text-right" id="tbl3"></td>
                                        </tr>
                                        <tr>
                                            <th colspan="9" style="border:none;"></th>
                                            <th style="text-align: left;" id="tbl3">Paid Amount:</th>
                                            <td class="text-right" id="tbl3"></td>
                                        </tr>
                                        <tr>
                                            <th colspan="9" style="border:none;"></th>
                                            <th style="text-align: left;" id="tbl3">Due Amount:</th>
                                            <td class="text-right" id="tbl3"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
</div>



</div>
                               <br> <br> <br> <br> <br> <br> <br><div class="row">
  <div class="row invoice-payment">
                    <div class="col-xs-8">
                        <p>Other information</p>
                        <p class="text-muted"></p>
                    </div>
                    <div class="col-xs-4">
                        <br>
                        <table style="border: #000 solid 1px; width: 40%;float: right" cellpadding="10">
                            <thead>
                                <tr>
                                    <th><p>For shop</p></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr><td>&nbsp;</td></tr>
                                <tr><td>&nbsp;</td></tr>
                                <tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Authorised Signatory</td></tr>
                            </tbody>
                        </table>
                    </div>
</div>
</div>
</div><?php /**PATH /home/ptindiao/public_html/demo/finance/resources/views/invoice/print_invoice.blade.php ENDPATH**/ ?>