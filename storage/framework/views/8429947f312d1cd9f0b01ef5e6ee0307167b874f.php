        <footer class="footer">
          <div class="d-sm-flex justify-content-center justify-content-sm-between">
            <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2018 <a href="https://www.urbanui.com/" target="_blank">Urbanui</a>. All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="mdi mdi-heart text-danger"></i></span>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

  <!-- plugins:js -->
  <script src="<?php echo e(url('/public/b')); ?>/vendors/js/vendor.bundle.base.js"></script>
  <script src="<?php echo e(url('/public/b')); ?>/vendors/js/vendor.bundle.addons.js"></script>
  <!-- endinject -->
  <!-- Plugin js for this page-->
  <!-- End plugin js for this page-->
  <!-- inject:js -->
  <script src=".<?php echo e(url('/public/b')); ?>/js/off-canvas.js"></script>
  <script src="<?php echo e(url('/public/b')); ?>/js/hoverable-collapse.js"></script>
  <script src="<?php echo e(url('/public/b')); ?>/js/template.js"></script>
  <script src="<?php echo e(url('/public/b')); ?>/js/settings.js"></script>
  <script src="<?php echo e(url('/public/b')); ?>/js/todolist.js"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="<?php echo e(url('/public/b')); ?>/js/dashboard.js"></script>
    <script src="<?php echo e(url('/public/b')); ?>/js/data-table.js"></script>
  <!-- End custom js for this page-->
</body>


<!-- Mirrored from www.urbanui.com/fily/template/demo/vertical-icon-menu/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 16 Jan 2019 20:51:54 GMT -->
</html><?php /**PATH D:\wamp\www\laravel\resources\views/footer.blade.php ENDPATH**/ ?>