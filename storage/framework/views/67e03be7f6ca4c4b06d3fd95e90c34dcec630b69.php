<script src="https://cdn.tiny.cloud/1/yr547cemhej50w7li3o6a7fo82vzep03uhgoax5quqj32mlo/tinymce/5/tinymce.min.js"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script>tinymce.init({selector:'textarea'});</script>
<?php echo $__env->make('header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="page-content-wrapper">
                <div class="page-content" style="min-height:1271px">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Ticket </div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="<?php echo e(url('home')); ?>">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li><a class="parent-item" href="<?php echo e(url('ticket')); ?>"> Ticket</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>                                       
                                <li class="active">Add Ticket</li>
                            </ol>
                        </div>
                    </div>
<div class="main-panel">
    <div class="content-wrapper">
       
            <div class="row">

                <div class="col-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Add Ticket</h4>
                          <form  id="customerform" method="POST" action="<?php echo e(url('ticket/store')); ?>">
                                            <?php echo csrf_field(); ?>
                                            <?php if(Session::has('message')): ?>
                                                    <div class='alert alert-success'>
                                                    <?php echo e(Session::get('message')); ?>

                                                    <?php
                                                    Session::forget('message');
                                                    ?>
                                                    </div>
                                            <?php endif; ?>
                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <label >Role:<span class="required-mark">*</span></label>
                                      <select name="user_role_id" id="user_role_id" class="form-control" >
                                        <option disabled selected>Select Role</option>
                                        <?php if(isset($roledropdown)): ?>
                                      <?php $__currentLoopData = $roledropdown; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $dropdownGroup): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                      <option value ="<?php echo e($key); ?>"><?php echo e($dropdownGroup); ?> </option>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                       <?php endif; ?>
                                    </select>
                                       <span class="text-danger"><?php echo e($errors->first('role_id')); ?></span>  
                                    </div>
                                </div>   
                              <div class="form-group row" id="client" style="display: none;">
                                <div class="col-lg-9 col-md-8" >
                                    <label >User Name:<span class="required-mark">*</span></label>
                                      <select name="user_id[]" id="user_id" class="form-control select2-multiple" multiple style="width:600px;">
                                        <option value='0'>Select User</option>
                                    </select>
                                </div>
                                </div>         
                                <div class="form-group row">
                                  <div class="col-md-6">
                                        <label>Remark:</label>
                                        <textarea class="form-control"  name="remark"></textarea>
                                       <span class="text-danger"><?php echo e($errors->first('remark')); ?></span>   
                                        
                                    </div>
                                </div>


                                <div class="form-group row">
                                 <!-- <div class="col">
                                        <label>Username:</label>
                                        <input type="text" class="form-control"  placeholder="Enter Username" name="username" >
                                       <span class="text-danger"><?php echo e($errors->first('username')); ?></span>   
                                        
                                    </div>-->
                                    
                              </div>
                              <div class="form-group row">
     
                                </div>
                               <button type="submit" class="btn btn-primary mr-2">Submit</button>
                                
                            </form>
                        </div>
                    </div>
                </div>

            </div>
    </div>
</div>
</div>
</div>

<script type="text/javascript">
        $('#user_role_id').on('change',function(){
          var user_role_id = $('#user_role_id').val();  
                $.ajax({
                type : 'get',
                url : "<?php echo e(URL::to('user/get_user')); ?>",
                data:{'role_id':user_role_id},
                success:function(data){
                  if(data ==0){
                    alert('No Record Found!');
                    $('#client').hide();
                  }
                else{
                  $('#client').show();
                  $('#user_id').html(data);  
                }  

            }
        });

          
})


</script>

<?php echo $__env->make('footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/ct/public_html/finance/resources/views/ticket/create.blade.php ENDPATH**/ ?>