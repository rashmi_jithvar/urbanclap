<?php echo $__env->make('header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">View Client</div>
                            </div>

                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="<?php echo e(url('home')); ?>">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                            <li><i class="fa fa-list"></i>&nbsp;<a class="parent-item" href="<?php echo e(url('import_list')); ?>"> List</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active">View </li>
                            </ol>
                        </div>
                    </div>
                     <div class="row">
                      <div class="col-sm-12">
                             <div class="card-box">
                                 <div class="card-head">
                                     <header>View Client</header>
                                 </div>
                                 <div class="card-body ">
                                 <div class="table-scrollable">
                                  <table id="mainTable" class="table table-striped">
                                  <thead>

                                  </thead>
                                  <tbody>
                                      <tr>
                                          <th>Docket No.</th>
                                          <td><?php echo e($list->docket); ?></td>
                                      </tr>
                                      <tr>
                                          <th>Booking Date</th>
                                          <td><?php echo e(date("d-m-Y",strtotime($list->booking_date))); ?></td>
                                      </tr>
                                      <tr>
                                          <th>EDD</th>
                                          <td><?php echo e(date("d-m-Y",strtotime($list->edd))); ?></td>
                                      </tr>
                                      <tr>
                                          <th>Delivery Date</th>
                                          <td><?php echo e(date("d-m-Y",strtotime($list->delivery_date))); ?></td>
                                      </tr>                                      
                                      <tr>
                                          <th>Origin</th>
                                          <td><?php echo e($list->origin); ?></td>
                                      </tr>
                                      <tr>
                                          <th>Destination</th>
                                          <td><?php echo e($list->destination); ?></td>
                                      </tr>                                      
                                      <tr>
                                          <th>Zone</th>
                                          <td><?php echo e($list->zone); ?></td>
                                      </tr>
                                      <tr>
                                          <th>No. Of Boxex</th>
                                          <td><?php echo e($list->no_of_boxes); ?></td>
                                      </tr>                                     
                                       <tr>
                                          <th>Dimensions</th>
                                          <td><?php echo e($list->dimensions); ?></td>
                                      </tr>
                                      <tr>
                                          <th>Gross Value</th>
                                          <td><?php echo e($list->gross_weight); ?></td>
                                      </tr>                                     
                                       <tr>
                                          <th>Vol. Weight</th>
                                          <td><?php echo e($list->vol_weight); ?></td>
                                      </tr>
                                      <tr>
                                          <th>Transporter</th>
                                          <td><?php echo e($list->transporter); ?></td>
                                      </tr>  
                                      <tr>
                                          <th>  Status</th>
                                          <td><?php echo e($list->status); ?></td>
                                      </tr>                                        
                                      <tr>
                                          <th>Remark</th>
                                          <td><?php echo e($list->remark); ?></td>
                                      </tr>                                        
                                      <tr>
                                          <th>  Created By</th>
                                          <td><?php echo e($list->created_b); ?></td>
                                      </tr>                                       
                                       <tr>
                                          <th>Created On</th>
                                          <td><?php echo e(date("d-m-Y",strtotime($list->created_on))); ?></td>
                                      </tr>                                        
                                      <tr>
                                          <th>Updated By</th>
                                          <td><?php echo e($list->updated_by); ?></td>
                                      </tr> 
                                        <tr>
                                        <th>Updated No.</th>
                                          <td><?php echo e(date("d-m-Y",strtotime($list->updated_on))); ?></td>
                                      </tr>
                                                                                                                 
                                  </tbody>
                                  <tfoot>
                                  </tfoot>
                              </table>
                              </div>
                                 </div>
                             </div>
                         </div>
                    </div>
                </div>
            </div>
<?php echo $__env->make('footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                <?php /**PATH D:\wamp\www\urbanclap\resources\views/import/show.blade.php ENDPATH**/ ?>