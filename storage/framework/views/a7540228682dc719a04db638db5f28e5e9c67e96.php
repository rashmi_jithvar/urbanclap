<?php echo $__env->make('header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Hospitals
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo e(url('home')); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Hospitals</li>
      </ol>
    </section>
	<div class="col-md-12">
	
	<section class="content">
      <div class="row">		
		<div class="box">
            <div class="box-header">
              <h3 class="box-title">All Hospitals</h3>
              <a data-toggle="modal" class="btn btn-success pull-right" href="#Add">Add Hospital</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>ID#</th>
                  <th>Hospital</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                    <?php $__currentLoopData = $hospital; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $h): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr> 
                        <td><?php echo e($h->id); ?></td>
                        <td><?php echo e($h->hospital_name); ?></td>
                        <td><?php echo e($h->status); ?></td>
                        <td>
                          <a href="#" class="btn-delete" data-id="<?php echo e($h->id); ?>">Delete</a>
                          <a data-toggle="modal" class="btn-update" href="#Update" data-id="<?php echo e($h->id); ?>" data-name="<?php echo e($h->hospital_name); ?>" data-status="<?php echo e($h->status); ?>">Edit</a>
                        </td>
                    </tr> 
				    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
</section>
	</div>
</div>


<div id="Add" class="modal fade">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
            <form role="form" action="<?php echo e(url('addhospital')); ?>" method="post">
                <input type="hidden" name="_token" id="csrf-token" value="<?php echo e(Session::token()); ?>" />
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Add Hospital</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Hospital Name</label>
                        <input type="text" required="" name="hospital_name" class="form-control" placeholder="Enter hospital name">
                    </div>
                    
                    <div class="form-group">
                        <label for="exampleInputEmail1">Status</label>
                        <select required="" name="status" class="form-control">
                            <option value="Active">Active</option>
                            <option value="Inactive">Inactive</option>
                        </select>
                    </div>
                    
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Add Now</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </form>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div id="Update" class="modal fade">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
            <form role="form" action="<?php echo e(url('updatehospital')); ?>" method="post">
                <input type="hidden" name="_token" id="csrf-token" value="<?php echo e(Session::token()); ?>" />
                <input type="hidden" name="id" value="" required />
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Update Hospital</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Hospital Name</label>
                        <input type="text" required="" name="hospital_name" class="form-control" placeholder="Enter hospital name">
                    </div>
                    
                    <div class="form-group">
                        <label for="exampleInputEmail1">Status</label>
                        <select required="" name="status" class="form-control">
                            <option value="Active">Active</option>
                            <option value="Inactive">Inactive</option>
                        </select>
                    </div>
                    
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Update Now</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </form>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<?php echo $__env->make('footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<script>
$('.btn-delete').click(function(){
    var id = $(this).attr('data-id');
    if(confirm("Are you sure want to delete ?")){
       window.location.href="<?php echo e(url('hospital/delete')); ?>/"+id;
    }
    else{
        return false;
    }
});
                       
$('.btn-update').click(function(){
    var id = $(this).attr('data-id');
    var hn = $(this).attr('data-name');
    var st = $(this).attr('data-status');
    $('#Update').find('input[name="id"]').val(id);
    $('#Update').find('input[name="hospital_name"]').val(hn);
    $('#Update').find('select[name="status"]').val(st);
});
</script><?php /**PATH D:\wamp\www\mcgill-laravel\resources\views/member/hospitals.blade.php ENDPATH**/ ?>