        <div class="page-footer">
            <div class="page-footer-inner"> 2018 &copy; Smile Admin Theme By
            <a href="mailto:redstartheme@gmail.com" target="_top" class="makerCss">RedStar Theme</a>
            </div>
            <div class="scroll-to-top">
                <i class="icon-arrow-up"></i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="<?php echo e(url('/public/admin')); ?>/assets/plugins/jquery/jquery.min.js" ></script>
    <script src="<?php echo e(url('/public/admin')); ?>/assets/plugins/popper/popper.min.js" ></script>
    <script src="<?php echo e(url('/public/admin')); ?>/assets/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
  <script src="<?php echo e(url('/public/admin')); ?>/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <!-- bootstrap -->
    <script src="<?php echo e(url('/public/admin')); ?>/assets/plugins/bootstrap/js/bootstrap.min.js" ></script>
    <script src="<?php echo e(url('/public/admin')); ?>/assets/plugins/sparkline/jquery.sparkline.min.js" ></script>
  <script src="<?php echo e(url('/public/admin')); ?>/assets/js/pages/sparkline/sparkline-data.js" ></script>
    <!-- Common js-->
  <script src="<?php echo e(url('/public/admin')); ?>/assets/js/app.js" ></script>
    <script src="<?php echo e(url('/public/admin')); ?>/assets/js/layout.js" ></script>
    <script src="<?php echo e(url('/public/admin')); ?>/assets/js/theme-color.js" ></script>
    <!-- material -->
    <script src="<?php echo e(url('/public/admin')); ?>/assets/plugins/material/material.min.js"></script>
    <!-- animation -->
    <script src="<?php echo e(url('/public/admin')); ?>/assets/js/pages/ui/animations.js" ></script>
    <!-- chart js -->
    <script src="<?php echo e(url('/public/admin')); ?>/assets/plugins/chart-js/Chart.bundle.js" ></script>
    <script src="<?php echo e(url('/public/admin')); ?>/assets/plugins/chart-js/utils.js" ></script>
    <script src="<?php echo e(url('/public/admin')); ?>/assets/js/pages/chart/chartjs/home-data.js" ></script>
    <!-- summernote -->
    <script src="<?php echo e(url('/public/admin')); ?>/assets/plugins/summernote/summernote.min.js" ></script>
    <script src="<?php echo e(url('/public/admin')); ?>/assets/js/pages/summernote/summernote-data.js" ></script>
    <!-- end js include path -->
  </body>

<!-- Mirrored from radixtouch.in/templates/admin/smile/source/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 17 Mar 2018 19:59:26 GMT -->
</html>
       <?php /**PATH D:\wamp\www\laravel_new\resources\views/footer.blade.php ENDPATH**/ ?>