<?php echo $__env->make('header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

  <!-- Content Wrapper. Contains page content -->
        <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
            <div class="col-md-12 grid-margin">
                <div class="card bg-white">
                  <div class="card-body d-flex align-items-center justify-content-between">
                    <h4 class="mt-1 mb-1">Hi, Welcomeback!</h4>
                    <button class="btn btn-info d-none d-md-block">Import</button>
                  </div>
                </div>
            </div>
          </div>
          <div class="row">
            <div class="col-xl-9 grid-margin stretch-card">
              <div class="card">
                <div class="row">
                  <div class="col-md-7 col-lg-6 col-xl-7">
                    <div class="card-body h-100 d-flex flex-column">
                      <p class="card-title">Sale report</p>
                      <p class="text-muted mb-4">Lorem ipsum dolor sit amet consectetur adipisicing elit. Quia id beatae sint dolorum quod ducimus quisquam ut minima atque quaerat.</p>
                      <canvas id="sale-report-chart"></canvas>
                    </div>
                  </div>
                  <div class="col-md-5 col-lg-6 col-xl-5">
                    <div class="card-body">
                      <p class="card-title">Sales report overview</p>
                      <p class="pb-2 text-muted">Sale information on advertising, exhibitions, market research, online media, customer desires, PR and much more</p>
                      <div class="d-flex flex-wrap justify-content-start mt-3 mr-4">
                        <div class="mr-3">
                          <p class="mb-0">Downloads</p>
                          <h4>13,956</h4>
                        </div>
                        <div class="mr-3">
                          <p class="mb-0">Purchases</p>
                          <h4>55,123</h4>
                        </div>
                        <div class="mr-3">
                          <p class="mb-0">Users</p>
                          <h4>29829</h4>
                        </div>
                      </div>
                      <div class="d-flex mb-3">
                        <i class="mdi mdi-arrow-expand-up mb-0 text-success mr-2 mt-1"></i>
                        <p class="mb-0 text-dark">15% more than last week</p>
                      </div>
                      <div class="d-flex flex-wrap mb-5">
                        <button class="btn btn-info mt-3 mr-2">Update</button>
                        <button class="btn btn-outline-light mt-3 text-dark">More</button>
                      </div>                  
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-xl-3 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body pb-0">
                    <p class="card-title mb-xl-0">Distribution</p>
                  </div>
                  <canvas id="distribution-chart"></canvas>
                  <div class="card-body">
                    <div id="distribution-legend" class="distribution-chart-legend pt-4 pb-3"></div>
                    <button class="btn btn-outline-light text-dark d-block mx-auto mt-2">View More</button>
                  </div>
                </div>
              </div>
          </div>
  <!-- /.content-wrapper -->

<!-- ./wrapper -->
<?php echo $__env->make('footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\wamp\www\laravel\resources\views/home1.blade.php ENDPATH**/ ?>