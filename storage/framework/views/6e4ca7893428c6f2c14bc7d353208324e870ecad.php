<?php echo $__env->make('header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php
if($invoice->id)
{
       $order_id = $invoice->id;
}
else{
      $order_id = Session::get('order_id');
} 

?>

<div class="page-content-wrapper">
                <div class="page-content" style="min-height:1271px">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Invoice</div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="<?php echo e(url('home')); ?>">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li><a class="parent-item" href="<?php echo e(url('invoice')); ?>"> Invoice</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active">Update Invoice</li>
                            </ol>
                        </div>
                    </div>
<div class="main-panel">
    <div class="content-wrapper">
       
            <div class="row">

                <div class="col-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <?php if(session()->has('message')): ?>
                                <div class="alert alert-success">
                                    <?php echo e(session()->get('message')); ?>

                                </div>
                            <?php endif; ?>
                            <h4 class="card-title">Add Invoice</h4>
                          <form  method="POST" action="<?php echo e(action('InvoiceController@update', $id)); ?>" enctype="multipart/form-data">
                                            <?php echo csrf_field(); ?>
                                            <?php if(Session::has('message')): ?>
                                                    <div class='alert alert-success'>
                                                    <?php echo e(Session::get('message')); ?>

                                                    <?php
                                                    Session::forget('message');
                                                    ?>
                                                    </div>
                                            <?php endif; ?>
                                <div class="form-group row">
                

                                <div class="col">
                                        <label>Invoice Date:</label>
                                        <input   type="text" id="invoice_date"  name="invoice_date" value="<?php echo e(date('d-m-Y',strtotime($invoice->invoice_date))); ?>"  class="form-control form_date" data-date-format="dd-mm-yyyy">
                    <input   type="hidden" id="invoice_id"  name="invoice_id" value="<?php echo e($invoice->invoice_id); ?>"  class="form-control">
                                          
                                    
                                        <span class="text-danger"><?php echo e($errors->first('invoice_date')); ?></span>
                                        
                                </div>  
                                <div class="col">
                           
                                                <label>Finance Invoice Date:</label>
                                                 <input   type="text" id="finance_invoice_date" name="finance_invoice_date" placeholder="Finance Invoice date" value="<?php echo e(date('d-m-Y',strtotime($invoice->finance_invoice_date))); ?>"  class="form-control form_date" data-date-format="dd-mm-yyyy">
                                            
                                                <span class="text-danger"><?php echo e($errors->first('finance_invoice_date')); ?></span>

                        
                                </div>
                                <div class="col">
                                        <label>Client:</label>
                                    <select name="client_id" id="client_id" class="form-control" >
                                        <option disabled selected>Select Client</option>
                                        <?php if(isset($clientdropdown)): ?>
                                      <?php $__currentLoopData = $clientdropdown; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $dropdownGroup): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value ="<?php echo e($key); ?>" <?php echo e($invoice->client_id == $key ? 'selected':''); ?>><?php echo e($dropdownGroup); ?> </option>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                       <?php endif; ?>
                                    </select>
                                        <span class="text-danger"><?php echo e($errors->first('client_id')); ?></span> 
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col">
                                        <label>Warehouse Name:</label>
                                    <select name="warehouse_name" id="warehouse_name" class="form-control select2" >
                                        <option disabled selected>Select Warehouse</option>
                                        <?php if(isset($warehousedropdown)): ?>
                                      <?php $__currentLoopData = $warehousedropdown; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $dropdownGroup): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value ="<?php echo e($key); ?>" <?php echo e($invoice->warehouse_name == $key ? 'selected':''); ?>><?php echo e($dropdownGroup); ?> </option>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                       <?php endif; ?>
                                    </select>
                                        <span class="text-danger"><?php echo e($errors->first('warehouse_name')); ?></span>
                                        
                                </div>
                                <div class="col">
                                        <label>Cost Center:</label>
                                        <input type="text" class="form-control" id="cost_center" value="<?php echo e($invoice->cost_center); ?>"  name="cost_center" >
                                        <span class="text-danger"><?php echo e($errors->first('cost_center')); ?></span>
                                        
                                </div>                                 
                        <div class="col-md-4">
                                 <label>Company Wings:</label>
                                <select name="company_id" id="company_id" class="form-control">
                                                     <option disabled selected>Select Company Wings</option>
                                                    <?php if(isset($companydropdown)): ?>
                                                  <?php $__currentLoopData = $companydropdown; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $dropdownGroup): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                  <option value ="<?php echo e($key); ?>" <?php echo e($invoice->company_id == $key ? 'selected':''); ?>><?php echo e($dropdownGroup); ?> </option>
                                                   <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                   <?php endif; ?>
                                  </select>
                                
                                <span class="text-danger"><?php echo e($errors->first('due_date')); ?></span>

                            </div>
                            </div>

        <div class="col-12 col-sm-12 col-lg-12 col-xl-12 col-md-12" id="Invoice_Modal">
           <h3>Add Product</h3>
            <table class="table table-borderds" id="form1">
                <thead >
                    <tr>
                        <th >Item Name <?php echo e(number_format($order_id)); ?></th>
                        <th >Value</th>
                        <th >Date From</th>
                        <th >Date To</th>

                       
                    </tr>
                    <tr>
                        <td>
                        <select name="head" id="head" class="form-control select2" >
                                <option disabled selected>Select Revenue Type</option>
                                        <?php if(isset($revenuedropdown)): ?>
                                      <?php $__currentLoopData = $revenuedropdown; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $dropdownGroup): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value ="<?php echo e($key); ?>"><?php echo e($dropdownGroup); ?> </option>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                       <?php endif; ?>
                            </select>
                            <br>
                            <input type="text" class="form-control" id="sub_head" placeholder="Enter Sub head Name" name="sub_head" >
                                        <span class="text-danger"><?php echo e($errors->first('sub_head')); ?></span> 
                                        <ul id="search_result" style="position: absolute; z-index: 9999"></ul>
                        </td>
                        <td>
                            <br><br>
                            <label> Value</label>
                            <input type="text" class="form-control" id="value" placeholder="Enter Value" name="value" >
                                <span class="text-danger"><?php echo e($errors->first('value')); ?></span> 
                        </td>
                        <td>
                            <br><br>
                           <lable>From Date
                            <input   type="text" id="from_date" name="from_date" id="from_date" placeholder="From Date"  class="form-control form_date" data-date-format="dd-mm-yyyy"></lable>
                             
                       <!-- <div class="input-group date form_datetime" data-date="1979-09-16T05:25:07Z" data-date-format="dd MM yyyy" data-link-field="">-->
                               
                                       <!-- <span class="input-group-addon"><span class="fa fa-remove"></span></span>
                                        <span class="input-group-addon"><span class="fa fa-calendar"></span></span>-->
                            <span class="text-danger"><?php echo e($errors->first('from_date')); ?></span> 
                        
                        </td>
                        <td ><br><br>
                        <!--<div class="input-group date form_datetime" data-date="1979-09-16T05:25:07Z" data-date-format="dd MM yyyy" data-link-field="" >-->
                       <label>To Date
                        <input   type="text" id="to_date" value="" name="to_date" placeholder="To Date"  class="form-control form_date" data-date-format="dd-mm-yyyy"></label>

                                        <!--<span class="input-group-addon"><span class="fa fa-remove"></span></span>
                                        <span class="input-group-addon"><span class="fa fa-calendar"></span></span>-->
                        
                            <span class="text-danger"><?php echo e($errors->first('to_date')); ?></span> 
                        </td>
                        <td>
                            <div class="form-group">
                                <button type="button" class="btn btn-success icon icon-plus2" onclick="UpdateCart();" id="add_new_item1"><i class="icon-plus"></i></button>
                            </div>
                        </td>
                    </tr>
                </thead>
            </table>

            </hr>

            <div id="cart-table">
                <table class="table table-borderd" id="cart-table-body">
                    <thead>
                        <tr>
                            <th><strong>#</strong></th>
                            <th><strong>Item Name</strong></th>
                            <th><strong>Value</strong></th>
                            <th ><strong>Date From</strong></th>
                            <th ><strong>Date To</strong></th>
                            <th><strong>Sub Total</strong></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody id="cart-body">
                    </tbody>
                    <tfoot>
                       
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th class="text-right"></th>
                            <th class="text-right"></th>
                            <th class="text-right"></th> 
                            <th class="text-right"></th>
                           
                        </tr>
                    </tfoot>
                </table>

            </div>

 

        </div>
                                 
                            <div class="form-group row">
                                    <div class="col-md-8">
                                    </div>
                                    <div class="col-md-1">
                                        <label>File:</label>
                                           <?php if($invoice->file !=''): ?>  
                                        <a href="<?php echo e(asset('public/invoice_file/' . $invoice->file)); ?>" target="_blank"><i class="fa fa-file" aria-hidden="true"></i></a>
                                        <?php endif; ?>
                                    </div>
                                    <div class="col-md-3">
                                    
                                        <label >File</label>
                                            <input class="form-control" size="20" type="file" name="file" id="file" >
                                             <input class="form-control" size="20" type="hidden" name="old_file"  value="<?php echo e($invoice->file); ?>" >
                                        <span class="text-danger"><?php echo e($errors->first('file')); ?></span> 

                                    </div>
                            </div>

                               <button type="submit" name="save" value="save" class="btn btn-primary mr-2">Save</button>
                               <!--<button type="submit" name="save" value="save&send" class="btn btn-primary mr-2">Save & Send mail</button>-->
                                
                            </form>
                        </div>
                    </div>
                </div>

            </div>
    </div>
</div>
</div>
<script type="text/javascript">
        function UpdateCart()
    {
        
         var head = $('#head').val();
        var sub_head = $("#sub_head").val();
        var invoice_id = $("#invoice_id").val();
        var value = $("#value").val();
        var from_date = $("#from_date").val();
        var to_date = $("#to_date").val();
        $.ajax({
                type : 'get',
                url : "<?php echo e(URL::to('invoice/update_add_product')); ?>",
                data:{'name':name,'value':value,'from_date':from_date,'to_date':to_date,'head':head,'sub_head':sub_head,'invoice_id':invoice_id},
                success:function(data){

                    UpdateloadCart('<?= $order_id; ?>');
                         if (data == '1') {
                               
                              //  alert("Added!", " Item Added successfully", "success");
                                 $("#sub_head").val("");
                                $("#value").val("");
                                $("#from_date").val("");
                                $("#to_date").val("");
                                $("#search_result").hide();
                                $('#head').trigger("");
                        } else {


                                alert("Oopss!", "Stock not Avaialble", "error");
                    }
                }
        });
 //       window.location = "<?php echo e(url('invoice/add_product')); ?>?name="+name+"&value="+value+"&from_date="+from_date
//+"&to_date="+to_date;

    }

UpdateloadCart(<?php echo e($order_id); ?>);
function UpdateloadCart($order_id)
{

    $.ajax({
        type : 'get',
        url : "<?php echo e(URL::to('invoice/update_load_cart')); ?>",
        data:{'order_id':$order_id},
        success:function(data){  
        $('#cart-table-body').html(data);

        }
        });
  

     // var total = $("#cart-table-body").find("#total").text();
     // alert(total);
    //  $("#invoice-total_amount").val(parseFloat($.trim(total)));
  }


</script>
<script type="text/javascript">
    function addtext(text) {
     // alert(text);
        $('#sub_head').val(text);
        $('#search_result').hide();
    }
$('#sub_head').on('keyup',function(){
        $head=$('#head').val();
        $sub_head=$('#sub_head').val();
    
        $.ajax({
        type : 'get',
        url : "<?php echo e(URL::to('invoice/search')); ?>",
        data:{'head':$head,'sub_head':$sub_head},
        success:function(data){
        $('#search_result').show();
        $("#search_result").trigger("chosen:updated");
        $('#search_result').html(data);
        


    }
});
})

$('#client_id').on('change',function(){
        $client_id=$('#client_id').val();
        //alert($invoice_date);
        $.ajax({
        type : 'get',
        url : "<?php echo e(URL::to('client/get_warehouse')); ?>",
        data:{'client_id':$client_id},
        success:function(data){
            //alert(data);
        $('#warehouse_name').html(data);
      
    }
});
})

</script>
<script type="text/javascript">
       function removeCart(url) {
                  
    var x = confirm("Are you sure you want to delete?");
      if (x)
            $.ajax({
                    type : 'get',
                    url : url,
                    
                    success:function(data){
                        UpdateloadCart('<?= $order_id; ?>');
                            if (result == 'Deleted') {
                               
                                alert("Deleted!", " Item deleted successfully", "success");
                     
                            } else {


                                alert("Oopss!", "Some error occured", "error");
                            }
                    }
            });

         
      else
        return false;
        }
</script>
<script>

   if ($("#customerform").length > 0) {

    $("#customerform").validate({

     

    rules: {

    invoice_date: {

        required: true,

      

      },

    finance_invoice_date: {

        required: true,

       

      },

    client_id: {

        required: true,

       

      },

    warehouse_name: {

        required: true,

      },

   

    cost_center: {

          required: true,

          maxlength: 150,

    }, 

    file: { extension: "image|jpg,image|jpeg,image|png,image|gif|doc|pdf|docx|zip|xlsx|xls|xlsm"}

  },

    messages: {

       

      invoice_date: {

        required: "Please Select Invoice Date",

      },



    finance_invoice_date: {

        required: "Please Select Finance Invoice Date",

    

      },

    client_id: {

        required: "Please Select Client",

      },  

    warehouse_name: {

        required: "Please Select Warehouse",

        

      },

    cost_center: {

        required: "Please Enter Cost center",

       

      }, 

      file: {required: 'Required!', accept: 'Not an file it should be PDF,DOC,DOCX,xlsx,xls or Image format!'}  

    },

    })

  }

</script>

<?php echo $__env->make('footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/ct/public_html/finance/resources/views/invoice/edit.blade.php ENDPATH**/ ?>