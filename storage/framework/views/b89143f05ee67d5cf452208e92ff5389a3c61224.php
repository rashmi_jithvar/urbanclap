<?php echo $__env->make('header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo e(url('home')); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>
	<div class="col-md-12">
	
	<section class="content">
      <div class="row">
        <!-- left column -->
         <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Change Password</h3>
              
              <?php if($errors->any()): ?>
                <div class="alert alert-danger" style="margin-top:15px;">
                    <ul>
                        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li><?php echo e($error); ?></li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </ul>
                </div>
            <?php endif; ?>
            
            <?php if(Session::has('message')): ?>
                <div class='alert alert-success' style="margin-top:15px;">
                <?php echo e(Session::get('message')); ?>

                <?php
                    Session::forget('message');
                ?>
                </div>
            <?php endif; ?>
            
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="<?php echo e(url('updatepassword')); ?>" method="post">
              <input type="hidden" name="_token" id="csrf-token" value="<?php echo e(Session::token()); ?>" />
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">New Password </label>
                  <input type="password" name="password" required="" class="form-control" placeholder="Enter Password">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Confirm Password</label>
                  <input type="password" name="password_confirmation" required="" class="form-control" placeholder="Confirm password">
                </div> 
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
		</div> 
	</section>
	</div>
</div>
<?php echo $__env->make('footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\wamp\www\mcgill-laravel\resources\views/member/changepassword.blade.php ENDPATH**/ ?>