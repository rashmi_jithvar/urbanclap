
<?php echo $__env->make('header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>  
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/additional-methods.min.js"></script>
  <style>
   .error{ color:red; } 
  </style>
<div class="page-content-wrapper">
                <div class="page-content" style="min-height:1271px">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Company Wings </div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="<?php echo e(url('home')); ?>">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                            <li><i class="fa fa-list"></i>&nbsp;<a class="parent-item" href="<?php echo e(url('company_wings')); ?>"> Company Wings</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active">Add Company Wings</li>
                            </ol>
                        </div>
                    </div>
<div class="main-panel">
    <div class="content-wrapper">
       
            <div class="row">

                <div class="col-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Add Company Wings</h4>
                          <form  id="customerform" method="POST" action="<?php echo e(url('company_wings/store')); ?>" enctype="multipart/form-data">
                                            <?php echo csrf_field(); ?>
                                            <?php if(Session::has('message')): ?>
                                                    <div class='alert alert-success'>
                                                    <?php echo e(Session::get('message')); ?>

                                                    <?php
                                                    Session::forget('message');
                                                    ?>
                                                    </div>
                                            <?php endif; ?>
                                <div class="form-group row">
                
                                    <div class="col">
                                        <label>Company Name:<span class="required-mark">*</span></label>
                                        <input type="text"  id="company_name" class="form-control"  placeholder="Enter company name" name="company_name" >
                                         <span class="text-danger"><?php echo e($errors->first('company_name')); ?></span> 
                                    </div>  
                                    <div class="col">
                                        <label>First Name:<span class="required-mark">*</span></label>
                                        <input type="text" id="first_name" class="form-control"  placeholder="Enter First Name" name="first_name" >
                                         <span class="text-danger"><?php echo e($errors->first('first_name')); ?></span>
                                        
                                    </div>
                                    <div class="col">
                                        <label>Last Name:</label>
                                        <input type="text" id="last_name" class="form-control"  placeholder="Enter Last name" name="last_name" >
                                         <span class="text-danger"><?php echo e($errors->first('last_name')); ?></span>
                                        
                                    </div>
                              </div>
                              <div class="form-group row">    
                                    <div class="col">
                                        <label>Mobile:<span class="required-mark">*</span></label>
                                        <input type="text"  id="mobile" class="form-control"  placeholder="Enter Mobile Name" name="mobile" maxlength="10" >
                                         <span class="text-danger"><?php echo e($errors->first('mobile')); ?></span>
                                        
                                    </div>
                                   
                                    <div class="col">
                                        <label>Email :<span class="required-mark">*</span></label>
                                        <input type="text" id="email" class="form-control"  placeholder="Enter Email" name="email" >
                                         <span class="text-danger"><?php echo e($errors->first('email')); ?></span>
                                        
                                    </div>
                  
                                    <div class="col">
                                        <label>Website :</label>
                                        <input type="text" id="website" class="form-control"  placeholder="Enter Website" name="website" >
                                         <span class="text-danger"><?php echo e($errors->first('website')); ?></span>
                                        
                                    </div>
                              </div> 
                                   
                              <div class="form-group row">  
                                 
                                    <div class="col">
                                      <label>Address Line 1 :<span class="required-mark">*</span></label>
                                        <input type="text" id="address_line_1" class="form-control"  placeholder="Enter Address Line 1" name="address_line_1" >
                                         <span class="text-danger"><?php echo e($errors->first('address_line_1')); ?></span>
                                        
                                    </div>
                             
                                    <div class="col">
                                      <label>Address Line 2 :</label>
                                        <input type="text" id="address_line_2" class="form-control"  placeholder="Enter Address Line 2" name="address_line_2" >
                                         <span class="text-danger"><?php echo e($errors->first('address_line_2')); ?></span>
                                        
                                    </div>

                                  <div class="col">
                                                <label for="exampleSelectGender">State<span class="required-mark">*</span>
                                                </label>
                                <input type="text"  id="state" class="form-control"  placeholder="Enter State" name="state" >
                                <span class="text-danger"><?php echo e($errors->first('state')); ?></span>  
                                </div>
                              </div>
                              <div class="form-group row">  
                                <div class="col">
                                        <label for="exampleSelectGender">City<span class="required-mark">*</span>
                                        </label>
                                          <input type="text"  id="city" class="form-control"  placeholder="Enter City" name="city" >
                                              <span class="text-danger"><?php echo e($errors->first('city')); ?></span> 
                                </div>
                                <div class="col">
                                    <label>Pincode<span class="required-mark">*</span>
                                    </label>
                                    <input type="text" class="form-control" id="pincode" placeholder="Enter Pincode" name="pincode" maxlength="6" minlength="6">
                                    <span class="text-danger"><?php echo e($errors->first('pincode')); ?>

                                    </span>      
                                  </div>

                    
                                <div class="col">
                                        <label>Logo:</label>
                                        <input type="file" class="form-control" id="image" placeholder="Enter image" name="image" >
                                      <span class="text-danger"><?php echo e($errors->first('image')); ?></span>    
                                </div>

                          </div>
                          <div class="form-group row">
                
                                    <div class="col-md-4">
                                        <label>GST Number:</label>
                                        <input type="text"  id="gst_number" class="form-control"  placeholder="Enter gst number" name="gst_number" >
                                         <span class="text-danger"><?php echo e($errors->first('gst_number')); ?></span> 
                                    </div>  

   
                              </div>
                              <div class="form-group row">
                               <button type="submit" class="btn btn-primary mr-2">Submit</button>
                             </div>
                              </div>
   
                            </form>
                        </div>
                    </div>
                </div>

            </div>
    </div>
</div>
</div>
</div>
<script>
   if ($("#customerform").length > 0) {
    $("#customerform").validate({
     
    rules: {
    first_name: {
        required: true,
        maxlength: 150
      },
    company_name: {
        required: true,
        maxlength: 255
      },
    mobile: {
        required: true,
        maxlength: 10,
        number: true,

      },
    email: {
        required: true,
        email: true,
      },
    address_line_1: {
        required: true,
      },
   
    state: {
          required: true,
          maxlength: 150,
    }, 
    city: {
        required: true,
      },
   
    pincode: {
          required: true,
          maxlength: 6,
    }, 
    credit_period_defination: {
          required: true,
          maxlength: 50,
    }, 
    password: {
          required: true,
          maxlength: 50,

    }, 
    confirm_password: {
        required: true,
       equalTo: "#password"
      
      },
    website: {  
        url: true
    },
    first_contact_spoc: {
          required: true,
          maxlength: 255,
    }, 
   
  },
    messages: {
       
      first_name: {
        required: "Please Enter First name",
        maxlength: "Your last name maxlength should be 50 characters long."
      },

    company_name: {
        required: "Please Enter Company name",
        maxlength: "Your company_name should be 255 characters long."
      },
    mobile: {
        required: "Please Enter Mobile number",
        maxlength: "Your mobile should be 10 characters long."
      },  
    email: {
        required: "Please Enter Email address",
          email: "Please enter valid email",
        
      },
    address_line_1: {
        required: "Please Enter Address",
       
      }, 
    state: {
          required: "Please Enter state",
          
        },
    city: {
          required: "Please Enter City",
          
      },
    pincode: {
          required: "Please Enter pincode",
          
      },
    credit_period_defination: {
          required: "Please Enter credit_period_defination",
          
        },
    first_contact_spoc: {
          required: "Please Enter First Contact SPOC",
          
        },
    password: {
          required: "Please Enter Password",
          
        },
    website: {
          required: "Please Enter valid url",
          
        },
    },
    })
  }
</script>
<?php echo $__env->make('footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/ptindiao/public_html/demo/finance/resources/views/company/create.blade.php ENDPATH**/ ?>