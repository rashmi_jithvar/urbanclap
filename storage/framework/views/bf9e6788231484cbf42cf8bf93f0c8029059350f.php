<?php echo $__env->make('header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>  
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/additional-methods.min.js"></script>
  <style>
   .error{ color:red; } 
  </style>
<div class="page-content-wrapper">
                <div class="page-content" style="min-height:1271px">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">User </div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.html">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                            
                                </li>
                                <li class="active">User</li>
                            </ol>
                        </div>
                    </div>
<div class="main-panel">
    <div class="content-wrapper">
       
            <div class="row">

                <div class="col-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Add User</h4>
                          <form  id="customerform" method="POST" action="<?php echo e(url('user/store')); ?>">
                                            <?php echo csrf_field(); ?>
                                            <?php if(Session::has('message')): ?>
                                                    <div class='alert alert-success'>
                                                    <?php echo e(Session::get('message')); ?>

                                                    <?php
                                                    Session::forget('message');
                                                    ?>
                                                    </div>
                                            <?php endif; ?>
                                <div class="form-group row">
                
                                    <div class="col">
                                        <label>FullName:</label>
                                        <input type="text"  id="formGroupExampleInput" class="form-control"  placeholder="Enter Fullname" name="name" >
                                         <span class="text-danger"><?php echo e($errors->first('name')); ?></span> 
                                    </div>
                                    <div class="col">
                                        <label>Email:</label>
                                        <input type="email" id="email" class="form-control"  placeholder="Enter Email" name="email" >
                                         <span class="text-danger"><?php echo e($errors->first('email')); ?></span>
                                        
                                    </div>
                                    <div class="col">
                                        <label for="exampleSelectGender">Role Id</label>
                                      <select name="role_id" id="role_id" class="form-control">
                                        <?php if(isset($roledropdown)): ?>
                                      <?php $__currentLoopData = $roledropdown; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $dropdownGroup): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                      <option value ="<?php echo e($key); ?>"><?php echo e($dropdownGroup); ?> </option>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                       <?php endif; ?>
                                    </select>
                                       <span class="text-danger"><?php echo e($errors->first('role_id')); ?></span>  
                                    </div>
                                </div>
                              
                                <div class="form-group row">
                                 <!-- <div class="col">
                                        <label>Username:</label>
                                        <input type="text" class="form-control"  placeholder="Enter Username" name="username" >
                                       <span class="text-danger"><?php echo e($errors->first('username')); ?></span>   
                                        
                                    </div>-->
                                <div class="col">
                                        <label>Password:</label>
                                        <input type="password" class="form-control" id="password" placeholder="Enter Password" name="password" >
                                      <span class="text-danger"><?php echo e($errors->first('password')); ?></span>    
                                </div>
                                <div class="col">
                                        <label>Confirm Password:</label>
                                        <input type="password" class="form-control" id="confirm_password" placeholder="Enter Confirm Password" name="confirm_password" >
                                    <span class="text-danger"><?php echo e($errors->first('confirm_password')); ?></span>      
                                </div>
                                <div class="col">
                                        <label for="exampleSelectGender">Status</label>
                                        <select class="form-control" id="status" name="status" >
                                            <option value="none">Select Status</option>
                                            <option value="Active">Active</option>
                                            <option value="Inactive">Inactive</option>
                                        </select>
                                    </div>
                                    <span class="text-danger"><?php echo e($errors->first('status')); ?></span> 
                              </div>
                              <div class="form-group row">
     
                                </div>
                               <button type="submit" class="btn btn-primary mr-2">Submit</button>
                                <button class="btn btn-light">Cancel</button>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
    </div>
</div>
</div>
</div>
<script>
   if ($("#customerform").length > 0) {
    $("#customerform").validate({
     
    rules: {
      name: {
        required: true,
        maxlength: 50
      },
       username: {
        required: true,
        maxlength: 50
      },
    password: {
        required: true,
        maxlength: 8
      },
    confirm_password: {
        required: true,
        maxlength: 8,
        equalTo : "#password"
      },
    role_id: {
        required: true,
      },
    status: {
        required: true,
      },
   
    email: {
                required: true,
                maxlength: 50,
                email: true,
            },    
    },
    messages: {
       
      name: {
        required: "Please enter name",
        maxlength: "Your last name maxlength should be 50 characters long."
      },
      username: {
        required: "Please enter username",
        maxlength: "Your last name maxlength should be 50 characters long."
      },
    password: {
        required: "Please enter password",
        maxlength: "Your password should be 8 characters long."
      },
    confirm_password: {
        required: "Please enter confirm password",
        maxlength: "Your password should be 8 characters long."
      },  
    role_id: {
        required: "Please select user role",
        
      },
    status: {
        required: "Please select status",
       
      }, 
      email: {
          required: "Please enter valid email",
          email: "Please enter valid email",
          maxlength: "The email name should less than or equal to 50 characters",
        },
        
    },
    })
  }
</script>
<?php echo $__env->make('footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\wamp\www\finance_laravel\finance\resources\views/user/create.blade.php ENDPATH**/ ?>