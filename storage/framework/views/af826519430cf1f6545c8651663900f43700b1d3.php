<?php echo $__env->make('header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="page-content-wrapper">
                <div class="page-content" style="min-height:1271px">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Follow Up</div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="<?php echo e(url('home')); ?>">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li><a class="parent-item" href="<?php echo e(url('invoice')); ?>"> Invoice</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active">Follow up Invoice</li>
                            </ol>
                        </div>
                    </div>
<div class="main-panel">
    <div class="content-wrapper">


            <div class="row">

                <div class="col-6 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">

                            <h4 class="card-title"></h4>
                          <form  method="POST" action="<?php echo e(action('InvoiceController@add_follow_up', $id)); ?>">
                                            <?php echo csrf_field(); ?>
                                            <?php if(Session::has('message')): ?>
                                                    <div class='alert alert-success'>
                                                    <?php echo e(Session::get('message')); ?>

                                                    <?php
                                                    Session::forget('message');
                                                    ?>
                                                    </div>
                                            <?php endif; ?>
                                            <label>Is New Payment:</label>
                        <div class="form-group row">
                
                                    <div class="col">
                                        
                                    <label>Yes
                                        <input type="radio" class="form-control" id="is_new_payment_yes" name="is_new_payment" value="1">  
                                    </label>
                                    <label>No
                                        <input type="radio" class="form-control" id="is_new_payment" name="is_new_payment" value="0">  
                                    </label>
                                        
                                    </div>
                                    <span class="text-danger"><?php echo e($errors->first('is_new_payment')); ?></span> 
                       </div>  
                       <div class="form-group row" id="new_payment" style="display: none;">           
                                 <div class="col">
                                        <label>Payment Date:</label>
                                        <input   type="text" id="invoice_date"  name="new_payment_date" placeholder="New Payment Date"  class="form-control form_date" data-date-format="dd-mm-yyyy">
                                  
                            
                                        <span class="text-danger"><?php echo e($errors->first('new_payment_date')); ?></span> 
                                    </div>
                        </div>                     
                                <div class="form-group row">
                
                                    <div class="col">
                                        <label>Follow Up:</label>
                                        <textarea class="form-control" id="remark" placeholder="Enter text" name="remark"></textarea>   
                                        <span class="text-danger"><?php echo e($errors->first('remark')); ?></span> 
                                    </div>
                              </div>
                                    <div class="form-group row">
                                    <div class="col">
                                        
                                        <input type="hidden" class="form-control" id="invoice_id" value="<?php echo e($invoice->invoice_id); ?>" name="invoice_id">   
                                       
                                    </div>
                                </div>
                               <button type="submit" class="btn btn-primary mr-2">Submit</button>
                                
                            </form>
                        </div>
                    </div>
                </div>

            </div>
                  <div class="row">
                      <div class="col-sm-12">
      <!-- start page content -->

                       <div class="card card-box" style="">
                              <div class="card-head">
                                  <header>Follow Ups</header>

                              </div>
                              <div class="card-body no-padding height-9">
                                <div class="row">
                                        <ul class="chat nice-chat chat-page small-slimscroll-style">
                                          <?php $__currentLoopData = $follow_up; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $follow): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <li class="in"><img src="<?php echo e(url('/public/admin')); ?>/assets/img/dp.jpg" class="avatar" alt="">
                                                <div class="message">
                                                    <span class="arrow"></span> <a class="name" href="#"><?php echo e($follow->name); ?></a> <span class="datetime">at <?php echo e($follow->created_at); ?></span> 
                                                    <span class="body"> <?php echo e($follow->remark); ?> </span>
                                                    <span class="body"><?php if($follow->new_payment_date !=''): ?> New Payment Date: &nbsp;&nbsp;&nbsp;<?php echo e($follow->new_payment_date); ?>

                                                    <?php endif; ?> </span>
                                                </div>
                                            </li>
                                             <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </ul>
                                        <div class="box-footer chat-box-submit">
                         </div>
            </div>
</div>
            <!-- end page content -->
</div>
    </div>
</div>
<script type="text/javascript">
        $('#is_new_payment_yes').on('click',function(){
          var selValue = $('input[name=is_new_payment]:checked').val();  
          if(selValue ='1'){
            $("#new_payment").show();
          }
          else{
            $("#new_payment").hide();
          }
          
})
</script>
<?php echo $__env->make('footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/ct/public_html/finance/resources/views/invoice/follow_up.blade.php ENDPATH**/ ?>