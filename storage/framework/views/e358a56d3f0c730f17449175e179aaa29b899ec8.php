<?php
$routeArray = app('request')->route()->getActionName();
        $controller = explode('@', $routeArray);


        $action = ($controller[1]);
?>
<!DOCTYPE html>

<html>

<head>

  <meta charset="utf-8">

  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <title>McGill | Dashboard</title>

  <!-- Tell the browser to be responsive to screen width -->

  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <!-- Bootstrap 3.3.7 -->

  <link rel="stylesheet" href="<?php echo e(url('/public/admin')); ?>/bower_components/bootstrap/dist/css/bootstrap.min.css">

  <!-- Font Awesome -->

  <link rel="stylesheet" href="<?php echo e(url('/public/admin')); ?>/bower_components/font-awesome/css/font-awesome.min.css">

  <!-- Ionicons -->

  <link rel="stylesheet" href="<?php echo e(url('/public/admin')); ?>/bower_components/Ionicons/css/ionicons.min.css">

  <!-- Theme style -->

  <link rel="stylesheet" href="<?php echo e(url('/public/admin')); ?>/dist/css/AdminLTE.min.css">

  <!-- AdminLTE Skins. Choose a skin from the css/skins

       folder instead of downloading all of them to reduce the load. -->

  <link rel="stylesheet" href="<?php echo e(url('/public/admin')); ?>/dist/css/skins/_all-skins.min.css">

  <!-- Morris chart -->

  <link rel="stylesheet" href="<?php echo e(url('/public/admin')); ?>/bower_components/morris.js/morris.css">

  <!-- jvectormap -->

  <link rel="stylesheet" href="<?php echo e(url('/public/admin')); ?>/bower_components/jvectormap/jquery-jvectormap.css">

  <!-- Date Picker -->

  <link rel="stylesheet" href="<?php echo e(url('/public/admin')); ?>/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

  <!-- Daterange picker -->

  <link rel="stylesheet" href="<?php echo e(url('/public/admin')); ?>/bower_components/bootstrap-daterangepicker/daterangepicker.css">

  <!-- bootstrap wysihtml5 - text editor -->

  <link rel="stylesheet" href="<?php echo e(url('/public/admin')); ?>/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">



  <!-- DataTables -->

  <link rel="stylesheet" href="<?php echo e(url('/public/admin')); ?>/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">



  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->

  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

  <!--[if lt IE 9]>

  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>

  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

  <![endif]-->



  <!-- Google Font -->

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <style type="text/css">
    .logo-mini{
      font-size: 14px !important;
    }
  </style>
</head>

<body class="hold-transition skin-blue sidebar-mini">

<div class="wrapper">



  <header class="main-header">

    <!-- Logo -->

    <a href="<?php echo e(url('home')); ?>" class="logo">

        <?php if(Auth::user()->user_type == 'A'): ?>
            <!-- mini logo for sidebar mini 50x50 pixels -->

            <span class="logo-mini">A P</span>

            <!-- logo for regular state and mobile devices -->

            <span class="logo-lg"><b>Admin Panel</b></span>
        <?php else: ?>
            <!-- mini logo for sidebar mini 50x50 pixels -->

            <span class="logo-mini">U P</span>

            <!-- logo for regular state and mobile devices -->

            <span class="logo-lg"><b>User Panel</b></span>
        <?php endif; ?>
    </a>

    <!-- Header Navbar: style can be found in header.less -->

    <nav class="navbar navbar-static-top">

      <!-- Sidebar toggle button-->

      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">

        <span class="sr-only">Toggle navigation</span>

      </a>



      <div class="navbar-custom-menu">

        <ul class="nav navbar-nav">



          <li>

		  <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">

				<?php echo csrf_field(); ?>

			</form>

			<a class="dropdown-item" href="<?php echo e(route('logout')); ?>"

			   onclick="event.preventDefault();

							 document.getElementById('logout-form').submit();">

				<?php echo e(__('Logout')); ?>


			</a>

          </li>

        </ul>

      </div>

    </nav>

  </header>

  <!-- Left side column. contains the logo and sidebar -->

  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->

    <section class="sidebar">

      <!-- Sidebar user panel -->

      <div class="user-panel">



      </div>

      <!-- sidebar menu: : style can be found in sidebar.less -->

      <ul class="sidebar-menu" data-widget="tree">

        <li class="header">MAIN NAVIGATION</li>

        <?php if(Auth::user()->user_type == 'A'): ?>

        <li class="<?= $action == 'index' && Auth::user()->user_type == 'A'? 'active' : ''; ?>">

          <a href="<?php echo e(url('home')); ?>">

            <i class="fa fa-dashboard"></i> <span>All Members</span>

          </a>

        </li>

        <li class="<?= $action == 'hospitals'? 'active' : ''; ?>">

          <a href="<?php echo e(url('hospitals')); ?>">

            <i class="fa fa-hospital-o"></i> <span>Hospitals</span>

          </a>

        </li>

        <?php else: ?>

        <li class="<?= $action == 'index' && Auth::user()->user_type != 'A'? 'active' : ''; ?>">

          <a href="<?php echo e(url('home')); ?>">

            <i class="fa fa-users"></i> <span>Add Patient</span>

          </a>

        </li>

        <?php endif; ?>


        <li class="<?= $action == 'changepassword'? 'active' : ''; ?>">
          <a href="<?php echo e(url('changepassword')); ?>">
            <i class="fa fa-user-plus"></i> <span>Change Password</span>
          </a>
        </li>

      </ul>

    </section>

    <!-- /.sidebar -->

  </aside>

<?php /**PATH D:\wamp\www\mcgill-laravel\resources\views/header.blade.php ENDPATH**/ ?>