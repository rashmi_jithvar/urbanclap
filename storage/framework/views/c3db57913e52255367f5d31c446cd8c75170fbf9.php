<?php echo $__env->make('header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">View User</div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="<?php echo e(url('/home')); ?>">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                 <li class="active">&nbsp;<a class="parent-item" href="<?php echo e(url('/user')); ?>">User List</a>&nbsp;<i class="fa fa-angle-right"></i></li> 
                                <li class="active">View User</li>
                            </ol>
                        </div>
                    </div>
                     <div class="row">
                      <div class="col-sm-12">
                             <div class="card-box">
                                 <div class="card-head">
                                     <header>View User</header>
                                 </div>
                                 <div class="card-body ">
                                 <div class="table-scrollable">
                                  <table id="mainTable" class="table table-striped">
                                  <thead>

                                  </thead>
                                  <tbody>
                                      <tr>
                                          <th>Name</th>
                                          <td><?php echo e($user->name); ?></td>
                                      </tr>
                                      <tr>
                                          <th>Email</th>
                                          <td><?php echo e($user->email); ?></td>
                                      </tr>
                                      <tr>
                                          <th>Role</th>
                                          <td><?php echo e($role_name->name); ?></td>
                                      </tr>
                                    <tr>
                                          <th>Status</th>
                                      <td>                           
                                             <?php if($user->status == 'Active'): ?>
                                                   <label class="badge badge-success">Active</label>
                                              
                                              <?php else: ?>
                                                    <label class="badge badge-danger">Pending</label>
                                               <?php endif; ?>
                                      </td>
                                      </tr>
                                      </tr></tr>                                        
                                      <tr>
                                          <th>Created At</th>
                                          <td> <?php echo e(date("d-m-Y",strtotime($user->created_at))); ?></td>
                                      </tr>                                       
                                  </tbody>
                                  <tfoot>
                                  </tfoot>
                              </table>
                              </div>
                                 </div>
                             </div>
                         </div>
                    </div>
                </div>
            </div>
<?php echo $__env->make('footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                <?php /**PATH /home/ptindiao/public_html/demo/finance/resources/views/user/view.blade.php ENDPATH**/ ?>