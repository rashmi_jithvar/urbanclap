<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
// Route::get('/', function () {
//    return view('home1');
//});

Route::get('/', function () {
    return view('auth.login');
});  
Auth::routes(['verify' => true]);
Route::view('/register', 'registration/create');
Route::get('/home', 'HomeController@index')->name('home');
//Route::post('/register', 'RegistrationController@store');
//Route::get('register', 'RegistrationController@create');
Route::get('/view', 'RegistrationController@index');
Route::post('/registrationaction', 'RegistrationController@storeUser');
//Route::match(['get', 'post'], '/test', 'Controller@action');
Route::get('/edit/user/{id}','RegistrationController@edit');
Route::post('/edit/user/{id}','RegistrationController@update');
Route::delete('/delete/user/{id}','RegistrationController@destroy');
Route::get('/roles', 'RolesController@index');
Route::get('/roles/create', 'RolesController@create');
Route::post('/roles/store', 'RolesController@store');
Route::get('/edit/roles/{id}','RolesController@edit');
Route::post('/edit/roles/{id}','RolesController@update');
Route::get('/delete/roles/{id}','RolesController@destroy');
Route::get('/permission_group', 'PermissionGroupController@index');
Route::get('/permission_group/create', 'PermissionGroupController@create');
Route::post('/permissiongroup/store', 'PermissionGroupController@store');
Route::get('/edit/permission_group/{id}','PermissionGroupController@edit');
Route::post('/edit/permission_group/{id}','PermissionGroupController@update');
Route::get('/delete/permission_group/{id}','PermissionGroupController@destroy');
//permission
Route::get('/permission', 'PermissionController@index');
Route::get('/permission/create', 'PermissionController@create');
Route::post('/permission/store', 'PermissionController@store');
Route::get('/edit/permission/{id}','PermissionController@edit');
Route::post('/edit/permission/{id}','PermissionController@update');
Route::get('/delete/permission/{id}','PermissionController@destroy');
//role Permission
Route::get('/role_permission', 'RolePermissionController@index');
Route::get('/role_permission/create', 'RolePermissionController@create');
Route::post('/role_permission/store', 'RolePermissionController@store');
Route::get('/edit/role_permission/{id}','RolePermissionController@edit');
Route::post('/edit/role_permission/{id}','RolePermissionController@update');
Route::get('/delete/role_permission/{id}','RolePermissionController@destroy');
//User
Route::get('/user', 'UserController@index');
Route::get('/user/create', 'UserController@create');
Route::post('/user/store', 'UserController@storeUser');
Route::get('/edit/user/{id}','UserController@edit');
Route::post('/edit/user/{id}','UserController@update');
Route::get('/delete/user/{id}','UserController@destroy');
Route::get('/user/view/{id}','UserController@view');
//warehouse
Route::get('/warehouse', 'WarehouseController@index');
Route::get('/warehouse/create', 'WarehouseController@create');
Route::post('/warehouse/store', 'WarehouseController@store');
Route::get('/edit/warehouse/{id}','WarehouseController@edit');
Route::get('/view/warehouse/{id}','WarehouseController@view');
Route::post('/edit/warehouse/{id}','WarehouseController@update');
Route::get('/delete/warehouse/{id}','WarehouseController@destroy');

//client
Route::get('/client', 'ClientController@index');
Route::get('/client/create', 'ClientController@create');
Route::post('/client/store', 'ClientController@store');
Route::get('/edit/client/{id}','ClientController@edit');
Route::post('/edit/client/{id}','ClientController@update');
Route::get('/view/client/{id}','ClientController@view');
Route::get('/delete/client/{id}','ClientController@destroy');
Route::get('/client/get_warehouse', 'ClientController@get_warehouse');
Route::get('/client/client_credit_period', 'ClientController@client_credit_period');


//profile
Route::get('/home/view_profile', 'HomeController@view_profile');
Route::get('/changepassword', 'HomeController@changepassword');
Route::post('/updatepassword', 'HomeController@updatepassword');
Route::get('/changedetails', 'HomeController@changedetails');
Route::post('/updatedetails', 'HomeController@updatedetails');
Route::get('home/show_notification', 'HomeController@show_notification');
Route::get('/update_notification_finance/{id}/{invoice_id}','HomeController@update_notification_finance');
Route::get('/update_notification_client/{id}/{invoice_id}','HomeController@update_notification_client');
Route::get('/sendmail','HomeController@sendmail');
Route::get('/home/genarate_ticket_approve_invoice', 'HomeController@Genarate_ticket_approve_invoice');
Route::get('/home/genarate_ticket_dues_invoice', 'HomeController@Genarate_ticket_dues_invoice');


//Import
Route::get('/import_list', 'ImportController@index');
Route::get('/import_list/create', 'ImportController@create');
Route::delete('/import_list/bulkDelete', 'ImportController@bulkDelete');
Route::post('/import_list/csvfileupload','ImportController@csvfileupload');
Route::post('/import_list/store', 'ImportController@store');
Route::get('/edit/import_list/{id}','ImportController@edit');
Route::post('/edit/import_list/{id}','ImportController@update');
Route::get('/delete/import_list/{id}','ImportController@destroy');
Route::get('/user/show/{id}','ImportController@show');

