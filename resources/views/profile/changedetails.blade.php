@include('header')
<!-- Content Wrapper. Contains page content -->
<div class="page-content-wrapper">
                <div class="page-content" style="min-height:1271px">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Change User Details</div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                               
                                </li>
                                <li class="active">Change User Details</li>
                            </ol>
                        </div>
                    </div>
  <div class="main-panel">
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->

      <div class="row">
        <!-- left column -->
        <div class="col-6 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
              <h3 class="box-title">Change User Details</h3>
              
              @if ($errors->any())
                <div class="alert alert-danger" style="margin-top:15px;">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            
            @if(Session::has('message'))
                <div class='alert alert-success' style="margin-top:15px;">
                {{ Session::get('message') }}
                @php
                    Session::forget('message');
                @endphp
                </div>
            @endif
            
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="{{url('updatedetails')}}" method="post">
              <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
              <input type="hidden" name="id"  value="{{$user->id}}" />
              <div class="box-body">
                <div class="form-group">
                  <div class="col">
                  <label for="exampleInputEmail1">Name</label>
                  <input type="text" name="name" required="" class="form-control" value="{{$user->name}}">
                </div>
                </div>
                <div class="form-group">
                  <div class="col">
                  <label for="exampleInputEmail1">Email</label>
                  <input type="email" name="email" required="" class="form-control" value="{{$user->email}}">
                </div>
                </div> 
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <div class="col">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
              </div>
            </form>
          </div>
        </div>
          <!-- /.box -->
		</div> 
	</div>
</div>
</div>
</div>


@include('footer')