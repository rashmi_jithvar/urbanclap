@include('header')
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">View Client</div>
                            </div>

                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="{{ url('home') }}">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                            <li><i class="fa fa-list"></i>&nbsp;<a class="parent-item" href="{{ url('import_list') }}"> List</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active">View </li>
                            </ol>
                        </div>
                    </div>
                     <div class="row">
                      <div class="col-sm-12">
                             <div class="card-box">
                                 <div class="card-head">
                                     <header>View Client</header>
                                 </div>
                                 <div class="card-body ">
                                 <div class="table-scrollable">
                                  <table id="mainTable" class="table table-striped">
                                  <thead>

                                  </thead>
                                  <tbody>
                                      <tr>
                                          <th>Docket No.</th>
                                          <td>{{$list->docket}}</td>
                                      </tr>
                                      <tr>
                                          <th>Booking Date</th>
                                          <td>{{date("d-m-Y",strtotime($list->booking_date))}}</td>
                                      </tr>
                                      <tr>
                                          <th>EDD</th>
                                          <td>{{date("d-m-Y",strtotime($list->edd))}}</td>
                                      </tr>
                                      <tr>
                                          <th>Delivery Date</th>
                                          <td>{{date("d-m-Y",strtotime($list->delivery_date))}}</td>
                                      </tr>                                      
                                      <tr>
                                          <th>Origin</th>
                                          <td>{{$list->origin}}</td>
                                      </tr>
                                      <tr>
                                          <th>Destination</th>
                                          <td>{{$list->destination}}</td>
                                      </tr>                                      
                                      <tr>
                                          <th>Zone</th>
                                          <td>{{$list->zone}}</td>
                                      </tr>
                                      <tr>
                                          <th>No. Of Boxex</th>
                                          <td>{{$list->no_of_boxes}}</td>
                                      </tr>                                     
                                       <tr>
                                          <th>Dimensions</th>
                                          <td>{{$list->dimensions}}</td>
                                      </tr>
                                      <tr>
                                          <th>Gross Value</th>
                                          <td>{{$list->gross_weight}}</td>
                                      </tr>                                     
                                       <tr>
                                          <th>Vol. Weight</th>
                                          <td>{{$list->vol_weight}}</td>
                                      </tr>
                                      <tr>
                                          <th>Transporter</th>
                                          <td>{{$list->transporter}}</td>
                                      </tr>  
                                      <tr>
                                          <th>  Status</th>
                                          <td>{{$list->status}}</td>
                                      </tr>                                        
                                      <tr>
                                          <th>Remark</th>
                                          <td>{{$list->remark}}</td>
                                      </tr>                                        
                                      <tr>
                                          <th>  Created By</th>
                                          <td>{{$list->created_b}}</td>
                                      </tr>                                       
                                       <tr>
                                          <th>Created On</th>
                                          <td>{{date("d-m-Y",strtotime($list->created_on))}}</td>
                                      </tr>                                        
                                      <tr>
                                          <th>Updated By</th>
                                          <td>{{$list->updated_by}}</td>
                                      </tr> 
                                        <tr>
                                        <th>Updated No.</th>
                                          <td>{{date("d-m-Y",strtotime($list->updated_on))}}</td>
                                      </tr>
                                                                                                                 
                                  </tbody>
                                  <tfoot>
                                  </tfoot>
                              </table>
                              </div>
                                 </div>
                             </div>
                         </div>
                    </div>
                </div>
            </div>
@include('footer')
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                