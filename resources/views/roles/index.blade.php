@include('header')
<div class="page-content-wrapper">
                <div class="page-content" style="min-height:1271px">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Roles</div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="{{ url('home') }}">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li><a class="parent-item" href="{{ url('roles/create') }}">Add Role</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active">Roles</li>
                            </ol>
                        </div>
                    </div>
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="card">

            <div class="card-body">
            	<a href="{{ url('roles/create') }}" class="btn btn-info" role="button">Create Roles</a>

<br><br>
              <h4 class="card-title">Data table</h4>
              <div class="row">

                <div class="col-12">                            
                  @if(session()->has('message'))
                                <div class="alert alert-success">
                                    {{ session()->get('message') }}
                                </div>
                  @endif
                  <div class="table-responsive">
                    <table id="example1" class="display" style="width:100%;">
                      <thead>
                        <tr>
                            <th>Sr.no. </th>
                            <th>Roles</th>
                            <th>Status</th>
                            
                            <th>Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                        @php $i = 0
                         @endphp
                        @foreach($roles as $role)
                        @php $i++
                         @endphp
                        <tr>
                            <td>{{$i}}</td>
                            <td>{{$role->name}}</td>
                            <td>
                         @if($role->status == '1')
                               <label class="badge badge-success">Active</label>
                          
                          @else
                                <label class="badge badge-danger">Pending</label>
                           @endif
                              
                        
                            </td>
                            <td>
            
                    <form action="{{action('RolesController@destroy', $role->id)}}" method="post">
                    {{csrf_field()}}
                        <input name="_method" type="hidden" value="DELETE">
                              <a href="{{action('RolesController@edit',$role->id)}}" class="btn btn-info" role="button"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                        <button class="btn btn-danger" type="submit" Onclick="return ConfirmDelete();"><i class="fa fa-trash" ></i></button>
                    </form>
                              
                            </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@include('footer')