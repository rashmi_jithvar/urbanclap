@include('header')
<?php
if($invoice->id)
{
       $order_id = $invoice->id;
}
else{
      $order_id = Session::get('order_id');
} 

?>

<div class="page-content-wrapper">
                <div class="page-content" style="min-height:1271px">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Invoice</div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                               
                                </li>
                                <li class="active">Invoice</li>
                            </ol>
                        </div>
                    </div>
<div class="main-panel">
    <div class="content-wrapper">
       
            <div class="row">

                <div class="col-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            @if(session()->has('message'))
                                <div class="alert alert-success">
                                    {{ session()->get('message') }}
                                </div>
                            @endif
                            <h4 class="card-title">Add Invoice</h4>
                          <form  method="POST" action="{{ url('invoice/store') }}">
                                            @csrf
                                            @if(Session::has('message'))
                                                    <div class='alert alert-success'>
                                                    {{ Session::get('message') }}
                                                    @php
                                                    Session::forget('message');
                                                    @endphp
                                                    </div>
                                            @endif
                                <div class="form-group row">
                
                                    <div class="col-md-3">
                                        <label>Invoice id:</label>
                                        <input type="text" class="form-control" id="invoice_id" placeholder="Enter Invoice id" name="invoice_id" >
                                        <span class="text-danger">{{ $errors->first('invoice_id') }}</span> 
                                    </div>
                                </div>

        <div class="col-12 col-sm-12 col-lg-12 col-xl-12 col-md-12" id="Invoice_Modal">
           <h3>Add Product</h3>
            <table class="table table-borderds" id="form1">
                <thead >
                    <tr>
                        <th >Item Name</th>
                        <th >Value</th>
                        <th >Date From</th>
                        <th >Date To</th>

                       
                    </tr>
                    <tr>
                        <td>
                            <input type="text" class="form-control" id="name" placeholder="Enter Item Name" name="name" >
                                        <span class="text-danger">{{ $errors->first('name') }}</span> 
                                        <div id="search_result"></div>
                        </td>
                        <td>
                            <input type="text" class="form-control" id="value" placeholder="Enter Value" name="value" >
                                <span class="text-danger">{{ $errors->first('value') }}</span> 
                        </td>
                        <td>

                       <!-- <div class="input-group date form_datetime" data-date="1979-09-16T05:25:07Z" data-date-format="dd MM yyyy" data-link-field="">-->
                                <input class="form-control" size="16" type="date" value="" name="from_date" id="from_date">
                                       <!-- <span class="input-group-addon"><span class="fa fa-remove"></span></span>
                                        <span class="input-group-addon"><span class="fa fa-calendar"></span></span>-->
                        </div>
                            <span class="text-danger">{{ $errors->first('from_date') }}</span> 
                        
                      </td>
                        <td style="width:300px;">
                        <!--<div class="input-group date form_datetime" data-date="1979-09-16T05:25:07Z" data-date-format="dd MM yyyy" data-link-field="" >-->
                                <input class="form-control" size="16" type="date" id="to_date" value="" name="to_date">
                                        <!--<span class="input-group-addon"><span class="fa fa-remove"></span></span>
                                        <span class="input-group-addon"><span class="fa fa-calendar"></span></span>-->
                        </div>
                            <span class="text-danger">{{ $errors->first('to_date') }}</span> 
                        </td>
                        <td>
                            <div class="form-group">
                                <button type="button" class="btn btn-success icon icon-plus2" onclick="addCart();" id="add_new_item1"><i class="icon-plus"></i></button>
                            </div>
                        </td>
                    </tr>
                </thead>
            </table>

            <hr>

            <div id="cart-table">
                <table class="table table-borderd" id="cart-table-body">
                    <thead>
                        <tr>
                            <th><strong>#</strong></th>
                            <th><strong>Item Name</strong></th>
                            <th><strong>Value</strong></th>
                            <th ><strong>Date From</strong></th>
                            <th ><strong>Date To</strong></th>
                            <th><strong>Sub Total</strong></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody id="cart-body">
                    </tbody>
                    <tfoot>
                       
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th class="text-right"></th>
                            <th class="text-right"></th>
                            <th class="text-right"></th> 
                            <th class="text-right"></th>
                           
                        </tr>
                    </tfoot>
                </table>

            </div>

        </div>                                
                                <!--<div class="form-group row">
                                    <div class="col">
                                        <label >Status</label>
                                        <select class="form-control"  name="status" >
                                            <option disabled selected>Select Status</option>
                                            <option value="1">Active</option>
                                            <option value="0">Inactive</option>
                                        </select>
                                        <span class="text-danger">{{ $errors->first('status') }}</span> 
                                    </div>

                                </div>-->
                               <button type="submit" name="save" value="save" class="btn btn-primary mr-2">Save</button>
                               <button type="submit" name="save" value="save&send" class="btn btn-primary mr-2">Save & Send mail</button>
                                
                            </form>
                        </div>
                    </div>
                </div>

            </div>
    </div>
</div>
</div>
<script type="text/javascript">
        function addCart()
    {
        var name = $("#name").val();
        var value = $("#value").val();
        var from_date = $("#from_date").val();
        var to_date = $("#to_date").val();
        window.location = "{{ url('invoice/add_product') }}?name="+name+"&value="+value+"&from_date="+from_date
+"&to_date="+to_date;
                        loadCart('<?= $order_id; ?>');
                         if (data == '1') {
                               
                                swal("Added!", " Item Added successfully", "success");
                     
                        } else {


                                swal("Oopss!", "Stock not Avaialble", "error");
                    }
    }
loadCart({{$order_id}});
function loadCart($order_id)
{
  
    $.ajax({
        type : 'get',
        url : "{{URL::to('invoice/load_cart')}}",
        data:{'order_id':$order_id},
        success:function(data){

        $('#cart-table-body').html(data);
        }
        });
  

     // var total = $("#cart-table-body").find("#total").text();
     // alert(total);
    //  $("#invoice-total_amount").val(parseFloat($.trim(total)));
  }


</script>
<script type="text/javascript">
        $('#name').on('keyup',function(){
        $value=$(this).val();   
        $.ajax({
        type : 'get',
        url : "{{URL::to('invoice/search')}}",
        data:{'search':$value},
        success:function(data){
           
        $('#search_result').html(data);
        $('#search_result > li > a').click(function(){
        var search_resultList = $(this).text();
        $("#search_result").val(search_resultList); 
        $("#search_result").hide();         
      });
    }
});
})
</script>
<script type="text/javascript">
       function removeCart(url) {
                  
            //var $form = $("form#" + modal_formName);
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url: url,
                        type: "POST",
                        success: function (result) {
                           //console.log(data);
                           loadCart('<?= $order_id; ?>');
                            if (result == 'Deleted') {
                               
                                swal("Deleted!", " Item deleted successfully", "success");
                     
                            } else {


                                swal("Oopss!", "Some error occured", "error");
                            }
                        },
                        error: function (xhr, status) {
                            swal("Oopss!", "Some error occured", "error");
                             
                        }
                    });
                } else {
                    swal("Your " + modal_formName + " Item is safe!");
                }
            });
        }
        }
</script>
<script type="text/javascript">
        $.ajaxSetup({ headers: { 'csrftoken' : '{{ csrf_token() }}' } });
</script>

@include('footer')