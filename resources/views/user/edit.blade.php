@include('header')
<div class="page-content-wrapper">
                <div class="page-content" style="min-height:1271px">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">User </div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="{{ url('/home') }}">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                            
                                <li class="active">&nbsp;<a class="parent-item" href="{{ url('/user') }}">User List</a>&nbsp;<i class="fa fa-angle-right"></i></li>     
                                <li class="active">Update</li>
                            </ol>
                        </div>
                    </div>
<div class="main-panel">
    <div class="content-wrapper">
       
            <div class="row">

                <div class="col-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Add User </h4>
                          <form  id="customerform" method="POST" action="{{ action('UserController@update',$id) }}">
                                            @csrf
                                            @if(Session::has('message'))
                                                    <div class='alert alert-success'>
                                                    {{ Session::get('message') }}
                                                    @php
                                                    Session::forget('message');
                                                    @endphp
                                                    </div>
                                            @endif
                                <div class="form-group row">
                
                                    <div class="col">
                                        <label>FullName:</label>
                                        <input type="text"  id="formGroupExampleInput" class="form-control"   name="name" value="{{$user->name}}" >
                                         <span class="text-danger">{{ $errors->first('name') }}</span> 
                                    </div>
                                    <div class="col">
                                        <label>Email:</label>
                                        <input type="email" id="email" class="form-control"  value="{{$user->email}}"name="email" >
                                         <span class="text-danger">{{ $errors->first('email') }}</span>
                                        
                                    </div>
                                    <div class="col">
                                        <label for="exampleSelectGender">Role Id</label>
                                      <select name="role_id" id="role_id" class="form-control">
                                        <option disabled selected >Select RoleId</option>
                                        @if (isset($roledropdown))
                                      @foreach($roledropdown as $key => $dropdownGroup)
                                      <option value ="{{$key}}" {{ $user->role_id == $key ? 'selected="selected"' : '' }}>{{ $dropdownGroup }} </option>
                                       @endforeach
                                       @endif
                                    </select>
                                       <span class="text-danger">{{ $errors->first('role_id') }}</span>  
                                    </div>
                                </div>
                              
                                <div class="form-group row">
                                 <!-- <div class="col">
                                        <label>Username:</label>
                                        <input type="text" class="form-control"  placeholder="Enter Username" name="username" >
                                       <span class="text-danger">{{ $errors->first('username') }}</span>   
                                        
                                    </div>-->
                                <div class="col">
                                        <label>Password:</label>
                                        <input type="password" class="form-control" id="password" placeholder="New Password ( if you want to change it )" name="password" >
                                      <span class="text-danger">{{ $errors->first('password') }}</span>    
                                </div>
                                <div class="col">
                                        <label>Confirm Password:</label>
                                        <input type="password" class="form-control" id="confirm_password" placeholder="Confirm Password ( if you want to change it )" name="confirm_password" >
                                    <span class="text-danger">{{ $errors->first('confirm_password') }}</span>      
                                </div>
                                <div class="col">
                                        <label for="exampleSelectGender">Status</label>
                                        <select class="form-control" id="status" name="status" >
                                            <option disabled selected>Select Status</option>
                                            <option value="Active" {{ $user->status == 'Active' ? 'selected':'' }}>Active</option>
                                            <option value="Inactive" {{ $user->status == 'Inactive' ? 'selected':'' }}>Inactive</option>
                                        </select>
                                        <span class="text-danger">{{ $errors->first('status') }}</span> 
                                    </div>
                                    
                              </div>
                              <div class="form-group row">
     
                                </div>
                               <button type="submit" class="btn btn-primary mr-2">Submit</button>
                              
                            </form>
                        </div>
                    </div>
                </div>

            </div>
    </div>
  </div>
</div>
</div>

@include('footer')